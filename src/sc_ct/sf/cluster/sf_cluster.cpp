#include "sc_ct/sf/cluster/sf_cluster.h"


sf::sf_cluster::sf_cluster(sc_core::sc_module_name name) 
    : sct_core::ct_module(name)
{
}

void sf::sf_cluster::get_derivatives(bool use_input_checkpoints,
    const sct_core::ct_state &x , sct_core::ct_state &dxdt , double t )
{
    // Set the mode of the inputs manager to either current_values or checkpoint
    inputs.use_checkpoints(use_input_checkpoints);

    // Iterate over components with DE inputs
    //for (auto [id, module]: de_in_modules) {
	for (auto i: de_in_modules) {
		sf::sf_de_in_module_if* module = i.second;
        for (auto port: module->get_de_boolean_ports()) {
            module->set_de_input(port->name(), inputs[*port]);
        }

        for (auto port: module->get_de_double_ports()) {
            module->set_de_input(port->name(), inputs[*port]);
        }
    }

    set_signals_from_state(x);
    execute_primitives(t);
    set_derivatives(dxdt);
}


std::unordered_map<int, bool>  sf::sf_cluster::is_event(const sct_core::ct_state &x, double t) {
    std::unordered_map<int, bool> events;
    
    set_signals_from_state(x);
    execute_primitives(t);
    
    //for (auto [id, ct_de_conv_ptr]: ct_de_converters) {
    for (auto i: ct_de_converters) {
		sf::id_t id = i.first;
		sf::sf_ct_de_converter* ct_de_conv_ptr = i.second;
        events[id] = ct_de_conv_ptr->execute(t);
    }
    
    return events;
}

sc_core::sc_time sf::sf_cluster::get_next_predictable_event_time() {
    sc_core::sc_time t0 = sc_core::sc_time(0, sc_core::SC_SEC);
    sc_core::sc_time next_event_time = t0;
    sc_core::sc_time temp;

    // Iterate over all CT/DE converters to get, if available,
    // the time of the next predictable event
    //for (auto [id, ct_de_conv_ptr]: ct_de_converters) { // only supported in c++17 but not standard SC 2.3.3
    for (auto i: ct_de_converters) {
		sf::sf_ct_de_converter* ct_de_conv_ptr = i.second;
        temp = ct_de_conv_ptr->get_next_event_time();
        bool should_update = next_event_time == t0 || 
                                temp < next_event_time;
        next_event_time = should_update ? temp : next_event_time;
    }

    // Return the time of the next predictable event or 
    // the t_0, which is interpreted as if no predictable event 
    // is found
    return next_event_time;
}

bool sf::sf_cluster::execute_updates() {
    bool has_been_updated = false;
    //for (auto [id, integ_ptr]: integrators) { // only supported in c++17 but not standard SC 2.3.3
    for (auto i: integrators) {
		sf::sf_integrator* integ_ptr = i.second;
        has_been_updated = has_been_updated || integ_ptr->execute_updates();
    }
    return has_been_updated;
}

void sf::sf_cluster::generate_outputs(bool state_event_located,
    std::unordered_map<int, bool> events) 
{
    report_state_events(events);
    report_sampler_events();
    execute_sinks(); 
}


void sf::sf_cluster::start_of_simulation() {
    store_primitives_and_signals();
    create_primitives_graph();
    order_primitives();
    resize_state();
}

void sf::sf_cluster::resize_state() {
    x.resize(integrators.size());
}

void sf::sf_cluster::store_primitives_and_signals() {
    std::vector<sc_core::sc_object *> children = get_child_objects();
    for (auto obj: children) {
        store_primitive(obj);
        store_signal(obj);
        store_ct_de_converter(obj);
        store_sink(obj);
        store_de_in_module(obj);
    }
}

void sf::sf_cluster::store_primitive(sc_core::sc_object *obj) {
    sf::sf_primitive *prim_ptr;
    sf::sf_integrator *integ_ptr;
    int integrators_count = 0;

    prim_ptr = dynamic_cast<sf::sf_primitive *>(obj);
    if (prim_ptr) {
        primitives.insert({prim_ptr->get_id(), prim_ptr});
        
        // If is integrator, insert it in integrators map
        integ_ptr  = dynamic_cast<sf::sf_integrator *>(prim_ptr);
        if (integ_ptr) {
            // Relate each integrator to a state variable
            integ_ptr->set_cluster_state_var_ptr(&x[integrators_count++]);
            // Insert them in a set to ease access
            integrators.insert({integ_ptr->get_id(), integ_ptr});
        }
    }
}

void sf::sf_cluster::store_signal(sc_core::sc_object *obj) {
    sf::sf_signal *sig_ptr;
    sig_ptr = dynamic_cast<sf::sf_signal *>(obj);
    if (sig_ptr) {
        signals.push_back(sig_ptr);
    }
}

void sf::sf_cluster::store_ct_de_converter(sc_core::sc_object *obj) {
    sf::sf_ct_de_converter *ct_de_conv_ptr;

    ct_de_conv_ptr = dynamic_cast<sf::sf_ct_de_converter *>(obj);
    if (ct_de_conv_ptr) {
        ct_de_converters.insert({ct_de_conv_ptr->get_id(), ct_de_conv_ptr});
    }
}

void sf::sf_cluster::store_sink(sc_core::sc_object *obj) {
    sf::sf_sink *sink_ptr;

    sink_ptr = dynamic_cast<sf::sf_sink *>(obj);
    if (sink_ptr) {
        sinks.insert({sink_ptr->get_id(), sink_ptr});
    }
}

void sf::sf_cluster::store_de_in_module(sc_core::sc_object *obj) {
    sf::sf_de_in_module_if *de_in_module_ptr;

    de_in_module_ptr = dynamic_cast<sf::sf_de_in_module_if *>(obj);
    if (de_in_module_ptr) {
        de_in_modules.insert({de_in_module_ptr->get_id(), de_in_module_ptr});
    }
}

void sf::sf_cluster::order_primitives() {
    boost::topological_sort(*g, std::front_inserter(ordered_primitives_list));

        for (vertex_list::iterator i = ordered_primitives_list.begin();
        i != ordered_primitives_list.end(); ++i){
    }
}

void sf::sf_cluster::create_primitives_graph() {         
    std::map<sf::id_t, vertex_t> prim_vertex_map;
    g = new Graph();
    add_vertices_to_graph(prim_vertex_map);
    add_edges_to_graph(prim_vertex_map);
}

void sf::sf_cluster::add_vertices_to_graph(std::map<sf::id_t, vertex_t> &prim_vertex_map) {
    vertex_t v;
    //for (auto [id, prim_ptr]: primitives) { // only supported in c++17 but not standard SC 2.3.3
    for (auto p: primitives) {
		sf::id_t id = p.first;
        v = boost::add_vertex(*g);
        prim_vertex_map[id] = v;
        vertex_primitive_map[v] = id;
    }
}

void sf::sf_cluster::add_edges_to_graph(std::map<sf::id_t, vertex_t> &prim_vertex_map) {
    std::list<std::pair<sf::id_t, sf::id_t> > interconnections;
    vertex_t source_v, target_v;
    for (auto signal: signals) {
        interconnections = signal->get_connected_primitives();
        for (auto interconnection: interconnections) {
            source_v = prim_vertex_map[interconnection.first];
            target_v = prim_vertex_map[interconnection.second];
            boost::add_edge(source_v, target_v, *g);
        }
    }
}

void sf::sf_cluster::set_signals_from_state(const sct_core::ct_state &x){
    int j = 0;
    //for (auto [id, integ_ptr]: integrators) { // only supported in c++17 but not standard SC 2.3.3
    for (auto i: integrators) {
		sf::sf_integrator* integ_ptr = i.second;
        integ_ptr->set_state(x[j++]);
    }
}

void sf::sf_cluster::execute_primitives(double t) {
    sf::id_t prim_id;
    for (vertex_list::iterator i = ordered_primitives_list.begin();
        i != ordered_primitives_list.end(); ++i){
        prim_id = vertex_primitive_map[*i];
        primitives[prim_id]->execute(t);
    }
}

void sf::sf_cluster::set_derivatives(sct_core::ct_state &dxdt){
    int j = 0;
    //for (auto [id, integ_ptr]: integrators) { // only supported in c++17 but not standard SC 2.3.3
    for (auto i: integrators) {
		sf::sf_integrator* integ_ptr = i.second;
        dxdt[j++] = integ_ptr->get_derivative();
    }
}

void sf::sf_cluster::report_state_events(std::unordered_map<int, bool> events) {
    // for (auto [id, detected] : events) { // only supported in c++17 but not standard SC 2.3.3
    for (auto e: events) {
		int id = e.first;
		bool detected = e.second;
        if (detected) {
            ct_de_converters[id]->report_event();
        }
    }
}

void sf::sf_cluster::report_sampler_events() {
    sf::sf_sampler *sampler_ptr;
    //for (auto [id, converter] : ct_de_converters) { // only supported in c++17 but not standard SC 2.3.3
    for (auto c: ct_de_converters) {
		sf::sf_ct_de_converter* converter = c.second;
        sampler_ptr = dynamic_cast<sf::sf_sampler *>(converter);
        if (sampler_ptr) {
            sampler_ptr->report_event();
        }
    }
}

void sf::sf_cluster::execute_sinks() {
    //for (auto [id, sink] : sinks) { // only supported in c++17 but not standard SC 2.3.3
    for (auto s : sinks) {
		sf::sf_sink* sink = s.second;
        sink->execute(sc_core::sc_time_stamp().to_seconds());
    }
}
