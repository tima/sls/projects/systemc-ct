#ifndef SF_CLUSTER_H
#define SF_CLUSTER_H

// Boost macro definition to allow deprecated headers
#define BOOST_ALLOW_DEPRECATED_HEADERS   1

#include <systemc>
#include <utility> // std::pair
#include <map>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topological_sort.hpp>

#include <iostream> // std::cout

////////////////////////////////////////////////////////////////////////
// CT/DE sync (temporary)
////////////////////////////////////////////////////////////////////////
#include "sc_ct/core/ct_module.h"

// MoC
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/communication/sf_de_in_module_if.h"
#include "sc_ct/sf/components/sf_ct_de_converter.h"
#include "sc_ct/sf/components/sf_integrator.h"
#include "sc_ct/sf/components/sf_primitive.h"
#include "sc_ct/sf/components/sf_sampler.h"
#include "sc_ct/sf/components/sf_sink.h"
#include "sc_ct/sf/components/sf_ct_de_converter.h"

//////////////////////////////////////////////////////////////////
// To detect algebraic loops
//////////////////////////////////////////////////////////////////
using namespace boost;
struct cycle_detector : public dfs_visitor<> {
    cycle_detector( bool& has_cycle) 
    : _has_cycle(has_cycle) { }

    template <class Edge, class Graph>
    void back_edge(Edge, Graph&) {
        _has_cycle = true;
    }
    protected:
        bool& _has_cycle;
};


class sf::sf_cluster : virtual public sct_core::ct_module { 
    public:
        sf_cluster(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_cluster"));

    /***********************************************************
     * SYNC. ALG. - MoC API
     * *********************************************************/
    protected:
        void get_derivatives(bool use_input_checkpoints,
            const sct_core::ct_state &x , sct_core::ct_state &dxdt , double t );


        std::unordered_map<int, bool>  is_event(const sct_core::ct_state &x, double t);
        
        sc_core::sc_time get_next_predictable_event_time();

        bool execute_updates();

        void generate_outputs(bool state_event_located,
            std::unordered_map<int, bool> events);

    /***********************************************************
     * MoC specific atributes and methods
     * *********************************************************/
    private:
        std::map<sf::id_t, sf::sf_primitive*> primitives;
        std::map<sf::id_t, sf::sf_integrator*> integrators;
        std::map<sf::id_t, sf::sf_ct_de_converter*> ct_de_converters;
        std::map<sf::id_t, sf::sf_sink*> sinks;
        // Modules with inputs
        std::map<sf::id_t, sf::sf_de_in_module_if*> de_in_modules;

        std::list<sf::sf_signal *> signals;


        // Type definitions for convenience 
        typedef adjacency_list<vecS, vecS, bidirectionalS, 
                property<vertex_color_t, default_color_type>
                > Graph;
        typedef Graph::vertex_descriptor vertex_t;
        typedef std::list<vertex_t> vertex_list;
        std::map<vertex_t, sf::id_t> vertex_primitive_map;


        // Ordering
        Graph *g;
        vertex_list ordered_primitives_list;

        void start_of_simulation();
        void resize_state();

        void store_primitives_and_signals();

        void store_primitive(sc_core::sc_object *obj);

        void store_signal(sc_core::sc_object *obj);

        void store_ct_de_converter(sc_core::sc_object *obj);

        void store_sink(sc_core::sc_object *obj);

        void store_de_in_module(sc_core::sc_object *obj);

        void order_primitives();

        void create_primitives_graph();

        void add_vertices_to_graph(std::map<sf::id_t, vertex_t> &prim_vertex_map);

        void add_edges_to_graph(std::map<sf::id_t, vertex_t> &prim_vertex_map);

        void set_signals_from_state(const sct_core::ct_state &x);

        void execute_primitives(double t);

        void set_derivatives(sct_core::ct_state &dxdt);

        void report_state_events(std::unordered_map<int, bool> events);

        void report_sampler_events();

        void execute_sinks();
};

#endif
