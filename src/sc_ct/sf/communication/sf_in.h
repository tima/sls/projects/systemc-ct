#ifndef SF_IN_H 
#define SF_IN_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/communication/sf_in_if.h"
#include "sc_ct/sf/components/sf_primitive.h"
#include "sc_ct/sf/components/sf_integrator_base.h"
#include "sc_ct/sf/components/sf_ct_de_converter.h"
#include "sc_ct/sf/communication/sf_signal.h"


namespace sf {

    class sf_in: public sc_core::sc_port<sf::sf_in_if> {
        public:
            sf_in(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_signal"));
            
            virtual const char* kind() const;

        protected:
            virtual void before_end_of_elaboration();
        
        private:
            // Utility
            bool is_integrator(sc_core::sc_object *obj);
            bool is_ct_de_converter(sc_core::sc_object *obj);
    };

}

#endif
