#ifndef SF_OUT_H
#define SF_OUT_H


#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/communication/sf_out_if.h"
#include "sc_ct/sf/communication/sf_signal.h"
#include "sc_ct/sf/components/sf_primitive.h"

namespace sf {

    class sf_out: public sc_core::sc_port<sf::sf_out_if> {
        public:
            sf_out(sc_core::sc_module_name name = sc_core::sc_gen_unique_name(sf::sf_out_kind.c_str()));
            
            virtual const char* kind() const;

        protected:
            virtual void before_end_of_elaboration();
    };

}

#endif
