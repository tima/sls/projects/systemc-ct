#include "sc_ct/sf/communication/sf_signal.h"


sf::sf_signal::sf_signal(sc_core::sc_module_name name) : 
    sc_core::sc_prim_channel(name) 
{
    value = 0.0; 
    has_writer = false;
    writer_id = 0;
}

void sf::sf_signal::write(double val) {
    value = val;
}

double sf::sf_signal::read() {
    return value;
}

void sf::sf_signal::update() {
    return;
}

 const char* sf::sf_signal::kind() const {
    return  sf::sf_signal_kind.c_str();
}

std::list<std::pair<sf::id_t, sf::id_t>> sf::sf_signal::get_connected_primitives() const {
    std::list<std::pair<sf::id_t, sf::id_t>> pairs;
    if (!has_writer){
        return pairs;
    }

    for (auto reader_id: reader_ids) {
        pairs.push_back(std::make_pair(writer_id, reader_id));
    }
    return pairs;
} 


void sf::sf_signal::add_reader_id(sf::id_t id){
    reader_ids.push_back(id);
}

void sf::sf_signal::set_writer_id(sf::id_t id){
    has_writer = true;
    writer_id = id;
}

