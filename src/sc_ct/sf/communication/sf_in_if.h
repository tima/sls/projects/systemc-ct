#ifndef SF_IN_IF
#define SF_IN_IF

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"

namespace sf {

    class sf_in_if: public virtual sc_core::sc_interface {
        public:
            virtual double read() = 0;
        protected:
            void add_reader_id(sf::id_t id);
    };

}

#endif
