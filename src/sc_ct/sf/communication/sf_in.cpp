#include "sc_ct/sf/communication/sf_in.h"


sf::sf_in::sf_in(sc_core::sc_module_name name) : sc_core::sc_port<sf::sf_in_if>(name) {

}

const char* sf::sf_in::kind() const {
    return sf::sf_in_kind.c_str();
}


void sf::sf_in::before_end_of_elaboration() {
    sf::sf_primitive *parent_prim;
    sc_core::sc_object *parent_obj;

    parent_obj = this->get_parent_object();
    parent_prim = dynamic_cast<sf::sf_primitive *>(parent_obj);

    // Omit adding integrator and ct_de converters  to the list
    // of modules with interconnection
    if (is_integrator(parent_obj) || is_ct_de_converter(parent_obj)) {
        return;
    }

    if (parent_prim != NULL) {
        sf::sf_in_if *in_if = this->operator->();
        dynamic_cast<sf::sf_signal *>(in_if)->add_reader_id(parent_prim->get_id());
    }
}

// Utility
bool sf::sf_in::is_integrator(sc_core::sc_object *obj) {
    sf::sf_integrator_base *ptr = dynamic_cast<sf::sf_integrator_base *>(obj);
    return ptr != NULL;
}

bool sf::sf_in::is_ct_de_converter(sc_core::sc_object *obj) {
    sf::sf_ct_de_converter *ptr = dynamic_cast<sf::sf_ct_de_converter *>(obj);
    return ptr != NULL;
}

