#ifndef SF_OUT_IF
#define SF_OUT_IF

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/communication/sf_in_if.h"

namespace sf {

    class sf_out_if: public virtual sf_in_if {
        public:
            virtual void write(double) = 0;
        protected: 
            void set_writer_id(sf::id_t id);
    };
}

#endif
