#include "sc_ct/sf/communication/sf_de_in_module_if.h"


sf::sf_de_in_module_if::sf_de_in_module_if(sc_core::sc_module_name name) :
    sf::sf_module(name), de_bool_ports(), de_double_ports()
{
}

const std::list<sc_in_bool *> &sf::sf_de_in_module_if::get_de_boolean_ports() {
    return de_bool_ports;
}

const std::list<sc_in_double *> &sf::sf_de_in_module_if::get_de_double_ports() {
    return de_double_ports;
}

// This should be protected but allowed for sf_cluster (friend class)
void sf::sf_de_in_module_if::set_de_input(std::string name, bool val) {
}

void sf::sf_de_in_module_if::register_de_port(sc_in_bool &port){
    de_bool_ports.push_back(&port);
}

void sf::sf_de_in_module_if::register_de_port(sc_in_double &port){
    de_double_ports.push_back(&port);
}
