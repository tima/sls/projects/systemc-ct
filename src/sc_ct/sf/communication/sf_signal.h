#ifndef SF_SIGNAL_H
#define SF_SIGNAL_H

#include <systemc>
#include <list>
#include <utility> // std::pair
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/communication/sf_out_if.h"


namespace sf {

    class sf_signal: public sc_core::sc_prim_channel, public sf_out_if {
        public: 
            sf_signal(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_signal") );

            virtual void write(double val);

            virtual double read();

            void update();

            virtual const char* kind() const;

            std::list<std::pair<sf::id_t, sf::id_t>> get_connected_primitives() const;

        protected:
            void add_reader_id(sf::id_t id);
            void set_writer_id(sf::id_t id);

            friend class sf::sf_out;
            friend class sf::sf_in;

        private:
            double value;
            sf::id_t writer_id;
            bool has_writer;
            std::list<sf::id_t> reader_ids;  
    };

}

#endif
