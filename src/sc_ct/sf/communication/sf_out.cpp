#include "sc_ct/sf/communication/sf_out.h"

sf::sf_out::sf_out(sc_core::sc_module_name name) 
    : sc_core::sc_port<sf::sf_out_if>(name) 
{

}

const char* sf::sf_out::kind() const {
    return sf::sf_out_kind.c_str();
}

void sf::sf_out::before_end_of_elaboration() {
    sf::sf_primitive *parent_prim;
    sc_core::sc_object *parent_obj;

    parent_obj = this->get_parent_object();
    parent_prim = dynamic_cast<sf::sf_primitive *>(parent_obj);

    if (parent_prim != NULL) {
        sf::sf_out_if *out_if = this->operator->();
        dynamic_cast<sf::sf_signal *>(out_if)->set_writer_id(parent_prim->get_id());
    }
}
