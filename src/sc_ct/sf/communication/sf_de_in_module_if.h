#ifndef sf_de_in_module_if_H
#define sf_de_in_module_if_H

#include <systemc>
#include <list>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/components/sf_module.h"

 // List of DE input ports to which the primitive is sensitive
typedef sc_core::sc_port<sc_core::sc_signal_in_if<bool>, 1> sc_in_bool; 
typedef sc_core::sc_port<sc_core::sc_signal_in_if<double>, 1> sc_in_double; 


// Interface for modules containing DE inputs that can change 
// structure of the signal flow graph
namespace sf {

    class sf_de_in_module_if : virtual public sf::sf_module {
        public:
            sf_de_in_module_if(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_de_in_module_if"));

            virtual const std::list<sc_in_bool *> &get_de_boolean_ports();

            virtual const std::list<sc_in_double *> &get_de_double_ports();

            // This should be protected but allowed for sf_cluster (friend class)
            virtual void set_de_input(std::string name, bool val);

        protected:
            virtual void register_de_port(sc_in_bool &port);

            virtual void register_de_port(sc_in_double &port);

            friend class sf::sf_de_in<bool>;
            friend class sf::sf_de_in<double>;

        private:
            std::list<sc_in_bool *> de_bool_ports;
            std::list<sc_in_double *> de_double_ports;

    };

}

#endif
