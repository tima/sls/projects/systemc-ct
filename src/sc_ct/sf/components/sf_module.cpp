#include "sc_ct/sf/components/sf_module.h"

sf::id_t sf::sf_module::count = 0;

sf::sf_module::sf_module(sc_core::sc_module_name name)
    : sc_core::sc_module(name)
{
    id = count++;
}

sf::id_t sf::sf_module::get_id() {
    return id;
}


const char* sf::sf_module::kind() const {
    return sf::sf_module_kind.c_str();
}
