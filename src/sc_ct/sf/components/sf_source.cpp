#include "sc_ct/sf/components/sf_source.h"

sf::sf_source::sf_source(sc_core::sc_module_name name, double _amplitude ) 
    : sf::sf_primitive(name), y("y")
{    
    amplitude = _amplitude;
}

const char* sf::sf_source::kind() const{
    return sf::sf_source_kind.c_str();
}

bool sf::sf_source::execute(double t) {
    y->write(amplitude);
    return true;
}

void sf::sf_source::set_amplitude(double amp) {
    amplitude = amp;
}

double sf::sf_source::get_amplitude() {
    return amplitude;
}
