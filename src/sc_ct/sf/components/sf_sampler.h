#ifndef SF_SAMPLER_H
#define SF_SAMPLER_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/components/sf_ct_de_converter.h"
#include "sc_ct/sf/communication/sf_in.h"

namespace sf {

    class sf_sampler: public sf::sf_ct_de_converter {
        public:
            sf::sf_in x;
            sc_core::sc_out<double> val_out;
        
            sf_sampler(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_sampler"), 
                sc_core::sc_time sampling_period = sc_core::sc_time(1, sc_core::SC_SEC) );

            virtual const char* kind() const;

            virtual sc_core::sc_time get_next_event_time();

            virtual void report_event();

        private:
            sc_core::sc_time next_sampling_time;
            sc_core::sc_time sampling_period;
    };

}

#endif
