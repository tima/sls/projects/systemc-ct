#ifndef SF_INTEGRATOR_BASE_H
#define SF_INTEGRATOR_BASE_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/components/sf_primitive.h"

namespace sf {
    class sf_integrator_base: public sf::sf_primitive {
        public: 
            virtual bool execute(double t = 0);

            void set_state(double _state);

            double get_state();

            virtual double get_derivative();

            virtual bool execute_updates();

            void set_cluster_state_var_ptr(double *ptr);

        protected:
            // Pointer to corresponding cluster state var
            double *cluster_state_var_ptr;
            double state;

            // Avoid object instantiation
            sf_integrator_base(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_integrator_base"), double state = 0);
    };
}

#endif
