#ifndef SF_MODULE_H
#define SF_MODULE_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"

namespace sf {

    class sf_module: public virtual sc_core::sc_module {
        public:
            // Class static variable to count the number of 
            // sf modules that have been instantiated
            static size_t count;
            
            sf_module(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_module"));

            sf::id_t get_id();

            virtual bool execute(double t = 0) = 0;


            virtual const char* kind() const;

        private:
            sf::id_t id;
    };

}

#endif
