#include "sc_ct/sf/components/sf_de_threshold_detector.h"


sf::sf_de_threshold_detector::sf_de_threshold_detector(sc_core::sc_module_name name) 
    : sf::sf_threshold_detector(name), threshold_in("threshold_in")
{    
}

const char* sf::sf_de_threshold_detector::kind() const{
    return sf::sf_de_threshold_detector_kind.c_str();
}

bool sf::sf_de_threshold_detector::execute(double t) {
    set_threshold(threshold_in.read());
    return sf::sf_threshold_detector::execute();
}
