#ifndef SF_CT_DE_CONVERTER_H
#define SF_CT_DE_CONVERTER_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/components/sf_module.h"


namespace sf {

    class sf_ct_de_converter: public sf::sf_module {
        public:
            virtual const char* kind() const;

            virtual bool execute(double t);

            virtual void report_event();

            virtual sc_core::sc_time get_next_event_time();

        protected:
            sf_ct_de_converter(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_ct_de_converter"));
    };

}
#endif
