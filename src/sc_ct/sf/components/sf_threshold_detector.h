#ifndef SF_DE_THRESHOLD_DETECTOR_H
#define SF_DE_THRESHOLD_DETECTOR_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/communication/sf_in.h"
#include "sc_ct/sf/components/sf_ct_de_converter.h"

namespace sf {

    class sf_threshold_detector: public sf::sf_ct_de_converter {
        public:
            sf::sf_in x;
            sc_core::sc_out<bool> event_out;

            sf_threshold_detector(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_threshold_detector"), double threshold = 1, bool report_rising = true) ;

            virtual const char* kind() const;

            virtual bool execute(double t = 0);

            virtual void report_event();

            void set_threshold(double threshold);

            double get_threshold();

        protected:
            void end_of_elaboration();

        private:
            double threshold, last_x;
            bool event_reported;
            bool report_rising;

    };

}

#endif
