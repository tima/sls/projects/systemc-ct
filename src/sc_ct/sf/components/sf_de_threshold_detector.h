#ifndef SF_THRESHOLD_DETECTOR_H
#define SF_THRESHOLD_DETECTOR_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/components/sf_threshold_detector.h"

namespace sf { 

    class sf_de_threshold_detector:  public sf::sf_threshold_detector {
        public:
            sc_core::sc_in<double> threshold_in;

            sf_de_threshold_detector(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_de_threshold_detector"));

            virtual const char* kind() const;

            virtual bool execute(double t = 0);
    };

}

#endif
