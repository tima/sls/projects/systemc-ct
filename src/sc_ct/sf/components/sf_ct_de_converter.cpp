#include "sc_ct/sf/components/sf_ct_de_converter.h"

sf::sf_ct_de_converter::sf_ct_de_converter(sc_core::sc_module_name name)
    : sf_module(name) 
{
}

const char* sf::sf_ct_de_converter::kind() const{
    return sf:: sf_ct_de_converter_kind.c_str();
}

bool sf::sf_ct_de_converter::execute(double t) {
    return false;
}

void sf::sf_ct_de_converter::report_event() {

}

sc_core::sc_time sf::sf_ct_de_converter::get_next_event_time() {
    return sc_core::sc_time(0, sc_core::SC_SEC);
}

