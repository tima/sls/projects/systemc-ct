#include "sc_ct/sf/components/sf_primitive.h"

sf::sf_primitive::sf_primitive(sc_core::sc_module_name name)
    : sf::sf_module(name)
{
}

const char* sf::sf_primitive::kind() const{
    return sf::sf_primitive_kind.c_str();
}
