#ifndef SF_SINK_FILE_H
#define SF_SINK_FILE_H

#include <systemc>
#include <fstream>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/components/sf_sink.h"
#include "sc_ct/sf/communication/sf_in.h"

// Base class for sink modules

namespace sf {
    class sf_sink_file:  public virtual sf::sf_sink {
        public:
            sf::sf_in x;

            sf_sink_file(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_sink_file"));

            virtual const char* kind() const;

            virtual bool execute(double t = 0);

        private:
            // To collect internal information: up and down thresholds
            std::ofstream *my_file;
            sf::id_t id;
    };
}

#endif
