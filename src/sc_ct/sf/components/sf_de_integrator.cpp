#include "sc_ct/sf/components/sf_de_integrator.h"

sf::sf_de_integrator::sf_de_integrator(sc_core::sc_module_name name, double state) 
    : sf::sf_integrator(name, state), set_in("set_in"), val_in("val_in")
{
    last_set_in = false;
}

const char* sf::sf_de_integrator::kind() const{
    return sf::sf_de_integrator_kind.c_str();
}

bool sf::sf_de_integrator::execute_updates() {
    if(last_set_in != set_in.read()) {
        std::cout << "=========================== Reseting state" << std::endl;
        *cluster_state_var_ptr = val_in.read(); // Reset state
        last_set_in = set_in.read();
        return true;
    }
    return false;
}
