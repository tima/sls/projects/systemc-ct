#include "sc_ct/sf/components/sf_demux.h"

sf::sf_demux::sf_demux(sc_core::sc_module_name name) :
    sf::sf_primitive(name), x("x"), y1("y1"), y2("y2"), ctrl_in("ctrl_in") {
}

const char* sf::sf_demux::kind() const{
    return sf::sf_demux_kind.c_str();
}

bool sf::sf_demux::execute(double t) {
    // We use a var instead of the input value
    // to support the catch_up process
    if(ctrl){
        y1->write(0.0);
        y2->write(x->read());    
    }
    else{
        y1->write(x->read());
        y2->write(0.0);    
    }
    
    return true;
}

// This should be protected but allowed for sf_cluster (friend class)
void sf::sf_demux::set_de_input(std::string name, bool val){
    // No need for name, as we have only one input
    ctrl = val;
}
