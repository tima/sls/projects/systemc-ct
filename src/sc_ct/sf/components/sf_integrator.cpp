#include "sc_ct/sf/components/sf_integrator.h"


sf::sf_integrator::sf_integrator(sc_core::sc_module_name name, double state) :
    sf::sf_integrator_base(name, state), x("x"), y("y") {
}

const char* sf::sf_integrator::kind() const {
    return sf::sf_integrator_kind.c_str();
}

bool sf::sf_integrator::execute(double t) {
    y->write(state);
    return true;
}

double sf::sf_integrator::get_derivative() {
    return x->read();
}
