#ifndef SF_MUX_H
#define SF_MUX_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/components/sf_primitive.h"
#include "sc_ct/sf/communication/sf_de_in_module_if.h"
#include "sc_ct/sf/communication/sf_in.h"
#include "sc_ct/sf/communication/sf_out.h"
#include "sc_ct/sf/communication/sf_de_in.h"

namespace sf {

    class sf_mux: virtual public sf::sf_primitive, virtual public sf::sf_de_in_module_if {
        public: 
            sf::sf_in  x1;
            sf::sf_in x2;
            sf::sf_out y;
            sf::sf_de_in<bool> ctrl_in;

            sf_mux(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_mux"));

            virtual const char* kind() const;

            virtual bool execute(double t = 0);

            // This should be protected but allowed for sf_cluster (friend class)
            virtual void set_de_input(std::string name, bool val);

            friend class Lsf;

        private:
            bool ctrl;
    };
    
}

#endif
