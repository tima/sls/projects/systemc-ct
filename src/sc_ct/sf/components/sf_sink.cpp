#include "sc_ct/sf/components/sf_sink.h"

sf::sf_sink::sf_sink(sc_core::sc_module_name name) 
    : sf::sf_module(name)
{
}

const char* sf::sf_sink::kind() const{
    return sf::sf_sink_kind.c_str();
}
