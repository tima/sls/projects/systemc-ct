#include "sc_ct/sf/components/sf_sampler.h"

sf::sf_sampler::sf_sampler(sc_core::sc_module_name name, 
    sc_core::sc_time sampling_period)
    : sf_ct_de_converter(name), next_sampling_time(0, sc_core::SC_SEC), sampling_period(sampling_period) 
{
}

const char* sf::sf_sampler::kind() const{
    return sf:: sf_sampler_kind.c_str();
}

sc_core::sc_time sf::sf_sampler::get_next_event_time() {
    while (next_sampling_time <= sc_core::sc_time_stamp()) {
        next_sampling_time = next_sampling_time + sampling_period;
    }
    return next_sampling_time;
}

void sf::sf_sampler::report_event() {
    if(sc_core::sc_time_stamp() == next_sampling_time) {
        val_out.write(x->read());
    }
}
