#include "sc_ct/sf/components/sf_adder.h"

sf::sf_adder::sf_adder(sc_core::sc_module_name name) :
    sf::sf_primitive(name), x1("x1"), x2("x2"), y("y") 
{
}

const char* sf::sf_adder::kind() const{
    return sf::sf_adder_kind.c_str();
}

bool sf::sf_adder::execute(double t) {
    y->write(x1->read() + x2->read());
    return true;
}
