#ifndef SF_SOURCE_H
#define SF_SOURCE_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/components/sf_primitive.h"
#include "sc_ct/sf/communication/sf_out.h"

namespace sf {

    // Constant source to be extended to support at least 
    // the waveforms available in sca_lsf::sca_source (SystemC AMS)
    class sf_source: public sf::sf_primitive {
        public:
            sf::sf_out y;

            sf_source(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_source"), double _amplitude = 1);

            virtual const char* kind() const;

            virtual bool execute(double t = 0) ;

            void set_amplitude(double amp);

            double get_amplitude();

        private:
            double amplitude;

    };

}

#endif
