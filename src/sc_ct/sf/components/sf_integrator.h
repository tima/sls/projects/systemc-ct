#ifndef SF_INTEGRATOR_H
#define SF_INTEGRATOR_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/components/sf_integrator_base.h"
#include "sc_ct/sf/communication/sf_in.h"
#include "sc_ct/sf/communication/sf_out.h"

namespace sf {
    class sf_integrator: public sf::sf_integrator_base {
        public: 
            sf::sf_in  x;
            sf::sf_out y;

            sf_integrator(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_integrator"), double state = 0);

            virtual const char* kind() const;

            virtual bool execute(double t = 0);

            virtual double get_derivative();
    };
    
}

#endif
