#ifndef SF_ADDER_H
#define SF_ADDER_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/components/sf_primitive.h"
#include "sc_ct/sf/communication/sf_in.h"
#include "sc_ct/sf/communication/sf_out.h"

namespace sf {
    class sf_adder: public sf::sf_primitive {
        public: 
            sf::sf_in  x1;
            sf::sf_in x2;
            sf::sf_out y;

            sf_adder(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_adder"));

            virtual const char* kind() const;

            virtual bool execute(double t = 0);
    };
}
#endif
