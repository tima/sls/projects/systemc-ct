#ifndef SF_GAIN_H
#define SF_GAIN_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/components/sf_primitive.h"
#include "sc_ct/sf/communication/sf_in.h"
#include "sc_ct/sf/communication/sf_out.h"

namespace sf {
    class sf_gain: public sf::sf_primitive {
        public: 
            sf::sf_in  x;
            sf::sf_out y;

            sf_gain(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_gain"), double _gain = 1);

            virtual const char* kind() const;

            virtual bool execute(double t = 0);

            void set_gain(double _gain);

            double get_gain();

        private:
            double gain;

    };
}

#endif
