#ifndef SF_PRIMITIVE_H
#define SF_PRIMITIVE_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/components/sf_module.h"

namespace sf {

    class sf_primitive: virtual public sf::sf_module {
        public:        
            sf_primitive(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_primitive"));

            virtual const char* kind() const;
    };

}

#endif
