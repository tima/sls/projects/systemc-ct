#include "sc_ct/sf/components/sf_integrator_base.h"


// Avoid object instantiation
sf::sf_integrator_base::sf_integrator_base(sc_core::sc_module_name name, double state) 
    : sf::sf_primitive(name), state(state) 
{

}

bool sf::sf_integrator_base::execute(double t) {
    return true;
}

void sf::sf_integrator_base::set_state(double _state) {
    state = _state;
}

double sf::sf_integrator_base::get_state() {
    return state;
}

double sf::sf_integrator_base::get_derivative() {
    return 0.0;
}

bool sf::sf_integrator_base::execute_updates() {
    return false;
}

void sf::sf_integrator_base::set_cluster_state_var_ptr(double *ptr) {
    cluster_state_var_ptr = ptr;
}

