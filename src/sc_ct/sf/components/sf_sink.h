#ifndef SF_SINK_H
#define SF_SINK_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/components/sf_module.h"

namespace sf {

    // Base class for sink modules
    class sf_sink:  public virtual sf::sf_module {
        public:
            sf_sink(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_sink"));

            virtual const char* kind() const;
    };

}

#endif
