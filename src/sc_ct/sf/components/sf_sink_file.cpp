#include "sc_ct/sf/components/sf_sink_file.h"

sf::sf_sink_file::sf_sink_file(sc_core::sc_module_name name) 
    : sf::sf_sink(name), x("x")
{
    my_file = new std::ofstream();
    my_file->open(name);
}

const char* sf::sf_sink_file::kind() const{
    return sf::sf_sink_file_kind.c_str();
}

bool sf::sf_sink_file::execute(double t) {
    // Just for tests ! 
    // This is the basis of the internal CT dynamics 
    // tracing system
    (*my_file) << t << "\t" << x->read() << std::endl;
    return true;
}
