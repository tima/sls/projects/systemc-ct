#include "sc_ct/sf/components/sf_threshold_detector.h"
  
sf::sf_threshold_detector::sf_threshold_detector(sc_core::sc_module_name name, double threshold, bool report_rising) 
    : sf_ct_de_converter(name), x("x"), threshold(threshold), 
    report_rising(report_rising)
{  
    event_reported = true;
}

const char* sf::sf_threshold_detector::kind() const{
    return sf::sf_threshold_detector_kind.c_str();
}

bool sf::sf_threshold_detector::execute(double t) {
    // Reinit event_reported whenever the detection condition 
    // is not met
    double x = this->x->read();
    bool is_rising = last_x < x;
    bool is_falling = last_x > x;
    if ( (report_rising && x < threshold) ||
        (!report_rising && x > threshold)   ) {
        event_reported = false;
    }

    // Detection condition
    bool rising_and_threshold_crossed = report_rising && is_rising && x >= threshold;
    bool falling_and_threshold_crossed = !report_rising && is_falling && x <= threshold;

    if (!event_reported &&
        ( rising_and_threshold_crossed ||
        falling_and_threshold_crossed ) ){
        return true;
    }

    last_x = x;
    
    return false;
}

void sf::sf_threshold_detector::report_event() {
    if (!event_reported) {
        event_reported = true;
        event_out.write(!event_out.read());
    }
}

void sf::sf_threshold_detector::set_threshold(double threshold) {
    this->threshold = threshold;
}

double sf::sf_threshold_detector::get_threshold() {
    return threshold;
}

void sf::sf_threshold_detector::end_of_elaboration() {
    last_x = x->read();    
}
