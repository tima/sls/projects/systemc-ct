#include "sc_ct/sf/components/sf_mux.h"

sf::sf_mux::sf_mux(sc_core::sc_module_name name) :
    sf::sf_primitive(name), x1("x1"), x2("x2"), y("y"), ctrl_in("ctrl_in")
{

}

const char* sf::sf_mux::kind() const{
    return sf::sf_mux_kind.c_str();
}

bool sf::sf_mux::execute(double t) {
    // We use a var instead of the input value
    // to support the catch_up process
    if(ctrl){
        y->write(x2->read());    
    }
    else{
        y->write(x1->read());    
    }
    
    return true;
}

// This should be protected but allowed for sf_cluster (friend class)
void sf::sf_mux::set_de_input(std::string name, bool val){
    // No need for name, as we have only one input
    this->ctrl = val;
}
