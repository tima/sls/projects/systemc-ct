#include "sc_ct/sf/components/sf_gain.h"


sf::sf_gain::sf_gain(sc_core::sc_module_name name, double _gain) 
    : sf::sf_primitive(name), x("x"), y("y")
{    
    gain = _gain;
}

const char* sf::sf_gain::kind() const{
    return sf::sf_gain_kind.c_str();
}

bool sf::sf_gain::execute(double t) {
    y->write(gain * x->read());
    return true;
}

void sf::sf_gain::set_gain(double _gain) {
    gain = _gain;
}

double sf::sf_gain::get_gain() {
    return gain;
}
