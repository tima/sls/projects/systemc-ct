#ifndef SF_DE_INTEGRATOR_H
#define SF_DE_INTEGRATOR_H

#include <systemc>
#include "sc_ct/sf/common/sf_common.h"
#include "sc_ct/sf/components/sf_integrator.h"

namespace sf {
    class sf_de_integrator: public sf::sf_integrator {
        public: 
            // Inputs for resetting state
            //SHould be false at the start of the simulation
            sc_core::sc_in<bool> set_in; 
            sc_core::sc_in<double> val_in;


            sf_de_integrator(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sf_integrator"), double state = 0);

            virtual const char* kind() const;

            bool execute_updates();

        private:
            // To identify when the set signal changes
            bool last_set_in;
    };
}

#endif
