#ifndef SF_H
#define SF_H

////////////////////////////////////////////////////////////////////////
// MoC classes
////////////////////////////////////////////////////////////////////////

// Eq. primitives
#include "components/sf_adder.h"
#include "components/sf_integrator.h"
#include "components/sf_de_integrator.h"
#include "components/sf_gain.h"
#include "components/sf_source.h"
#include "components/sf_mux.h"
#include "components/sf_demux.h"

// Sinks
#include "components/sf_sink_file.h"


// CT/DE converters
#include "components/sf_threshold_detector.h" 
#include "components/sf_sampler.h" 
#include "components/sf_de_threshold_detector.h" 


// Channels
#include "communication/sf_de_in_module_if.h"
#include "communication/sf_de_in.h"
#include "communication/sf_in_if.h"
#include "communication/sf_in.h"
#include "communication/sf_out_if.h"
#include "communication/sf_out.h"
#include "communication/sf_signal.h"

// Cluster
#include "cluster/sf_cluster.h"

#endif
