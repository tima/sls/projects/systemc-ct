#ifndef SF_COMMON_H
#define SF_COMMON_H

#include<string>

namespace sf { 

    class sf_cluster;
    class sf_module;

    // Primitives
    class sf_primitive;
    class sf_adder;
    class sf_integrator_base;
    class sf_integrator;
    class sf_de_integrator;
    class sf_gain;
    class sf_source;
    class sf_mux;
    class sf_demux;

    // 
    class sf_de_in_module_if;

    // Sinks
    class sf_sink;
    class sf_sink_file;

    // CT/DE converters
    class sf_ct_de_converter;
    class sf_threshold_detector;
    class sf_de_threshold_detector;
    class sf_sampler;

    // Ports and channels
    class sf_in_if;
    class sf_out_if;
    class sf_signal;
    class sf_in;
    class sf_out;

    template<class T>
    class sf_de_in;

    // Kinds
    const std::string sf_module_kind = "sf_module";
    const std::string sf_integrator_kind = "sf_integrator";
    const std::string sf_de_integrator_kind = "sf_de_integrator";
    const std::string sf_adder_kind = "sf_adder";
    const std::string sf_gain_kind = "sf_gain";
    const std::string sf_primitive_kind = "sf_primitive";
    const std::string sf_signal_kind = "sf_signal";
    const std::string sf_in_kind = "sf_in";
    const std::string sf_out_kind = "sf_out";
    const std::string sf_source_kind = "sf_source";
    const std::string sf_sink_kind = "sf_sink";
    const std::string sf_ct_de_converter_kind = "sf_ct_de_converter";
    const std::string sf_threshold_detector_kind = "sf_threshold_detector";
    const std::string sf_de_threshold_detector_kind = "sf_de_threshold_detector";
    const std::string sf_sink_file_kind = "sf_sink_file";
    const std::string sf_mux_kind = "sf_mux";
    const std::string sf_demux_kind = "sf_demux";
    const std::string sf_de_in_kind = "sf_de_in";
    const std::string sf_sampler_kind = "sf_sampler";

    // Types
    typedef std::size_t id_t;

}

#endif