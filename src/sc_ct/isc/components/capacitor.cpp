#include "sc_ct/isc/components/capacitor.h"

ISC::capacitor::capacitor(sc_core::sc_module_name name, double val)
    : ISC::circuit_element(name, ISC::capacitor_el, val)
{
}

const char* ISC::capacitor::kind() const {
    return capacitor_kind.c_str();
}
