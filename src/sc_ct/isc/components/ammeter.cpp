#include "sc_ct/isc/components/ammeter.h"

ISC::ammeter::ammeter(sc_core::sc_module_name name)
    : ISC::circuit_element(name, ISC::ammeter_el, 1), 
    signal_out("signal_out")
{
}

const char* ISC::ammeter::kind() const {
    return ammeter_kind.c_str();
}
