#include "sc_ct/isc/components/diode.h"

ISC::diode::diode(sc_core::sc_module_name name, bool is_on)
    : ISC::ideal_switch(name, ISC::diode_el, is_on)
{
}

const char* ISC::diode::kind() const {
    return diode_kind.c_str();
}
