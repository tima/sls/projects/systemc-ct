#ifndef DIODE_H
#define DIODE_H

#include <systemc>

#include "sc_ct/isc/components/ideal_switch.h"

class ISC::diode : virtual public ISC::ideal_switch {

    public: 
        diode(sc_core::sc_module_name name = sc_core::sc_gen_unique_name(diode_kind.c_str()), bool = true);

        const char* kind() const;
    
    private:
};

#endif
