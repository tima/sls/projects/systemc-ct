#ifndef INDUCTOR_H
#define INDUCTOR_H

#include <systemc>

#include "sc_ct/isc/components/circuit_element.h"

class ISC::inductor : virtual public ISC::circuit_element {

    public: 
        inductor(sc_core::sc_module_name name = sc_core::sc_gen_unique_name(inductor_kind.c_str()), double val = 1.0);

        const char* kind() const;
    
    private:
};

#endif
