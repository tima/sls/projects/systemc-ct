#ifndef V_SOURCE_H
#define V_SOURCE_H

#include <systemc>

#include "sc_ct/isc/components/independent_source.h"

class ISC::v_source : public ISC::internal::independent_source {

    public: 
        v_source(sc_core::sc_module_name name = sc_core::sc_gen_unique_name(v_source_kind.c_str()), double value = 1.0);

        const char* kind() const;
    
    private:
};

#endif
