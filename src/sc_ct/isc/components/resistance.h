#ifndef RESISTANCE_H
#define RESISTANCE_H

#include <systemc>

#include "sc_ct/isc/components/circuit_element.h"

class ISC::resistance : virtual public ISC::circuit_element {

    public: 
        resistance(sc_core::sc_module_name name = sc_core::sc_gen_unique_name(resistance_kind.c_str()), double val = 1.0);

        const char* kind() const;
    
    private:
};

#endif
