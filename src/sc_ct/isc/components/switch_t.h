#ifndef SWITCH_T_H
#define SWITCH_T_H

#include <systemc>

#include "sc_ct/isc/components/ideal_switch.h"
#include "sc_ct/isc/communication/de_in_module_if.h"
#include "sc_ct/isc/communication/de_in.h"

class ISC::switch_t : virtual public ISC::ideal_switch, virtual public ISC::internal::de_in_module_if {

    public: 
        de_in<bool> ctrl_in;

        switch_t(sc_core::sc_module_name name = sc_core::sc_gen_unique_name(switch_t_kind.c_str()), bool = true);

        const char* kind() const;
    
        void end_of_elaboration();
    private:
};

#endif
