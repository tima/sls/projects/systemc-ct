#include "sc_ct/isc/components/voltmeter.h"

ISC::voltmeter::voltmeter(sc_core::sc_module_name name)
    : ISC::circuit_element(name, ISC::voltmeter_el, 1), 
    signal_out("signal_out")
{
}

const char* ISC::voltmeter::kind() const {
    return voltmeter_kind.c_str();
}
