#include "sc_ct/isc/components/dependent_source.h"


ISC::internal::dependent_source::dependent_source(sc_core::sc_module_name name, ISC::element _element, double factor)
    : ISC::circuit_element(name, _element, factor)
{
}

double ISC::internal::dependent_source::get_factor() const {
    return ISC::circuit_element::get_value();
}
