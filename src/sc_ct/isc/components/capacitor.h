#ifndef CAPACITOR_H
#define CAPACITOR_H

#include <systemc>

#include "sc_ct/isc/components/circuit_element.h"

class ISC::capacitor : virtual public ISC::circuit_element {

    public: 
        capacitor(sc_core::sc_module_name name = sc_core::sc_gen_unique_name(capacitor_kind.c_str()), double val = 1.0);

        const char* kind() const;
    
    private:
};

#endif
