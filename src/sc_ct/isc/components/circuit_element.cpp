#include "sc_ct/isc/components/circuit_element.h"

ISC::circuit_element::circuit_element(sc_core::sc_module_name name, 
    ISC::element _element, double value)
    : ISC::module(name), terminal_a("terminal_a", positive), terminal_b("terminal_b", negative), element(_element)
{
    if (!set_value(value)) {
        set_value(ISC_CIRCUIT_ELEMENT_DEFAULT_VALUE);
    }
}

const char* ISC::circuit_element::kind() const{
    return ISC::circuit_element_kind.c_str();
}

ISC::element ISC::circuit_element::get_element() const {
    return element;
}

bool ISC::circuit_element::set_value(double new_value) {
    // Allow negative values flag
    bool allow_negative_value = true;

    // Signal that the element does not 
    // allow negative values
    switch (get_element()) {
        case resistance_el:
        case inductor_el:
        case capacitor_el:
            allow_negative_value = false;
            break;
        default:
            break;
    }

    // If it does not allow negative values 
    // and the new value is negative
    // do not modify the current value
    if (!allow_negative_value && new_value < 0) {
        return false;
    }

    // Set the new value
    value = new_value;
    return true;
}

double ISC::circuit_element::get_value() const {
    return value;
}
