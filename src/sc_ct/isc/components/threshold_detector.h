#ifndef ISC_THRESHOLD_DETECTOR_H
#define ISC_THRESHOLD_DETECTOR_H

#include <systemc>
#include "sc_ct/isc/common/common.h"
#include "sc_ct/isc/communication/ct_in.h"

class ISC::threshold_detector : virtual public ISC::module {

    public: 
        ISC::ct_in signal_in;
        sc_core::sc_out<bool> event_out;        
        
        threshold_detector(sc_core::sc_module_name name = sc_core::sc_gen_unique_name(ISC::signal_kind.c_str()), 
         double threshold = 1, bool report_rising = true);

        const char* kind() const;

        virtual bool is_crossing(double t, double x);

        virtual void generate_outputs();

        void set_threshold(double threshold);

        double get_threshold() const;

        void set_last_val(double x);

    protected:


    private:
        double threshold, last_x;
        bool event_reported;
        bool report_rising;
};

#endif
