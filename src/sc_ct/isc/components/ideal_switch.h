#ifndef IDEAL_SWITCH_H
#define IDEAL_SWITCH_H

#include <systemc>

#include "sc_ct/isc/components/circuit_element.h"

class ISC::ideal_switch : public ISC::circuit_element {

    public:    
        bool is_on() const;
        bool set_state(bool);


        ideal_switch(sc_core::sc_module_name name, ISC::element element, bool);

    private:
        bool state;


};

#endif
