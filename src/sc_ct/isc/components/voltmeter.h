#ifndef VOLTMETER_H
#define VOLTMETER_H

#include <systemc>

#include "sc_ct/isc/components/circuit_element.h"
#include "sc_ct/isc/communication/ct_out.h"

class ISC::voltmeter : virtual public ISC::circuit_element {
    public: 
        ISC::ct_out signal_out;

        voltmeter(sc_core::sc_module_name name = sc_core::sc_gen_unique_name(voltmeter_kind.c_str()));

        const char* kind() const;
    
    private:
};

#endif
