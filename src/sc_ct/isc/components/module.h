#ifndef ISC_MODULE_H
#define ISC_MODULE_H

#include <set>
#include <systemc>
#include "sc_ct/isc/common/common.h"

#include "sc_ct/isc/communication/node.h"


typedef std::pair<ISC::id_type, ISC::polarity> node_polarity_pair;
typedef std::set<node_polarity_pair> node_polarity_set;

class ISC::module : virtual public sc_core::sc_module {
    public:
        // Class static variable to count the number of 
        // sf modules that have been instantiated
        static size_t count;
        
        module(sc_core::sc_module_name = sc_core::sc_gen_unique_name(module_kind.c_str()));

        ISC::id_type get_id() const;

        virtual const char* kind() const;

        const node_polarity_set &get_nodes_with_polarities() const;
        ISC::id_type get_node_id_by_polarity(ISC::polarity) const; 

    private:
        ISC::id_type id;

        friend class ISC::terminal;
        
        // Nodes to which the element is connected
        void connect_node(const ISC::node &, ISC::polarity polarity);
        node_polarity_set nodes;

};

#endif
