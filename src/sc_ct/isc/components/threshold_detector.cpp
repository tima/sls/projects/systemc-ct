#include "sc_ct/isc/components/threshold_detector.h"


ISC::threshold_detector::threshold_detector(
    sc_core::sc_module_name name, 
    double threshold,
    bool report_rising) 
    : ISC::module(name), threshold(threshold), report_rising(report_rising)
{
    event_reported = true;
}

const char* ISC::threshold_detector::kind() const {
    return ISC::threshold_detector_kind.c_str();
}

bool ISC::threshold_detector::is_crossing(double t, double x) {

    // Reinit event_reported whenever the detection condition 
    // is not met
    bool is_rising = last_x < x;
    bool is_falling = last_x > x;
    if ( (report_rising && x < threshold) ||
            (!report_rising && x > threshold)   ) {
        event_reported = false;
    }

    // Detection condition
    bool rising_and_threshold_crossed = report_rising && is_rising && x >= threshold;
    bool falling_and_threshold_crossed = !report_rising && is_falling && x <= threshold;

    if (!event_reported &&
        ( rising_and_threshold_crossed ||
            falling_and_threshold_crossed ) ){
        return true;
    }

    last_x = x;
    
    return false;
}

void ISC::threshold_detector::generate_outputs() {
    if (!event_reported) {
        event_reported = true;
        event_out.write(!event_out.read());
    }
}

void ISC::threshold_detector::set_threshold(double threshold) {
    this->threshold = threshold;
}

double ISC::threshold_detector::get_threshold() const {
    return threshold;
}

void ISC::threshold_detector::set_last_val(double x) {
    last_x = x;
}
