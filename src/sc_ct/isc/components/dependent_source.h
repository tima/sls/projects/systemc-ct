#ifndef DEPENDENT_SOURCE_H
#define DEPENDENT_SOURCE_H

#include "sc_ct/isc/components/circuit_element.h"

class ISC::internal::dependent_source : public ISC::circuit_element {

    public: 
        dependent_source(sc_core::sc_module_name name, ISC::element _element, double factor);
        const char* kind() const = 0;
    
        double get_factor() const;

    private:
};


#endif 
