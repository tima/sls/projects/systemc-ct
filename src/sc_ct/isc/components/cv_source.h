#ifndef CV_SOURCE_H
#define CV_SOURCE_H

#include <systemc>

#include "sc_ct/isc/components/dependent_source.h"
#include "sc_ct/isc/communication/ct_in.h"

class ISC::cv_source : public ISC::internal::dependent_source {

    public: 
        ISC::ct_in signal_in;

        cv_source(sc_core::sc_module_name name = sc_core::sc_gen_unique_name(cv_source_kind.c_str()), double factor = 1.0);

        const char* kind() const;
    
    private:
};

#endif
