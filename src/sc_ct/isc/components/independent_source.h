#ifndef INDEPENDENT_SOURCE_H
#define INDEPENDENT_SOURCE_H

#include "sc_ct/isc/components/circuit_element.h"

class ISC::internal::independent_source : public ISC::circuit_element {

    public: 
        independent_source(sc_core::sc_module_name name, ISC::element _element, double value);
        const char* kind() const = 0;

        double get_value(double t = 0);
    
    private:
};


#endif 
