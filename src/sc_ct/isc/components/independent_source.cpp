#include "sc_ct/isc/components/independent_source.h"


ISC::internal::independent_source::independent_source(sc_core::sc_module_name name, ISC::element _element, double value)
    : ISC::circuit_element(name, _element, value)
{
}

double ISC::internal::independent_source::get_value(double t) {
    return ISC::circuit_element::get_value();
}
