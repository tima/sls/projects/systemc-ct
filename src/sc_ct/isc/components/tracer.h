#ifndef ISC_TRACER_H
#define ISC_TRACER_H

#include <systemc>
#include "sc_ct/isc/common/common.h"
#include "sc_ct/isc/communication/ct_in.h"

class ISC::tracer : virtual public ISC::module {
    public: 
        ISC::ct_in signal_in;
        
        tracer(sc_core::sc_module_name name = sc_core::sc_gen_unique_name(ISC::signal_kind.c_str()));

        const char* kind() const {
            return ISC::tracer_kind.c_str();
        }
        
    private:
};

#endif
