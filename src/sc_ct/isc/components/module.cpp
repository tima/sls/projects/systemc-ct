#include "sc_ct/isc/components/module.h"

ISC::id_type ISC::module::count = 0;

ISC::module::module(sc_core::sc_module_name name)
    : sc_core::sc_module(name)
{
    id = count++;
}

ISC::id_type ISC::module::get_id() const {
    return id;
}

const char* ISC::module::kind() const {
    return ISC::module_kind.c_str();
}

void ISC::module::connect_node(const ISC::node &node, ISC::polarity polarity) {
    if (nodes.size() == 2) {
        throw std::domain_error("Triying to connect element to more than two nodes");
    }
    
    auto node_polarity = std::make_pair(node.get_id(), polarity);

    bool connected = nodes.insert(node_polarity).second;

    if (!connected) {
        throw std::domain_error("Trying to connect the two element ports to the same branch. Self-loops are not allowed.");
    }

}

const node_polarity_set &ISC::module::get_nodes_with_polarities() const {
    return nodes;
}

ISC::id_type ISC::module::get_node_id_by_polarity(
        ISC::polarity desired_polarity) const 
{
    //for (auto [node,polarity]: nodes) {
    for (auto n: nodes) {
		ISC::id_type node = n.first;
		ISC::polarity polarity = n.second;
        if (polarity == desired_polarity) {
            return node;
        }
    }
    throw std::domain_error("The element has no nodes connected to its terminal with the desired polarity");
}
