#include "sc_ct/isc/components/cc_source.h"

ISC::cc_source::cc_source(sc_core::sc_module_name name, double value)
    : ISC::internal::dependent_source(name, ISC::cc_source_el, value)
{
}

const char* ISC::cc_source::kind() const {
    return cc_source_kind.c_str();
}
