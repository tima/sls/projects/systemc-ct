#include "sc_ct/isc/components/inductor.h"

ISC::inductor::inductor(sc_core::sc_module_name name, double val)
    : ISC::circuit_element(name, ISC::inductor_el, val)
{
}

const char* ISC::inductor::kind() const {
    return inductor_kind.c_str();
}
