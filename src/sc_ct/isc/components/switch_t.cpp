#include "sc_ct/isc/components/switch_t.h"

ISC::switch_t::switch_t(sc_core::sc_module_name name, bool is_on)
    : ISC::ideal_switch(name, switch_el, is_on), ctrl_in("ctrl_in")
{
}

const char* ISC::switch_t::kind() const {
    return switch_t_kind.c_str();
}

void ISC::switch_t::end_of_elaboration() {
    
    // Set initial assumed value for switch ctrl port from DE domain
    // This value is used to generate the initial topology
    // and then the real value of the port is used to 
    // get the right topology at end of elaboration of the whole cluster
    for( auto port_ptr : get_de_boolean_ports() ) {
        set_de_input(port_ptr, is_on());
    }

    // ISC::ideal_switch::end_of_elaboration();
}
