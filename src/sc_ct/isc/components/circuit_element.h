#ifndef CIRCUIT_ELEMENT_H
#define CIRCUIT_ELEMENT_H

#include <systemc>

#include "sc_ct/isc/components/module.h"
#include "sc_ct/isc/communication/terminal.h"

#define  ISC_CIRCUIT_ELEMENT_DEFAULT_VALUE 1.0

class ISC::circuit_element : virtual public ISC::module {

    public: 
        ISC::terminal terminal_a;
        ISC::terminal terminal_b;

        circuit_element(sc_core::sc_module_name name = sc_core::sc_gen_unique_name(circuit_element_kind.c_str()), 
        ISC::element _element = resistance_el, 
        double value = ISC_CIRCUIT_ELEMENT_DEFAULT_VALUE);

        const char* kind() const;
    
        bool set_value(double);
        double get_value() const;

        ISC::element get_element() const;

    protected:
        ISC::element element;

    private:
        // Parameter value
        double value;

};

#endif
