#include "sc_ct/isc/components/resistance.h"

ISC::resistance::resistance(sc_core::sc_module_name name, double val)
    : ISC::circuit_element(name, ISC::resistance_el, val)
{
}

const char* ISC::resistance::kind() const {
    return resistance_kind.c_str();
}
