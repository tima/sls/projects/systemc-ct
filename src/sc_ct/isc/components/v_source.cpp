#include "sc_ct/isc/components/v_source.h"

ISC::v_source::v_source(sc_core::sc_module_name name, double value)
    : ISC::internal::independent_source(name, ISC::ind_v_source_el, value)
{
}

const char* ISC::v_source::kind() const {
    return v_source_kind.c_str();
}
