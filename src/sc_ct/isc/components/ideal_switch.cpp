#include "sc_ct/isc/components/ideal_switch.h"

ISC::ideal_switch::ideal_switch(sc_core::sc_module_name name, ISC::element _element, bool is_on)
    : ISC::circuit_element(name, _element, 0)
{
        set_state(is_on);
}

bool ISC::ideal_switch::is_on() const
{
    return state;
}

bool ISC::ideal_switch::set_state(bool val)
{
    return (state = val);
    
}
