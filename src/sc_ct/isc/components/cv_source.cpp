#include "sc_ct/isc/components/cv_source.h"

ISC::cv_source::cv_source(sc_core::sc_module_name name, double value)
    : ISC::internal::dependent_source(name, ISC::cv_source_el, value)
{
}

const char* ISC::cv_source::kind() const {
    return cv_source_kind.c_str();
}
