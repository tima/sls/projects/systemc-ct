#ifndef ISC_COMMON_H
#define ISC_COMMON_H


#include <cstddef>
#include <string>
#include <ostream>

namespace ISC { 
    class cluster;
    class circuit_element;
    class module;
    class terminal;
    class node;
    class terminal_if;
    class resistance;
    class capacitor;
    class inductor;
    class diode;
    class switch_t;
    class v_source;
    class ideal_switch;
    class voltmeter;
    class ammeter;
    class signal;
    class ct_in_if;
    class ct_out_if;
    class ct_in;
    class ct_out;
    class tracer;
    class cv_source;
    class cc_source;
    class threshold_detector;
    
    template<class T>
    class de_in;

    const std::string cluster_kind = "ISC::cluster";
    const std::string module_kind = "ISC::module";
    const std::string circuit_element_kind = "ISC::circuit_element";
    const std::string terminal_kind = "ISC::terminal";
    const std::string node_kind = "ISC::node";
    const std::string terminal_if_kind = "ISC::terminal_if";
    const std::string resistance_kind = "ISC::resistance";
    const std::string capacitor_kind = "ISC::capacitor";
    const std::string inductor_kind = "ISC::inductor";
    const std::string diode_kind = "ISC::diode";
    const std::string switch_t_kind = "ISC::switch_t";
    const std::string v_source_kind = "ISC::v_source";
    const std::string c_source_kind = "ISC::c_source";
    const std::string cv_source_kind = "ISC::cv_source";
    const std::string cc_source_kind = "ISC::cc_source";
    const std::string de_in_kind = "ISC::de_in_kind";
    const std::string voltmeter_kind = "ISC::voltmeter_kind";
    const std::string ammeter_kind = "ISC::ammeter_kind";
    const std::string signal_kind = "ISC::signal_kind";
    const std::string ct_in_kind = "ISC::ct_in_kind";
    const std::string ct_out_kind = "ISC::ct_out_kind";
    const std::string tracer_kind = "ISC::tracer_kind";
    const std::string threshold_detector_kind = "ISC::threshold_detector_kind";


    enum polarity {
        positive,
        negative
    };

    enum element {
        capacitor_el, 
        inductor_el, 
        resistance_el, 
        ind_v_source_el,
        ind_c_source_el,
        cv_source_el,
        cc_source_el, 
        switch_el,
        diode_el,
        voltmeter_el,
        ammeter_el,
    };

    typedef std::size_t id_type;

    // Internal namespace, 
    // not to be used by the user 
    namespace internal {
        class branch;
        class label;
        class labels;
        class branch_law_term;
        class branch_generator;
        class topology_selector;
        class de_in_module_if;
        class independent_source;
        class dependent_source;
        class branches;
        class cutset_matrix;
        class incidence_matrix;
        class ss_matrix;
        class tableau_matrix;
        class topological_matrix;

        enum branch_type {
            a_port_in_tree, 
            b_port_in_cotree,
            z_nonport_in_tree,
            y_nonport_in_cotree
        };

        enum col_type {
            current_t,
            voltage_t,
        };

        // Type of variable in state equation
        // According to Massarini & Reggiani (2001)
        enum state_space_type {
            nonport_var,
            // Response vars of source ports
            // current for voltage ports and
            // voltage for current ports
            response_source_var,  
            // Ideal switches and diodes
            switch_var, 
            // Outputs
            // short circuits (for currents)
            // and open circuits (for voltages) 
            output_var,
            // State derivative
            // dvc_dt and dil_dt
            derivative_var, 
            // State
            // vc, il
            state_var,
            // Zero valued port variables
            // switches currents or voltages and
            // diodes currents or voltages
            // depending on the state (ON/OFF)
            zero_port_var, 
            // Input sources 
            source_var
        };

        const std::size_t THRESHOLD_CROSSING_EVENT = 0;

        std::ostream& operator<<(std::ostream&, const ISC::internal::branch&);
    }

}

#endif