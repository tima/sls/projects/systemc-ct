#ifndef BRANCH_GENERATOR_H 
#define BRANCH_GENERATOR_H

#include "sc_ct/isc/common/common.h"
#include "sc_ct/isc/components/circuit_element.h"
#include "sc_ct/isc/components/dependent_source.h"
#include "sc_ct/isc/equation_generator/branch.h"
#include "sc_ct/isc/components/ideal_switch.h"

class ISC::internal::branch_generator {

    public:
        branch_generator();

        ISC::internal::branch generate_branch(const ISC::circuit_element &) const;

        void add_external_law_term(ISC::internal::branch &, const ISC::internal::branch &, const ISC::internal::dependent_source &) const;

    private:
        std::set<ISC::element> port_elements;

        bool is_port(const ISC::circuit_element &) const;
        bool is_on(const ISC::circuit_element &) const; 
        void add_own_law_term(ISC::internal::branch &, const ISC::circuit_element &) const;

};

#endif
