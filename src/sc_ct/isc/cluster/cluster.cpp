#include "sc_ct/isc/cluster/cluster.h"


ISC::cluster::cluster(sc_core::sc_module_name name,
            bool use_adaptive, bool avoid_rollback, 
            double sync_step) 
    : sct_core::ct_module(name), 
    _branches(),
    use_adaptive(use_adaptive), 
    avoid_rollback(avoid_rollback),
    sync_step(sync_step),
    next_sampling_time(0, sc_core::SC_SEC)
{
    #ifdef PROVIDE_STATISTICS
        is_event_sim_time = 0;
        get_derivatives_sim_time = 0;
        execute_updates_sim_time = 0;
        generate_outputs_sim_time = 0;
        elaboration_time = 0;
    #endif
}

void ISC::cluster::end_of_elaboration() {

    #ifdef PROVIDE_STATISTICS
        clock_t _t = clock();
    #endif

    // Notice that ct_module::end_of_elaboration 
    // calls set_initial_conditions, 
    // which ensures that the lines that follow 
    // are correctly executed (usage of x)
    sct_core::ct_module::end_of_elaboration();


    store_elements_and_nodes();
    create_node_row_map();
    create_branches_and_incidence_matrix();

    // init topology selector
    create_signal_reader_writer_map();
    process_dependent_sources();
    _topology_selector.init(matrix, _branches, labels);
    create_output_positions_map();
    create_tracer_mask();

    //  // Use current inputs
    process_de_inputs(0, false);

    #ifdef PROVIDE_STATISTICS
        _t = clock() - _t;
        elaboration_time += ((double) _t) / CLOCKS_PER_SEC;
    #endif
}

void ISC::cluster::add_to_set_of_module_with_de_inputs(sc_core::sc_object *obj) {
    ISC::internal::de_in_module_if *ptr = dynamic_cast<ISC::internal::de_in_module_if *>(obj);

    if (ptr) {
        modules_with_de_inputs.insert(ptr);
    }
}

void ISC::cluster::add_to_set_of_ind_sources(sc_core::sc_object *obj) {
    ISC::internal::independent_source *ptr = dynamic_cast<ISC::internal::independent_source *>(obj);

    if (ptr) {
        ind_sources.insert(ptr);
    }
}

void ISC::cluster::add_to_set_of_threshold_detectors(sc_core::sc_object *obj) {
    ISC::threshold_detector *ptr = dynamic_cast<ISC::threshold_detector *>(obj);

    if (ptr) {
        threshold_detectors.insert(ptr);
    }
}


void ISC::cluster::add_to_set_of_dep_sources(sc_core::sc_object *obj) {
    ISC::internal::dependent_source *ptr = dynamic_cast<ISC::internal::dependent_source *>(obj);

    if (ptr) {
        dep_sources.insert(ptr);
    }
}

void ISC::cluster::store_elements_and_nodes() {
    // Traverse all child objects
    std::vector<sc_core::sc_object *> children = get_child_objects();
    for (auto obj: children) {
        add_to_set_of_circuit_elements(obj);
        add_to_set_of_ind_sources(obj);
        add_to_set_of_dep_sources(obj);
        add_to_set_of_nodes(obj);
        add_to_set_of_tracers(obj);
        add_to_set_of_signals(obj);
        add_to_set_of_threshold_detectors(obj);
        add_to_set_of_module_with_de_inputs(obj);
    }
}

void ISC::cluster::add_to_set_of_circuit_elements(sc_core::sc_object *obj) {
    ISC::circuit_element *ptr = dynamic_cast<ISC::circuit_element *>(obj);

    if (ptr) {
        circuit_elements.insert(ptr);
    }
}

void ISC::cluster::add_to_set_of_nodes(sc_core::sc_object *obj) {
    ISC::node *ptr = dynamic_cast<ISC::node *>(obj);

    if (ptr) {
        nodes.insert(ptr);
    }
}

void ISC::cluster::add_to_set_of_signals(sc_core::sc_object *obj) {
    ISC::signal *ptr = dynamic_cast<ISC::signal *>(obj);

    if (ptr) {
        signals.insert(ptr);
    }
}


void ISC::cluster::add_to_set_of_tracers(sc_core::sc_object *obj) {
    ISC::tracer *ptr = dynamic_cast<ISC::tracer *>(obj);

    if (ptr) {
        tracers.insert(ptr);
    }
}

void ISC::cluster::create_node_row_map() {
    static unsigned row = 0;
    for (auto node_ptr: nodes) {
        node_row_map[node_ptr->get_id()] = row++;
    }
}

void ISC::cluster::create_branches_and_incidence_matrix() {
    static unsigned col = 0;
    
    //
    initialize_matrix();

    // Traverse all circuit elements
    for (auto ptr: circuit_elements) {
        // // Create branch and store it in set of branches
        create_branch(*ptr);
        
        // // Map branch id to incidence matrix column
        // branch_col_map[branch.get_id()] = col++;
        branch_col_map[ptr->get_id()] = col++;
        fill_matrix(*ptr);
    }

    remove_row(matrix, 0);

    // std::cout << "Matrix is \n" << matrix << std::endl;

    // std::cout << "branches are \n";
    // for (auto [id, branch]: branches.get_branches()) {
    //     std::cout << branch << std::endl;
    // }
}

// Fill incidence matrix according to certain convention
void ISC::cluster::fill_matrix(const ISC::circuit_element &element) {

    unsigned positive_node_row = get_node_row(element, positive);
    unsigned negative_node_row = get_node_row(element, negative);
    unsigned column = get_column(element);

    // Branch currents go from the assumed positive to the 
    // negative terminal of a branch

    // Set entry 1 if the branch current goes out of a node 
    matrix(positive_node_row, column) = 1;
    // Set entry -1 if the branch current goes in to a node
    matrix(negative_node_row, column) = -1;

    // Entries that correspond to the nodes that are not touched by a branch
    // are zero. 
}

void ISC::cluster::initialize_matrix() {
    unsigned nodes_count = nodes.size();
    unsigned branches_count = circuit_elements.size();
    // The incidence matrix has with n rows and b columns, 
    // where n is the number of nodes and b the number
    // of branches.
    // We will remove the last row, which is linearly dependent 
    // later on.
    matrix = MatrixXd::Zero(nodes_count, branches_count);
}

unsigned ISC::cluster::get_node_row(const ISC::circuit_element &element,    
            ISC::polarity polarity)
{
    ISC::id_type node_id = element.get_node_id_by_polarity(polarity);
    return node_row_map[node_id];
}

unsigned ISC::cluster::get_column(const ISC::circuit_element &element) {
    return branch_col_map[element.get_id()];
}

void ISC::cluster::create_branch(const ISC::circuit_element &element) {
    ISC::internal::branch branch = branch_generator.generate_branch(element);
    /// branches.push_back(branch);
    _branches.add_branch(branch);
    labels.push_back(label(branch.get_id(), voltage_t));
    element_branch_ids_map[element.get_id()] = branch.get_id();
    branch_element_ids_map[branch.get_id()] = element.get_id();
}

void ISC::cluster::remove_row(Eigen::MatrixXd& matrix, unsigned int rowToRemove)
{
    unsigned int numRows = matrix.rows()-1;
    unsigned int numCols = matrix.cols();

    if( rowToRemove < numRows )
        matrix.block(rowToRemove,0,numRows-rowToRemove,numCols) = matrix.block(rowToRemove+1,0,numRows-rowToRemove,numCols);

    matrix.conservativeResize(numRows,numCols);
}



void ISC::cluster::set_sync_parameters(){  
    sync_step = sync_step <= 0 ? DELTA_T : sync_step;
    
    set_max_timestep(sync_step);
    use_adaptive_sync(use_adaptive);
    avoid_rollbacks(avoid_rollback);
}

void ISC::cluster::set_initial_conditions(){
    x.resize(2);
    x[0] = 0.0;
    x[1] = 0.0;
}

bool ISC::cluster::process_de_inputs(double t, bool use_input_checkpoints) {
    ISC::id_type id;

    // Set the mode of the inputs manager to either current_values or checkpoint
    inputs.use_checkpoints(use_input_checkpoints);

    std::vector<ISC::id_type> switch_branches;

    // Iterate over components with DE inputs
    for (auto module_ptr: modules_with_de_inputs) {
        for (auto port_ptr: module_ptr->get_de_boolean_ports()) {
            // Get the id of the branch that corresponds to 
            // the switching component 
            id = element_branch_ids_map[module_ptr->get_id()]; 
            // Store the pair <id, state> in the map.
            // if the state has changed 
            if (module_ptr->set_de_input(port_ptr, inputs[*port_ptr])) {
                switch_branches.push_back(id);
            }
        }
    }

    bool result =  _topology_selector.set_switch_states(switch_branches, t);

    // Pass the map to the topology selector
    return result;
}


void ISC::cluster::get_derivatives(bool use_input_checkpoints,
    const sct_core::ct_state &x , sct_core::ct_state &dxdt , double t ){


   #ifdef PROVIDE_STATISTICS
        clock_t _t = clock(); 
    #endif


    // Necessary because of rollbacks
    process_de_inputs(t, use_input_checkpoints);

    sct_core::ct_state u = get_u(use_input_checkpoints, t);



    dxdt = _topology_selector.get_derivatives(x,u);


    #ifdef PROVIDE_STATISTICS
        _t = clock() - _t;
        get_derivatives_sim_time += ((double) _t) / CLOCKS_PER_SEC;
    #endif
}


std::unordered_map<int, bool> ISC::cluster::is_event(const sct_core::ct_state &x, double t) {


    std::unordered_map<int, bool> events;
    
    std::vector<ISC::id_type> switches_to_toggle;
    

    sct_core::ct_state u = get_u(false, t);

    /////////// Events produced by threshold detectors 
    if (is_crossing_event(x, u, t)) {
        events[ISC::internal::THRESHOLD_CROSSING_EVENT] = true;
    }
    //////////


    #ifdef PROVIDE_STATISTICS
        clock_t _t = clock();
    #endif
    switches_to_toggle = _topology_selector.get_internal_switches_to_toggle(x, u, t);

    #ifdef PROVIDE_STATISTICS
        _t = clock() - _t;
        is_event_sim_time += ((double) _t) / CLOCKS_PER_SEC;
    #endif

    // Map switches to toggle to events
    for (auto el: switches_to_toggle) {
        events[el] = true;
    }
    


    return events;
}

bool ISC::cluster::is_crossing_event(
    sct_core::ct_state x, 
    sct_core::ct_state u, 
    double t)
{
    active_threshold_detectors.clear();
    bool result = false;

    // Get output values 
    sct_core::ct_state y =  _topology_selector.get_outputs(x, u);

    // Get the id of the metering elements related to the threshold detectors
     for (const auto detector_ptr: threshold_detectors) {
        // Get id of the metering element connected to the tracer
        ISC::id_type meter_id = reader_writer_map[detector_ptr->get_id()];
        unsigned pos =  output_positions_map.at(meter_id);
        // Check if there is a crossing    
        if (detector_ptr->is_crossing(t, y[pos])) {
            active_threshold_detectors.insert(detector_ptr);
            result = true;
        }
    }
    return result;
}

void ISC::cluster::generate_outputs(bool state_event_located, 
    std::unordered_map<int, bool> events
) {
    #ifdef PROVIDE_STATISTICS
        clock_t _t = clock();
    #endif

        

        std::vector<ISC::id_type> branch_ids;

        for (auto it = events.begin(); it != events.end(); it++) {

            // Threshold crossing event 
            if (it->first == ISC::internal::THRESHOLD_CROSSING_EVENT) {
                generate_threshold_outputs();
            }
            // Internal event from diodes
            else {
                branch_ids.push_back(it->first);
            }
        }

        // Profit from the fact that if switches are toggled 
        // here, at the next reactivation the new equations will be 
        // available
        double last_internal_switching_time = sc_core::sc_time_stamp().to_seconds();

        _topology_selector.toggle_switches(branch_ids, last_internal_switching_time);
    

    #ifdef PROVIDE_STATISTICS
        _t = clock() - _t;
        generate_outputs_sim_time += ((double) _t) / CLOCKS_PER_SEC;
    #endif

}

void ISC::cluster::generate_threshold_outputs() {
    for (const auto detector_ptr: threshold_detectors) {
        detector_ptr->generate_outputs();
    }
}

// Get value of all volage and current sources 
sct_core::ct_state ISC::cluster::get_u(bool use_input_checkpoints, double t) {

    inputs.use_checkpoints(use_input_checkpoints);

    // Declare u vector
    sct_core::ct_state u;
    
    // with as many elements as there are sources
    u.resize(ind_sources.size());
    
    // Fill in u with the source values
    int i = 0;
    for (auto source_ptr : ind_sources) {
        u[i++] = source_ptr->get_value(t);
    }

    return u;
}
    

bool ISC::cluster::execute_updates() {
    #ifdef PROVIDE_STATISTICS
        clock_t _t = clock();
    #endif

    bool result = process_de_inputs(sc_core::sc_time_stamp().to_seconds());
    
    #ifdef PROVIDE_STATISTICS
        _t = clock() - _t;
        execute_updates_sim_time += ((double) _t) / CLOCKS_PER_SEC;
    #endif
    
    return result;
}

void ISC::cluster::end_of_simulation() {
    sct_core::ct_module::end_of_simulation();
    #ifdef PROVIDE_STATISTICS
        std::cout << "TIME SPENT IN CLUSTER::END_OF_ELABORATION " << elaboration_time << std::endl;
        std::cout << "TIME SPENT IN CLUSTER::EXECUTE_UPDATES " << execute_updates_sim_time << std::endl;
        std::cout << "TIME SPENT IN CLUSTER::IS_EVENT " << is_event_sim_time << std::endl;
        std::cout << "TIME SPENT IN CLUSTER::GET_DERIVATIVES " << get_derivatives_sim_time << std::endl;
        std::cout << "TIME SPENT IN CLUSTER::GENERATE_OUTPUTS " << generate_outputs_sim_time << std::endl;
    #endif
}

sct_core::ct_state ISC::cluster::map_state_to_trace(double t, const sct_core::ct_state &x) {
    sct_core::ct_state y =  _topology_selector.get_outputs(x, get_u(false, t));
    return sct_core::ct_state(tracer_mask * y.get_core());
}

void ISC::cluster::create_output_positions_map() {
    std::vector<ISC::id_type> output_branches =  _topology_selector.get_ordered_output_branches();
    // Create map from output element id to position in the output vector
    unsigned pos = 0;
    for (const auto& branch_id: output_branches) {
        ISC::id_type element_id = branch_element_ids_map[branch_id];
        output_positions_map[element_id] = pos++;
    }

}

void ISC::cluster::create_tracer_mask() {

    // Fill matrix
    tracer_mask = MatrixXd::Zero(tracers.size(), output_positions_map.size());
    unsigned tracer_row = 0;
    for (const auto tracer_ptr: tracers) {
        // Get id of the metering element connected to the tracer
        ISC::id_type meter_id = reader_writer_map[tracer_ptr->get_id()];
        unsigned col =  output_positions_map.at(meter_id);
        tracer_mask(tracer_row++, col) = 1;
    }

}

void ISC::cluster::create_signal_reader_writer_map() {
    for (const auto &signal_ptr : signals) {
        for (const auto &reader_id : signal_ptr->get_reader_ids()) {
            reader_writer_map[reader_id] = signal_ptr->get_writer_id();
        }
    }
}

void ISC::cluster::process_dependent_sources() {
    for (const auto &source_ptr: dep_sources) {
        ISC::id_type s_id = source_ptr->get_id();
        ISC::id_type meter_id = reader_writer_map[s_id];
        
        // Get branches ids
        ISC::id_type s_branch_id = element_branch_ids_map[s_id]; 
        ISC::id_type meter_branch_id = element_branch_ids_map[meter_id];
    
        // Get branches
        ISC::internal::branch &source_branch = _branches.get_branch(s_branch_id);
        ISC::internal::branch &meter_branch = _branches.get_branch(meter_branch_id);


        // Add law term
        branch_generator.add_external_law_term(source_branch, meter_branch, *source_ptr);
    }
}
