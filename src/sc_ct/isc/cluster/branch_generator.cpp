#include "sc_ct/isc/cluster/branch_generator.h"

using namespace ISC::internal;

branch_generator::branch_generator() :
    port_elements{ind_v_source_el, ind_c_source_el, switch_el, diode_el, capacitor_el, inductor_el, voltmeter_el, ammeter_el}
{

}


branch branch_generator::generate_branch(const ISC::circuit_element &element) const 
{
    branch _branch;

    _branch.set_port(is_port(element));
    _branch.set_on_state(is_on(element));
    add_own_law_term(_branch, element);
    _branch.set_element(element.get_element());
    _branch.set_parameter_value(element.get_value());

    if ( _branch.get_element() == ISC::cc_source_el) {
        _branch.set_law_expressed_as_voltage(false);
    }

    return _branch;
}


bool branch_generator::is_port(const ISC::circuit_element &element) const 
{
    // Traverse vector of port elements
    // If the given circuit element is one of these, 
    // return true. Otherwise, return false
    for (auto el: port_elements) {
        if (element.get_element() == el){
            return true;
        }
    }

    return false;
}


bool branch_generator::is_on(const ISC::circuit_element &element) const 
{
      const ISC::ideal_switch *ptr = dynamic_cast<const ISC::ideal_switch *>(&element);

      if(ptr) {
          return ptr->is_on();
      }

      return true;
}

void branch_generator::add_own_law_term(ISC::internal::branch &branch, const ISC::circuit_element &element) const 
{
    switch (element.get_element()) {
        case ISC::resistance_el:
            // For resistances: v = R*i
            branch.add_law_term(
                // Depends on current branch variable 
                branch.get_id(),    
                // The independependent variable is current (v = R*i)
                true,               
                // R
                element.get_value() // Value of the resistance
            );
            break;
        case ISC::cc_source_el:
            branch.set_law_expressed_as_voltage(false);
            break;
        // Capacitor and inductors have differential laws
        // that are treated during the generation of equations 
        default:
            break;
    }

}

void branch_generator::add_external_law_term(
    ISC::internal::branch &dependent_branch,
    const ISC::internal::branch &independent_branch,
    const ISC::internal::dependent_source &element) const 
{

    switch (element.get_element()) {
        case ISC::cv_source_el:
            // For resistances: v = R*i
            dependent_branch.add_law_term(
                // Depends on current branch variable 
                independent_branch.get_id(),    
                // The independent variable is current (vs voltage)?
                independent_branch.get_element() == ISC::ammeter_el  ,               
                // Factor
                element.get_factor() // Value of the resistance
            );
            break;
        case ISC::cc_source_el:
            // For resistances: v = R*i
            dependent_branch.add_law_term(
                // Depends on current branch variable 
                independent_branch.get_id(),    
                // The independent variable is current (vs voltage)?
                independent_branch.get_element() == ISC::ammeter_el  ,               
                // Factor
                element.get_factor() // Value of the resistance
            );
            break;
        // Capacitor and inductors have differential laws
        // that are treated during the generation of equations 
        default:
            break;
    }
}
