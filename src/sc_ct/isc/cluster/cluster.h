#ifndef ISC_H
#define ISC_H

#include <systemc>
#include <iostream>
#include <map>

// Include ISC components, ports and channels
#include "sc_ct/isc/isc.h"

#include "sc_ct/isc/equation_generator/branch.h"
#include "sc_ct/isc/equation_generator/label.h"
#include "sc_ct/isc/equation_generator/branches.h"
#include "sc_ct/isc/equation_generator/topology_selector.h"
#include "sc_ct/isc/cluster/branch_generator.h"

using namespace ISC::internal;


#include <Eigen/Dense>
using namespace Eigen;

#include "sc_ct/core/ct_module.h"

// Length of integration interval
#define DELTA_T 2 // seconds

// #define PROVIDE_STATISTICS


class ISC::cluster : virtual public sct_core::ct_module {
    public:
        cluster(sc_core::sc_module_name name = sc_core::sc_gen_unique_name(cluster_kind.c_str()),
            bool use_adaptive = true, bool avoid_rollback = true, 
            double sync_step = DELTA_T);

        void end_of_elaboration();
        void end_of_simulation();

        //////////////////////////////////////////////////////////////////
        // METHODS RELATED TO CT_MODULE
        //////////////////////////////////////////////////////////////////
        void set_sync_parameters();

        virtual void set_initial_conditions() = 0;
        
        void get_derivatives(bool use_input_checkpoints,
            const sct_core::ct_state &x , sct_core::ct_state &dxdt , double t );
        
        std::unordered_map<int, bool> is_event(const sct_core::ct_state &x, double t = 0);
        
        void generate_outputs(bool state_event_located, 
            std::unordered_map<int, bool> events
        );

        bool execute_updates();

        virtual sct_core::ct_state map_state_to_trace(double t, const sct_core::ct_state &x);

    private: 
        // Set of circuit elements 
        std::set<ISC::circuit_element *> circuit_elements;

        // Set of independent voltage and current sources
        std::set<ISC::internal::independent_source *> ind_sources;

        // Set of dependent voltage and current sources
        std::set<ISC::internal::dependent_source *> dep_sources;

        // Set of tracers
        std::set<ISC::tracer *> tracers;

        // Set of signals
        std::set<ISC::signal *> signals;

        // Set of modules with inputs 
        std::set<ISC::internal::de_in_module_if *> modules_with_de_inputs;

        // Set of threshold detector
        std::set<ISC::threshold_detector *> threshold_detectors;

        // Set of lastly activated threshold detectors
        std::set<ISC::threshold_detector *> active_threshold_detectors;

        // Set of nodes
        std::set<ISC::node *> nodes;

        // Map form nodes to rows in the incidence matrix
        std::map<ISC::id_type, unsigned> node_row_map;

        // Map from branches to columns in the incidence matrix
        std::map<ISC::id_type, unsigned> branch_col_map;

        // Map from the set of element ids to the set of branch ids
        std::map<ISC::id_type, ISC::id_type> element_branch_ids_map;

        // Map from the set of branch ids to the set of element ids
        std::map<ISC::id_type, ISC::id_type> branch_element_ids_map;

        // Map from the set of signal readers ids to the set of 
        // signal writers ids
        std::map<ISC::id_type, ISC::id_type> reader_writer_map;

        // Map from the set output elements to the positions
        // in the Y vector of the state space representation
        std::map<ISC::id_type, unsigned> output_positions_map;

        // Set of branch labels
        std::vector<label> labels;

        // Object that generates branches from a circuit element
        ISC::internal::branch_generator branch_generator;

        // Set of branches 
        branches _branches;

        // Synchronization parameters
        bool use_adaptive, avoid_rollback; 
        double sync_step;
        sc_core::sc_time next_sampling_time;
        
        // Object that handles the circuit topology and equations<
        ISC::internal::topology_selector _topology_selector;

        // Incidence matrix
        MatrixXd matrix;

        // Matrix to transform a set of outputs 
        // to a set of tracer variables
        MatrixXd tracer_mask;

        // Function to store pointers to circuit elements 
        // and nodes so that these elements can be later 
        // manipulated 
        void store_elements_and_nodes();

        // Add a given sc_object to the set of circuit elements
        void add_to_set_of_circuit_elements(sc_core::sc_object *);

        // Add a given sc_object to the set of independent sources (voltage and current sources)
        void add_to_set_of_ind_sources(sc_core::sc_object *);

        // Add a given sc_object to the set of dependent sources (voltage and current sources)
        void add_to_set_of_dep_sources(sc_core::sc_object *);

        // Add a given sc_object to the set of modules with DE inputs such as externally controlled switches
        void  add_to_set_of_module_with_de_inputs(sc_core::sc_object *);

        // Add a given sc_object to the set of nodes 
        void add_to_set_of_nodes(sc_core::sc_object *);

        // Add a given sc_object to the set of threshold detectors 
        void add_to_set_of_threshold_detectors(sc_core::sc_object *);

        // Add a given sc_object to the set of signals
        void add_to_set_of_signals(sc_core::sc_object *);

        // Add a given sc_object to the set of tracers
        void add_to_set_of_tracers(sc_core::sc_object *);

        // Create a mapping from the set of signal reader ids 
        // to the set of signal writer ids
        void create_signal_reader_writer_map();

        // Create a mapping from the set of nodes to the set of incidence matrix rows
        void create_node_row_map();

        // Create the set of circuit branches and the incidence matrix
        // from the set of circuit elements and their connection to nodes
        void create_branches_and_incidence_matrix();

        // Process dependent sources: add law terms that depend 
        // on other element branches
        void process_dependent_sources();

        // Initialize the incidence matrix with the approproate dimensions
        void initialize_matrix();

        // Fill the given circuit_element corresponding entries in the incidence matrix according to its interconnections by follwing the rules given in Chua & Lin (1975)
        void fill_matrix(const ISC::circuit_element &);

        // Get the row in the incidence matrix that corresponds to the node t which the given circuit_element's terminal with the passed polarity is connected
        unsigned get_node_row(const ISC::circuit_element &, ISC::polarity);

        // Get the column in the incidence matrix that corresponds to the given circuit element
        unsigned get_column(const ISC::circuit_element &);

        // Create a branch given a circuit_element
        void create_branch(const ISC::circuit_element &);

        // Create a mask to distinguish the outputs 
        // that correspond to tracer elements 
        // from other kind of outputs
        void create_tracer_mask();

        // Creates the mapping structure from metering element 
        // to the position in the Y vector of the state space representation
        void create_output_positions_map();

        
        // Other utility functions
        // Remove the given row from the passed matrix element
        void remove_row(Eigen::MatrixXd& matrix, unsigned int row);

        // Invoke the functions that update the state of the externally controlled switches if there are changes in their DE inputs
        bool process_de_inputs(double t, bool use_input_checkpoints = false);

        // Get value of all volage and current sources
        sct_core::ct_state get_u(bool use_input_checkpoints, double t);

        // Returns true if there is a crossing event and false otherwise
        bool is_crossing_event(sct_core::ct_state x, sct_core::ct_state u, double t);

        // Write DE output ports connected to the threshold detectors 
        void generate_threshold_outputs();

        #ifdef PROVIDE_STATISTICS
            // SIM STATISTICS
            double is_event_sim_time;
            double get_derivatives_sim_time;
            double execute_updates_sim_time;
            double generate_outputs_sim_time;
            double elaboration_time;
        #endif        
};

#endif
