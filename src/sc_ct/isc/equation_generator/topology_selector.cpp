#include "sc_ct/isc/equation_generator/topology_selector.h"



ISC::internal::topology_selector::topology_selector()
{

}

void ISC::internal::topology_selector::init(const MatrixXd &incidence_mat, const branches &_branches, const labels &_labels)
{
    this->_branches = _branches;

    // Chained objects that process
    // incidence matrix and branches to
    // get a circuit model
    // 1. Create incidence matrix that is able to 
    // synthesize the cutset matrix
    A = new incidence_matrix(incidence_mat, _labels);
    MatrixXd cutset_m = A->get_cutset_matrix(this->_branches); 
    // 2. Create the curset matrix from which we will 
    // obtain the reduced tableau matrix


    D = new cutset_matrix(cutset_m, A->get_labels(), this->_branches);
    // 3. Get the reduced tableau matrix

    tableau_matrix F;
    MatrixXd reduced_tableau = F.get_reduced_tableau(this->_branches, *D);
    labels reduced_tableau_labels = this->_branches.get_reduced_tableau_labels();


    // 4. Create the state space representation from the 
    // reduced tableau
    SS = new ss_matrix(reduced_tableau, reduced_tableau_labels, this->_branches);

    // 5. Create and configure the 
    // final switched circuit model 
    circuit_model = new SwitchedModel(
        SS->get_matrix(),
        SS->get_labels(),
        this->_branches,
        SS->get_Q_start_row(),
        SS->get_Q_start_col(),
        SS->get_S_start_row(),
        SS->get_S_start_col()
    );   

    circuit_model->extract_ss_matrices();
}


ISC::internal::topology_selector::~topology_selector() {
    delete A;
    delete D;
    delete SS;
    delete circuit_model;
}


sct_core::ct_state ISC::internal::topology_selector::get_derivatives(const sct_core::ct_state &x, const sct_core::ct_state &u) {

    return circuit_model->get_derivatives(x, u);
}

sct_core::ct_state ISC::internal::topology_selector::get_outputs(const sct_core::ct_state &x, const sct_core::ct_state &u) {


    return circuit_model->get_outputs(x, u);
}



bool ISC::internal::topology_selector::set_switch_states(std::vector<ISC::id_type> branch_ids, double t) {

    if (branch_ids.empty()) {
        return false;
    }

    circuit_model->toggle_switches(branch_ids);

    return true;
}

void ISC::internal::topology_selector::toggle_switches(std::vector<ISC::id_type> branch_ids, double t) {

    // if (branch_ids.size() == 0){
    //     return;
    // }

    // Get new topology id
    

    circuit_model->toggle_switches(branch_ids);
    
    check_repeated_topology(t);

    // ADD CONDITIONS ON SIZE OF STATE

}

void ISC::internal::topology_selector::check_repeated_topology(double t) {
 
    // Reset the set of traverse topologies 
    // during the current time everytime 
    // real time advances
    if (t != last_switching_time) {
        traversed_topologies.clear();
        last_switching_time = t;
    }

    // Get new topology id
    std::bitset<MAX_SWITCHES> new_topology = circuit_model->get_topology_id(); 
    

    // If can't insert because it is in the set, 
    //  throw an error
    auto result = traversed_topologies.insert(new_topology);
    bool inserted = result.second;
    if (!inserted) {
        throw std::domain_error("Detected infinite loop during switching. The same topology has been found twice without real time advancing.");
    }

}

// 
std::vector<ISC::id_type> ISC::internal::topology_selector::get_internal_switches_to_toggle( 
    const sct_core::ct_state &x, const sct_core::ct_state &u, double t) {
    sct_core::ct_state dxdt = get_derivatives(x, u);
    return circuit_model->get_internal_switches_to_toggle(_branches, dxdt, x, u);
}

std::vector<ISC::id_type> ISC::internal::topology_selector::get_ordered_output_branches() const {
    return circuit_model->get_ordered_output_branches();
}
