#include "sc_ct/isc/equation_generator/topological_matrix.h"


#define DEBUG

topological_matrix::topological_matrix(const MatrixXd &m,
    const labels &_labels) : 
    m(m), _labels(_labels)
{
    set_labels(_labels);
}


bool topological_matrix::is_valid_col(unsigned col, MatrixXd mat) {
    if (col >= mat.cols()) {
        return false;
    }
    return true;
}

bool topological_matrix::is_valid_row(unsigned row, MatrixXd mat) {
    if (row >= mat.rows()) {
        return false;
    }
    return true;
}

const labels &topological_matrix::get_labels() const {
    return _labels;
}

void topological_matrix::set_labels(labels _labels) {
    // Check that there are exactly as many _labels as columns 
    // in the matrix
    if (_labels.size() != (unsigned) m.cols()) {
        // Throw an exception if they are different
        throw std::invalid_argument("topological_matrix::topological_matrix : there must be as many _labels as the number of columns");
       
        // Copy the given labels, 
        // They are either more or less than 
        // the number of columns
        for (unsigned i = 0; i <  m.cols(); i++) {
            
            // Try to copy
            try {
                this->_labels.set_label(i, _labels.get_label(i));
            }
            // Not enough given labels,
            // (they are less than cols), break loop
            catch(...) {
                break;
            }
        } 
    }
    // Exactly the right number of labels given
    this->_labels = _labels;
 }

// Swaps two columns of a topological matrix and their corresponding 
// labels
void topological_matrix::swap_cols(unsigned col_a, unsigned col_b){
    return _swap_cols(col_a, col_b, m, true);
}

// Swaps two columns of a topological matrix and their corresponding 
// labels
void topological_matrix::_swap_cols(
    unsigned col_a, unsigned col_b, 
    MatrixXd &mat, bool affect_labels)
{
    if (topological_matrix::is_valid_col(col_a, mat) && 
        topological_matrix::is_valid_col(col_b, mat)) {
        mat.col(col_a).swap(mat.col(col_b));
        if (affect_labels) {
            _labels.swap_labels(col_a, col_b);
        }
    }
    else {
        throw std::invalid_argument("topological_matrix::swap_cols : invalid columns");
    }
}


std::vector<unsigned> topological_matrix::to_row_echelon_form() {
    return topological_matrix::gauss_jordan_elimination(m);
}

void topological_matrix::swap_rows(unsigned row_a, unsigned row_b,
    MatrixXd &mat) 
{ 
    if (topological_matrix::is_valid_row(row_a, mat) && 
        topological_matrix::is_valid_row(row_b, mat)) {
        mat.row(row_a).swap(mat.row(row_b));
    }
    else {
        throw std::invalid_argument("topological_matrix::swap_cols : invalid columns");
    }
} 



void  topological_matrix::print() const {
    
    std::cout << "| ";
    for (auto label: _labels.get_labels()) {
        std::string var_type_str = (label.get_col_type() == current_t ? "i_" : "v_");
        std::string s = var_type_str + std::to_string(label.get_branch_id()) +  " | ";

        std::cout << std::setw(10) << s;
    }
    std::cout << std::endl;
    
    for (int i = 0; i < m.rows(); i++) {
        for(int j = 0; j < m.cols(); j++) {
            std::cout << std::setw(10) << m(i,j);
        }
        std::cout << std::endl;
    }
}

void  topological_matrix::remove_col(unsigned col) {
    if (topological_matrix::is_valid_col(col, m)) {
        unsigned rows = m.rows();
        unsigned cols = m.cols()-1;

        // Shift the whole block at the right of the column to remove
        // one column to the left
        m.block(0,col,rows,cols-col) = m.block(0,col+1,rows,cols-col);
        m.conservativeResize(rows,cols);
        _labels.erase(col);
    }
    else {
        throw std::invalid_argument("topological_matrix::remove_cols : invalid columns");
    }
}   

const MatrixXd &topological_matrix::get_matrix() const {
    return m;
}


std::vector<unsigned> topological_matrix::gauss_jordan_elimination(
    MatrixXd &mat, 
    bool back_substitute, int start_row, int final_row, int start_col, int final_col) 
{
    std::vector<unsigned> pivots;
    int cols = final_col > -1 && final_col < mat.cols() ? final_col + 1 : mat.cols();
    int rows = final_row > -1 && final_row < mat.cols() ? final_row + 1 : mat.rows();

    int h = start_row > -1 ? start_row : 0; // Pivot row
    int k = start_col > -1 ? start_col : 0; // Pivot column 

    int i_max; 
    double v_max;

    while (h < rows && k < cols) {
        i_max = h; 
        v_max = mat(i_max,k); 

        /* find greater amplitude for pivot if any */
        for (int i = h+1; i < rows; i++){
            if (fabs(mat(i,k)) > fabs(v_max)) {
                v_max = mat(i,k);
                i_max = i;
            }
        } 
    
        // No pivot in this column, pass to next column */
        if (v_max == 0) {  
            k = k+1;
        }
        else {


            // Insert column index to set of pivots
            pivots.push_back(k);

            // Divide whole row by pivot element
            mat.row(i_max) = mat.row(i_max) / v_max;
            
            // Swap
            topological_matrix::swap_rows(h, i_max, mat);

            // For all rows below pivot
            for(int i = h+1; i < rows; i++) {
                double f = mat(i,k);

                // Fill with zeros the lower part of the pivot column
                mat(i,k) = 0;

                if(f != 0){
                    for (int j=k+1; j < cols; j++) {
                        mat(i,j) = mat(i,j) - (mat(h,j) * f);
                        if ( fabs(mat(i,j)) < GAUSS_JORDAN_ZERO_TOL ) {
                            mat(i,j) = 0;
                        }
                    }
                }
            }

            if (back_substitute) {
                // For all rows above pivot (back substitution)
                for(int i = h-1; i >= 0; i--) {
                    // mat(h,k) is v_max != 0
                    double f = mat(i,k);
                    // Fill with zeros the part of the pivot column
                    mat(i,k) = 0;

                    if(f != 0){
                        for (int j=k+1; j < cols; j++) {
                            mat(i,j) = mat(i,j) - (mat(h,j) * f);  
                            if (fabs(mat(i,j)) < GAUSS_JORDAN_ZERO_TOL) {
                                mat(i,j) = 0;
                            }  
                        }
                    }
                    
                }
            }

            // Increase pivot row and column
            h = h + 1;
            k = k + 1;
        }
    }

    return pivots;
}
