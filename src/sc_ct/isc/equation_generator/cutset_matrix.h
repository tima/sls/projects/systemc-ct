#ifndef CUTSET_MATRIX_H
#define CUTSET_MATRIX_H

#include <vector>
#include <Eigen/Dense>
#include "sc_ct/isc/equation_generator/topological_matrix.h"
#include "sc_ct/isc/equation_generator/branches.h"

using namespace Eigen;

using namespace ISC::internal;

class ISC::internal::cutset_matrix : public topological_matrix {
    public: 
        cutset_matrix(const MatrixXd &,const labels &, const branches &); 
        
        MatrixXd get_Day() const;
        MatrixXd get_Dab() const;
        MatrixXd get_Dzy() const;
        MatrixXd get_Dzb() const;
        
    private:
        int z_s;
        int y_s;
        int b_s;
        int a_s;

};

#endif
