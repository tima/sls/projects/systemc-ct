#include "sc_ct/isc/equation_generator/cutset_matrix.h"

cutset_matrix::cutset_matrix(const MatrixXd &m,
    const labels &labels, const branches &branches) :
    topological_matrix(m, labels)
{
    z_s = branches.get_count_by_type(z_nonport_in_tree);
    y_s = branches.get_count_by_type(y_nonport_in_cotree);
    b_s = branches.get_count_by_type(b_port_in_cotree);
    a_s = branches.get_count_by_type(a_port_in_tree);
}


MatrixXd cutset_matrix::get_Day() const {
    return m.block(0, a_s+z_s, a_s, y_s);
}

MatrixXd cutset_matrix::get_Dab() const {
    return m.block(0, a_s+z_s+y_s, a_s, b_s);
}

MatrixXd cutset_matrix::get_Dzy() const {
    return m.block(a_s, a_s+z_s, z_s, y_s);
}

MatrixXd cutset_matrix::get_Dzb() const {
    return m.block(a_s, a_s+z_s+y_s, z_s, b_s);
}
