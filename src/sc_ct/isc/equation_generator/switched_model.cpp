#include "sc_ct/isc/equation_generator/switched_model.h"

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/vector.hpp>


SwitchedModel::SwitchedModel(const MatrixXd &m, const labels &_labels,
    const branches &branches,
    unsigned outputs_start_row, unsigned outputs_start_col, unsigned switches_start_row, unsigned switches_start_col ) 
    : topological_matrix(m, _labels),
    outputs_start_row(outputs_start_row),
    outputs_start_col(outputs_start_col),
    switches_start_row(switches_start_row),
    switches_start_col(switches_start_col)
{
    // Set up some internal auxiliary variables
    number_of_switches = this->_labels.get_count_by_ss_type(switch_var);
    number_of_outputs = this->_labels.get_count_by_ss_type(output_var);  
    derivatives_start_col = outputs_start_col + number_of_outputs;
    number_of_state_variables = this->_labels.get_count_by_ss_type(state_var);
    number_of_inputs =  this->_labels.get_count_by_ss_type(source_var);
    ss_start_col = derivatives_start_col + number_of_state_variables;
    impulsive_var_col_shift = outputs_start_col + number_of_state_variables;


    if(number_of_switches > MAX_SWITCHES) {
        throw std::domain_error("SwitchedModel::SwitchedModel: the circuit has too many switch elements.  Impossible to simulate.");
    }

    input_cols = _labels.get_columns_by_type(source_var);
    switch_cols = _labels.get_columns_by_type(switch_var);    
    state_var_cols = _labels.get_columns_by_type(state_var);
    derivative_var_cols = _labels.get_columns_by_type(derivative_var);

    // impulsive =  VectorXd(number_of_switches);

    calc_diode_cols(branches);
}

void SwitchedModel::extract_ss_matrices() {

    std::bitset<MAX_SWITCHES> topo_id = get_topology_id();

    unsigned rows_shift, cols_shift;
    unsigned rows, cols;

    // Position of the C1 matrix
    rows_shift = outputs_start_row;
    cols_shift = outputs_start_col + number_of_outputs;
    rows = number_of_outputs;
    cols = number_of_state_variables;

    #ifdef STORE_MATRICES_BY_TOPOLOGY
        try { 
           m = m_map.at(topo_id);
        }
        catch(const std::exception& e){
        topological_matrix::gauss_jordan_elimination(m, true);
            m_map[topo_id] = m;
        }
    #endif

    #ifndef STORE_MATRICES_BY_TOPOLOGY
        topological_matrix::gauss_jordan_elimination(m, true);
    #endif


    // Extract the C1_ss and M_ss matrices
    C1_ss = -m.block(rows_shift, cols_shift, rows, cols);
    M_ss = m.block(rows_shift + rows, cols_shift, number_of_state_variables, cols);

    // Avoid re-calculation of pivot elements for already calculated topologies
    // Find independent state derivatives
    try { 
        M_ss_pivots = M_ss_pivots_map.at(topo_id);
        M_ss_zero_rows = M_ss_zero_rows_map.at(topo_id); 
    }
    catch(const std::exception& e){
        M_ss_pivots = topological_matrix::gauss_jordan_elimination(M_ss, false);
        M_ss_pivots_map[topo_id] = M_ss_pivots;

        M_ss_zero_rows = get_zero_row_positions(M_ss);
        M_ss_zero_rows_map[topo_id] = M_ss_zero_rows;
    }


    // Increment cols to extract C and A matrices 
    cols_shift += cols;
    cols =  number_of_state_variables;
    C_ss = -m.block(rows_shift, cols_shift, rows, cols);
    A_ss = -m.block(rows_shift + rows, cols_shift, number_of_state_variables, cols);

    // std::cout << "\n\n***** A IS\n";
    // std::cout << A_ss << std::endl << std::endl;

    // // Increment cols to extract D and B matrices, 
    // // skip Zero port variables
    cols_shift += cols;
     // Each switch has a zero port
    cols_shift += number_of_switches;
    // Each output (voltmeter, ammeter)
    // has a zero port
    cols_shift += number_of_outputs;  
    cols = number_of_inputs;

    D_ss = -m.block(rows_shift, cols_shift, rows, cols);
    B_ss = -m.block(rows_shift + rows, cols_shift, number_of_state_variables, cols);


    // Set switch related matrices
    unsigned u_start_col = m.cols()-number_of_inputs;
    Sdxdt = -m.block(switches_start_row, derivatives_start_col, number_of_switches,number_of_state_variables);
    Sx = -m.block(switches_start_row, ss_start_col, number_of_switches,number_of_state_variables);
    Su = -m.block(switches_start_row, u_start_col, number_of_switches, number_of_inputs);

    calc_dependent_state_vars();
    precalculate_dependent_state_matrices();    

    // std::cout << "** ** MATRICES ** **\n";
    // std::cout << "A\n" << A_ss << std::endl;
    // std::cout << "B\n" << B_ss << std::endl;
    // std::cout << "C\n" << C_ss << std::endl;
    // std::cout << "D\n" << D_ss << std::endl;
    // std::cout << "extracted from\n" << m << std::endl; 
}


void SwitchedModel::exchange_columns(unsigned branch_id) {
    unsigned col_1 = 0;
    unsigned col_2 = 0;
    label label_1, label_2, aux;

    bool found = false;
    // Find first col
    for(int i = 0; i < m.cols(); i++) {
        label_1 = _labels.get_label(i);
        if (label_1.get_branch_id() == branch_id) {
            col_1 = i;
            found = true;
            break;
        }
    }

    // std::cout << "Branch id " << branch_id << std::endl;

    if (!found) {
        throw std::domain_error("SwitchedModel::exchange_columns: branch does not exist ");
    }

    // Find second col
    found = false;
    for(int i = col_1 + 1; i < m.cols(); i++) {
        label_2 = _labels.get_label(i);
        if (label_2.get_branch_id() == branch_id) {
            col_2 = i;
            found = true;
            break;
        }
    }

    if (!found) {
        throw std::domain_error("SwitchedModel::exchange_columns: inconsistent labels");
    }

    swap_cols(col_1, col_2);

    // Exchange ss types switch_var <-> zero_port_var
    label_1 = _labels.get_label(col_1);
    label_2 = _labels.get_label(col_2);
    aux = label_1;

    label_1.set_ss_type(label_2.get_ss_type());
    label_2.set_ss_type(aux.get_ss_type());

    _labels.set_label(col_1, label_1);
    _labels.set_label(col_2, label_2);

}

sct_core::ct_state SwitchedModel::get_derivatives(const sct_core::ct_state &x, const sct_core::ct_state &u) {

    sct_core::ct_state dxdt(number_of_state_variables);

    // Calculate values
    dxdt.get_core() = (A_ss * x.get_core()) + (B_ss * u.get_core());

    return dxdt;
}

sct_core::ct_state SwitchedModel::get_outputs(const sct_core::ct_state &x, const sct_core::ct_state &u) {
    sct_core::ct_state y(number_of_outputs);

    // Calculate values
    y.get_core() = (C_ss * x.get_core()) + (D_ss * u.get_core());

    return y;
}

// Returns columns whose label represent the set of 
// dependent and independent state variables
void SwitchedModel::calc_dependent_state_vars() {

    independent_state_vars.clear();
    dependent_state_vars.clear();
    
    unsigned cols = M_ss.cols();
    unsigned j = 0;
    bool is_independent = false;
    for (unsigned i = 0; i < cols; i++){

        is_independent = j < M_ss_pivots.size() && M_ss_pivots[j] == i;

        if (is_independent) {
            independent_state_vars.push_back(state_var_cols[0] + M_ss_pivots[j++]);
        }
        else {
            dependent_state_vars.push_back(state_var_cols[0] + i);
        }
    }
    
}

std::vector<unsigned> SwitchedModel::get_zero_row_positions(const MatrixXd &m) {
    
    std::vector<unsigned> zero_rows;
    unsigned rows = m.rows();
    unsigned cols = m.cols();
    bool zero;
    
    for (unsigned i = 0; i < rows; i++) {
        zero = true;
        for (unsigned j = 0; j < cols; j++) {
            if (m(i,j) != 0) {
                zero = false;
                break;
            }
        }
        if (zero) {
            zero_rows.push_back(i);
        }
    }

    return zero_rows;
}

VectorXd SwitchedModel::calculate_dependent_state_vars(const VectorXd &x, const VectorXd  &u) {


    unsigned independent_count = independent_state_vars.size();
    unsigned dependent_count = dependent_state_vars.size();

    if (M_ss_zero_rows.size() == 0) {
        return x;
    }

    VectorXd xi(independent_count);
    VectorXd xd(dependent_count);
    VectorXd _x(number_of_state_variables);

    unsigned first_state_var =  state_var_cols[0];

    // Copy independent states
    unsigned pos;



    for (unsigned i = 0; i < independent_count; i++) {
        pos = independent_state_vars[i]-first_state_var;
        xi(i) = _x(pos) = x(pos);
    }


    // Calc. new values
    xd = -Add_inv*(Adi*xi + Bd*u);

    // Copy new values to auxiliary state vector
    for (unsigned i = 0; i < dependent_count; i++) {
        pos = dependent_state_vars[i]-first_state_var;
        _x(pos) = xd(i);
    }

    return _x;
}

void SwitchedModel::precalculate_dependent_state_matrices(){

    unsigned zero_count = M_ss_zero_rows.size();
    unsigned independent_count = independent_state_vars.size();
    unsigned dependent_count = dependent_state_vars.size();
    

    // Offset of the M_ss matrix in the whole tableau
    unsigned row_offset = outputs_start_row + number_of_outputs;
    unsigned row, col;

    Adi = MatrixXd(zero_count, independent_count);
    Add_inv = MatrixXd(zero_count, dependent_count);
    Bd = MatrixXd(zero_count, B_ss.cols());

    // Get Adi Add and Bd
    
    // To optimize: Zero rows are always at the end of M_ss
    for (unsigned i = 0; i < zero_count; i++) {
        row = M_ss_zero_rows[i] + row_offset;

        for (unsigned j = 0; j < dependent_count; j++) {
            col = dependent_state_vars[j];
            Add_inv(i,j) = -m(row,col);
        }

        for (unsigned j = 0; j < independent_count; j++) {
            col = independent_state_vars[j];
            Adi(i,j) = -m(row,col);
        }

        for (unsigned j = 0; j < input_cols.size(); j++) {
            col = input_cols[j];
            Bd(i,j) = -m(row,col);
        }
    }

    // To avoid calling .inverse() multiple times
    Add_inv = Add_inv.inverse();
}



std::vector<ISC::id_type> SwitchedModel::get_internal_switches_to_toggle(const branches &branches,  const sct_core::ct_state &dxdt, const sct_core::ct_state &x, const sct_core::ct_state &u) {

    VectorXd _x_new = calculate_dependent_state_vars(x.get_core(), u.get_core());
  
    calculate_switch_control_variables(dxdt.get_core(), x.get_core(), _x_new, u.get_core());

    std::vector<ISC::id_type> result;

    unsigned i;
    label _label;
    for (auto col : diode_cols) {
        i = col - switch_cols[0];
        _label = _labels.get_label(col);

        if (_label.is_current()) {
            //negative_current =
            // negative current 
            if ( impulsive(i) < 0 || 
            (impulsive(i) == 0 && nonimpulsive(i) < 0)) {
                result.push_back(_label.get_branch_id());
            }
        }
        else if (_label.is_voltage()) {
            // Positive voltage
             if ( impulsive(i) > 0 || 
            (impulsive(i) == 0 && nonimpulsive(i) > 0)) {
                result.push_back(_label.get_branch_id());
            }
        }

    }

    return result;
}

void SwitchedModel::calc_diode_cols(const branches &branches) {

    label _label;
    ISC::internal::branch branch;

    for (unsigned i = 0; i < switch_cols.size(); i++) {
        _label = _labels.get_label(switch_cols[i]);
        branch = branches.get_branch(_label.get_branch_id());
        if (branch.is_diode()) {
            diode_cols.push_back(switch_cols[i]);
        }
    }
}



void SwitchedModel::calculate_switch_control_variables(const VectorXd & dxdt, const VectorXd &x_old,  const VectorXd &x_new,
const VectorXd &u) 
{
    impulsive = Sdxdt * (x_new - x_old);
    nonimpulsive = Sx*x_new + Su*u;

}

void SwitchedModel::determine_topology(const branches &branches) {
    
     // std::vector<unsigned> switch_cols = _labels.get_columns_by_type(switch_var);

    double positive_voltage;
    double negative_current;
    unsigned col;
    label _label;
    ISC::internal::branch branch;

    for (unsigned i = 0; i < switch_cols.size(); i++) {
        col = switch_cols[i];
        _label = _labels.get_label(col);
        branch = branches.get_branch(_label.get_branch_id());
        
        positive_voltage = impulsive(i) > 0 || 
            (impulsive(i) == 0 && nonimpulsive(i) > 0);
        negative_current = impulsive(i) < 0 || 
            (impulsive(i) == 0 && nonimpulsive(i) < 0);

        if (branch.is_diode()) {

            if ( (_label.is_current() && negative_current) ||
                 (_label.is_voltage() && positive_voltage) ) {

                exchange_columns(branch.get_id());
            }
        }
    }
}

void SwitchedModel::toggle_switches(const std::vector<ISC::id_type> &branch_ids) {
    for (auto branch_id: branch_ids) {
        // std::cout << "Exchanging cols of " << branch_id << std::endl;
        exchange_columns(branch_id);
    }

    extract_ss_matrices();
}

std::bitset<MAX_SWITCHES> SwitchedModel::get_topology_id() {
    std::bitset<MAX_SWITCHES> result;

    std::vector<unsigned> switches = _labels.get_columns_by_type(switch_var);

    label _label;
    for (unsigned i = 0; i < switches.size(); i++) {
        _label = _labels.get_label(switches[i]);

        result = (result << i) | 
            (_label.is_current() ? std::bitset<MAX_SWITCHES>(1) : std::bitset<MAX_SWITCHES>(0)); 
    }

    // std::cout << "Topology id is \n" << result << std::endl;
    return result;
}

std::vector<ISC::id_type> SwitchedModel::get_ordered_output_branches() const
{
    std::vector<label> labels = _labels.get_output_labels();
    std::vector<ISC::id_type> result;

    std::transform(labels.begin(), labels.end(), 
        std::back_inserter(result), [&](label l){ return l.get_branch_id();});

    return result;
}
