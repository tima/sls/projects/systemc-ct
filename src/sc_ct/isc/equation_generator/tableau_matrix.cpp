#include "sc_ct/isc/equation_generator/tableau_matrix.h"

#define DEBUG

tableau_matrix::tableau_matrix()
{
    z_s = -1; 
    y_s = -1; 
    b_s = -1;
    a_s = -1; 
}

void tableau_matrix::create_reduced_tableau(const branches &branches, const cutset_matrix &D) {
    create_nonport_characteristics(branches);
    
    z_s = branches.get_count_by_type(z_nonport_in_tree);
    y_s = branches.get_count_by_type(y_nonport_in_cotree);
    b_s = branches.get_count_by_type(b_port_in_cotree);
    a_s = branches.get_count_by_type(a_port_in_tree);

    
    MatrixXd Day = D.get_Day();
    MatrixXd Dzb_t = D.get_Dzb().transpose();
    MatrixXd Fiy_hat = get_Fiy_hat(D);
    

    unsigned rows_1 = Day.rows(); 
    unsigned rows_2 = Dzb_t.rows(); 
    unsigned rows_3 =  Fiy_hat.rows(); 

    unsigned rows = rows_1 + rows_2 + rows_3;
    unsigned cols = y_s + z_s + 2 * a_s + 2 * b_s;


    // Eq. 6-67, of Computer Aided Analysis of Electronic Circuits. Chua L. 1975.
    m = MatrixXd(rows, cols);

    if (rows_1 > 0) {
        MatrixXd row(rows_1, cols);
        row << Day,  MatrixXd::Zero(rows_1, z_s),  MatrixXd::Identity(rows_1, a_s), MatrixXd::Zero(rows_1,b_s),  D.get_Dab(), MatrixXd::Zero(rows_1, a_s);
        m.block(0,0,rows_1,cols) = row;
    }

    if (rows_2 > 0) {
        MatrixXd row(rows_2, cols);
        row <<  MatrixXd::Zero(rows_2, y_s), -Dzb_t, MatrixXd::Zero(rows_2, a_s), MatrixXd::Identity(rows_2, b_s), MatrixXd::Zero(rows_2, b_s), -(D.get_Dab().transpose());
        m.block(rows_1,0,rows_2,cols) = row;
    }

    if (rows_3 > 0) {
        MatrixXd row(rows_3, cols);
        row << get_Fiy_hat(D), get_Fvz_hat(D), get_Fia_hat(D), get_Fvb_hat(D), get_Fib_hat(D), get_Fva_hat(D); 
        m.block(rows_1+rows_2,0,rows_3,cols) = row;
    }

    // Reduce
    topological_matrix::gauss_jordan_elimination(m, true);
}

const MatrixXd &tableau_matrix::get_reduced_tableau(
        const branches &branches, const cutset_matrix &D) 
{
    create_reduced_tableau(branches, D);
    return m;
}

void tableau_matrix::create_nonport_characteristics(const branches &branches) {
    unsigned rows = branches.get_nonport_count();
    unsigned cols = branches.get_count();
    
    F = MatrixXd::Zero(rows, 2*cols);

    unsigned col = 0;
    unsigned row = 0;

    //for (auto [id, branch]: branches.get_branches()) { // only supported in c++17 but not standard SC 2.3.3
    for (auto b: branches.get_branches()) {
		unsigned id = b.first;
		branch bran = b.second;
        // F contains the equations of only the
        // non-port variables
        if (bran.is_port()) {
            continue;
        }

        for (auto law_term: bran.get_law()) {
            
            if (law_term.is_current()) {
                col = branches.get_current_position_in_tableau(law_term.get_branch_id());
            }
            else {
                col = branches.get_voltage_position_in_tableau(law_term.get_branch_id());
            }

            F(row, col) = -1*law_term.get_coefficient();

        }
        
        if (bran.is_law_expressed_as_voltage()) {
            col = branches.get_voltage_position_in_tableau(id);
        }
        else {
            col = branches.get_current_position_in_tableau(id);
        
        }
        // Set coeff of current of the branch to -1 
        // i/v - (kv1*v1 + kv2*v2 + ... + ki1*i1 + ki2*i2 + ... kin*in) = 0
        F(row, col) = 1;
 
        row++;
    }
}


MatrixXd tableau_matrix::get_Fiy_hat(const cutset_matrix &D) {
    return get_Fiy() - get_Fiz()*D.get_Dzy(); 
}

MatrixXd tableau_matrix::get_Fvz_hat(const cutset_matrix &D) {
    return get_Fvz() + get_Fvy()*(D.get_Dzy().transpose()); 
}

MatrixXd tableau_matrix::get_Fib_hat(const cutset_matrix &D) {
    return get_Fib() - get_Fiz()*D.get_Dzb(); 
}

MatrixXd tableau_matrix::get_Fva_hat(const cutset_matrix &D) {
    return get_Fva() + get_Fvy()*(D.get_Day().transpose()); 
}

MatrixXd tableau_matrix::get_Fia_hat(const cutset_matrix &D) {
    return get_Fia(); 
}

MatrixXd tableau_matrix::get_Fvb_hat(const cutset_matrix &D) {
    return get_Fvb(); 
}


MatrixXd tableau_matrix::get_Fiz() {
    check_count_by_type();
    int rows = F.rows();
    return F.block(0, 0, rows, z_s); 
}

MatrixXd tableau_matrix::get_Fvy() {
    check_count_by_type();
    int rows = F.rows();
    return F.block(0, z_s, rows, y_s); 
}

MatrixXd tableau_matrix::get_Fiy() {
    check_count_by_type();
    int rows = F.rows();
    return F.block(0, z_s + y_s, rows, y_s); 
}

MatrixXd tableau_matrix::get_Fvz() {
    check_count_by_type();
    int rows = F.rows();
    return F.block(0, z_s + 2*y_s, rows, z_s); 
}

MatrixXd tableau_matrix::get_Fia() {
    check_count_by_type();
    int rows = F.rows();
    return F.block(0, 2*z_s + 2*y_s, rows, a_s); 
}

MatrixXd tableau_matrix::get_Fvb() {
    check_count_by_type();
    int rows = F.rows();
    return F.block(0, 2*z_s + 2*y_s + a_s, rows, b_s); 
}

MatrixXd tableau_matrix::get_Fib() {
    check_count_by_type();  
    int rows = F.rows();
    return F.block(0, 2*z_s + 2*y_s + a_s + b_s, rows, b_s); 
}

MatrixXd tableau_matrix::get_Fva() {
    check_count_by_type();
    int rows = F.rows();
    return F.block(0, 2*z_s + 2*y_s + a_s + 2*b_s, rows, a_s); 
}

void tableau_matrix::check_count_by_type() {
    if (a_s == -1 || b_s == -1 || z_s == -1 || y_s == -1) {
        throw std::logic_error("tableau_matrix::check_count_by_type: tried to use count by type before assignment");
    }
}
