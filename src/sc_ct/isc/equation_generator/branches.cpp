#include "sc_ct/isc/equation_generator/branches.h"

branches::branches() {

}

branches::branches(const std::vector<branch> &branches)    
{
    init_id_map(branches);
    // update_position_maps();
}

void branches::init_id_map(const std::vector<branch> &branches) {
    for (auto branch : branches) {
        map_id_to_branch[branch.get_id()] = branch;
    }
    count_types();
}

void branches::add_branch(branch &branch) {
    map_id_to_branch[branch.get_id()] = branch;
    count_types();
    // update_position_maps();
}

const std::map<unsigned, branch> &branches::get_branches() const {
    return map_id_to_branch;
}

// The user must ensure to call count_types if he does
// any modification to the branch (tree, port)
branch &branches::get_branch(unsigned branch_id) {
    return map_id_to_branch.at(branch_id);
}

const branch &branches::get_branch(unsigned branch_id) const {
    return map_id_to_branch.at(branch_id);
}

// Initialize all elements of the array of size 
// size to val
void branches::init_vector(unsigned *vector, int size, unsigned val) const {
    // Initialize counters to zero
    for(int i = 0; i < size; i++) {
        vector[i] = val;
    }

}

unsigned branches::get_current_position_in_tableau(unsigned id) const {
    return currents_map.at(id);
}

unsigned branches::get_voltage_position_in_tableau(unsigned id) const {
    return voltages_map.at(id);
}

void branches::count_types() {
    init_vector(count_by_type, BRANCH_VARS_CATEGORIES, 0);
    
    // Count branch categories
    for ( auto it = map_id_to_branch.begin(); it != map_id_to_branch.end(); it++) {
        // Tree elements (branch.get_type())
        count_by_type[it->second.get_type()]++;
    } 

    // Total sizes of each variable type
    z_s = count_by_type[z_nonport_in_tree];
    y_s = count_by_type[y_nonport_in_cotree];
    b_s = count_by_type[b_port_in_cotree];
    a_s = count_by_type[a_port_in_tree];

    nonport_count = z_s + y_s;
}

unsigned branches::get_count_by_type(branch_type type) const {
    return count_by_type[type];
}

// Returns the position of the current variable of the 
// branch in the tableau matrix (Eq. 6-66 of Chua & Lin, 1975)
int branches::get_i_position_in_tableau(const branch &branch) {
    // Position to return
    int pos = -1;

    // Calculate positon: one formula per type
    switch(branch.get_type()){
        case z_nonport_in_tree:
            pos = current_count[z_nonport_in_tree];
            current_count[z_nonport_in_tree]++;
            break;
        case y_nonport_in_cotree:
            // pos = |iz| + |vy| + current_count
            pos = z_s + y_s + current_count[y_nonport_in_cotree];
            current_count[y_nonport_in_cotree]++;
            break;
        case a_port_in_tree:
            // pos = |iz| + |vy| + |iy| + |vz| + current_count
            pos = 2*z_s + 2*y_s + current_count[a_port_in_tree];
            current_count[a_port_in_tree]++;
            break;
        case b_port_in_cotree:
            // pos = |iz| + |vy| + |iy| + |vz| + |ia| + |vb| + current_count
            pos = 2*z_s + 2*y_s + a_s + b_s + current_count[b_port_in_cotree];
            current_count[b_port_in_cotree]++;
            break;
        default:
            throw(std::invalid_argument("FMatrix::calc_i_pos:  element type did not match any case"));
    }

    return pos;     
}

// Return the position of the voltage variable of the 
// branch in the tableau matrix (Eq. 6-66 of Chua & Lin, 1975)
int branches::get_v_position_in_tableau(const branch &branch, int i_pos) {
    // Position to return
    int pos = -1;

    // Throw exception if a bad electrical current position
    // is passed as parameter
    if(i_pos < 0){
        throw(std::invalid_argument("FMatrix::calc_v_pos:  invalid electrical current position"));
        return -1;
    }

    // Calculate positon: one formula per type
    switch(branch.get_type()){
        case z_nonport_in_tree:
            // pos = iz_pos + |iz| + |vy| + |iy|
            pos = i_pos + z_s + 2*y_s;
            break;
        case y_nonport_in_cotree:
            // pos = iy_pos - |vy|
            pos = i_pos - y_s;
            break;
        case a_port_in_tree:
            // pos = ia_pos + |vb| + |i_b|
            pos = i_pos + 2*b_s + a_s;
            break;
        case b_port_in_cotree:
            // pos = ib_pos - |vb|
            pos = i_pos - b_s;
            break;
        default:
            throw(std::invalid_argument("FMatrix::calc_v_pos:  element type did not match any case"));
    }

    return pos;     
}

unsigned branches::get_nonport_count() const {
    return nonport_count;
}

unsigned branches::get_count() const {
    return map_id_to_branch.size();
}

void branches::update_position_maps(labels _labels) {
    int pos;

    init_vector(current_count, BRANCH_VARS_CATEGORIES, 0);

    // Create currents and voltages map
    // Key: branch id, value: position

    for (auto _label: _labels.get_labels()) {
        unsigned id = _label.get_branch_id();

        branch branch = map_id_to_branch[id];
    
        pos = get_i_position_in_tableau(branch);
        currents_map[id] = pos;

        pos = get_v_position_in_tableau(branch, pos);
        voltages_map[id] = pos;
    }
}


void branches::print_branch_positions() {

    // |i_y| + |v_z| + |i_a| + |v_b| + |i_b| + |v_a|
    unsigned vars_count = 2*get_count();
    bool found = false;
    std::cout << "branch positions for [Fb|Fp]" << std::endl;
    for(unsigned i = 0; i < vars_count; i++) {
        found = false;
        //for(auto [branch_id, pos]: currents_map){ // only supported in c++17 but not standard SC 2.3.3
        for(auto cm: currents_map){
			unsigned branch_id = cm.first;
			unsigned pos = cm.second;
            if (pos == i) {
                std::cout << "["<< i <<"] i_" << (branch_id) << std::endl;
                found = true;
                break;
            }
        }
        if(found) {
            continue;
        }
        //for(auto [branch_id, pos]: voltages_map){ // only supported in c++17 but not standard SC 2.3.3
        for(auto vm: voltages_map){ 
			unsigned branch_id = vm.first;
			unsigned pos = vm.second;
            if (pos == i) {
                std::cout << "["<< i <<"] v_" <<(branch_id) << std::endl;
                found = true;
                break;
            }
        }

        if(!found)
        std::cout << "DID NOT FOUND " << i << std::endl;
    }

}

std::vector<label> branches::get_reduced_tableau_labels() const {
    std::vector<label> _labels;

    // |i_y| + |v_z| + |i_a| + |v_b| + |i_b| + |v_a|
    unsigned vars_count = 2*get_count() - y_s - z_s;
    bool found = false;
    label _label;
    unsigned shifted_pos;
    branch branch;

    for(unsigned i = 0; i < vars_count; i++) {
        found = false;
        //for(auto [branch_id, pos]: currents_map){ // only supported in c++17 but not standard SC 2.3.3
        for(auto cm: currents_map){
			unsigned branch_id = cm.first;
			unsigned pos = cm.second;
            shifted_pos = pos - y_s - z_s;
            if (shifted_pos == i) {
                branch = map_id_to_branch.at(branch_id);
                _label.set_branch_id(branch_id);
                _label.set_col_type(current_t);
                _label.set_ss_type(branch.get_current_ss_type());
                _labels.push_back(_label);
                found = true;
                break;
            }
        }
        if(found) {
            continue;
        }
        //for(auto [branch_id, pos]: voltages_map){ // only supported in c++17 but not standard SC 2.3.3
		for(auto vm: voltages_map){ 
			unsigned branch_id = vm.first;
			unsigned pos = vm.second;
			shifted_pos = pos - y_s - z_s;
			if (shifted_pos == i) {
				branch = map_id_to_branch.at(branch_id);
				_label.set_branch_id(branch_id);
				_label.set_col_type(voltage_t);
				_label.set_ss_type(branch.get_voltage_ss_type());
				_labels.push_back(_label);
				found = true;
				break;
			}
		}

        if(!found)
        std::cout << "DID NOT FOUND " << i << std::endl;
	}

    return _labels;
}
