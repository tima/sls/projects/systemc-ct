#ifndef LABEL_H
#define LABEL_H

#include <iostream>
#include <vector>

#include "sc_ct/isc/common/common.h"
using namespace ISC::internal;


class ISC::internal::label {
    public: 
        label(unsigned id = 0, col_type t = current_t, 
            state_space_type ss_t = nonport_var);

        void set_branch_id(unsigned); 
        unsigned get_branch_id() const;

        void set_col_type(col_type);
        col_type get_col_type() const;

        void set_ss_type(state_space_type);
        state_space_type get_ss_type() const;

        bool is_current() const;
        bool is_voltage() const;

        friend std::ostream& operator<<(std::ostream&, const label&);


        // Returns the state space variable types in the order 
        // specified by Massarini and Reggiani (2001)
        static std::vector<state_space_type> get_ss_types(); 

    private:
        col_type _col_type;
        state_space_type ss_type;
        
        unsigned branch_id;
};

#endif
