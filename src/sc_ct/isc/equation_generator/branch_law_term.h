#ifndef BRANCH_LAW_TERM_H
#define BRANCH_LAW_TERM_H

#include "sc_ct/isc/common/common.h"

class ISC::internal::branch_law_term {

    public:
        branch_law_term(unsigned id =-1, bool is_current = false, double k = 0.0) :
            id(id), 
            _is_current(is_current),
            k(k)
        {
        }

        unsigned get_branch_id() {
            return id;
        }

        bool is_current() {
            return _is_current;
        }

        double get_coefficient() {
            return k;
        }

        void set_coefficient(double c) {
            k = c;
        }

        void set_is_current(bool is_current_val) {
            _is_current = is_current_val;
        }

        void set_branch_id(unsigned branch_id) {
            id = branch_id;
        }

    private:
        // ISC::internal::branch id
        unsigned id;
        // Multiplies either current or voltage
        bool _is_current;
        // Term coefficient 
        double k;

};

#endif
