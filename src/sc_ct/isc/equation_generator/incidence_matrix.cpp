#include "sc_ct/isc/equation_generator/incidence_matrix.h"

// #define DEBUG 1

incidence_matrix::incidence_matrix(const MatrixXd &m, 
            const labels &_labels)
    : topological_matrix(m, _labels)
{

}

int incidence_matrix::reorder_tree_cotree(branches &branches){
    MatrixXd A_backup(m); 
    std::vector<unsigned> pivots;

    // Get pivot columns
    pivots = topological_matrix::gauss_jordan_elimination(A_backup);

    // Just for testing ex 6-4 Chua 
    // pivots.push_back(0);
    // pivots.push_back(1);
    // pivots.push_back(4);
    // pivots.push_back(5);

    label _label;    
    for (unsigned i = 0; i < pivots.size(); i++) {
        _label = _labels.get_label(pivots[i]);
        branches.get_branch(_label.get_branch_id()).set_tree(true);
        swap_cols(i, pivots[i]);
    }

    // Update branches type count
    branches.count_types();
    branches.update_position_maps(_labels);


    return pivots.size();
}

void incidence_matrix::reorder_port_nonport(branches &branches) {
    int tree_elements_count = m.rows();
    int cols = m.cols();

    reorder_tree_cotree(branches);


    // Reorder tree
    swap_port_columns_first(branches, 0, tree_elements_count);
    // Reorder cotree
    swap_port_columns_last(branches, tree_elements_count, cols);

    // Update branches type count
    branches.count_types();
    branches.update_position_maps(_labels);
}

// Calculate the cutset matrix by matrix inversion.
// We cna improve efficiency by calculating it directly
// during gauss elimination with back substitution
MatrixXd incidence_matrix::get_cutset_matrix(branches &branches) {
    MatrixXd loop_matrix, cutset_matrix;
    reorder_port_nonport(branches);
    get_loop_and_cutset_matrices(m, loop_matrix, cutset_matrix);
    return cutset_matrix;
}

// Calculates loop (B) and cutset (D) matrices from the incidence matrix A
// using matrix in version (See Chua & Lin 1975 for notation/equations). 
// Assumes that A is ordered in tree and cotree
void incidence_matrix::get_loop_and_cutset_matrices(const MatrixXd &A, MatrixXd &B, MatrixXd &D) const {
    int rows = A.rows();
    int cols = A.cols();

    MatrixXd At_inv = A.block(0, 0, rows,rows).inverse();
    int Al_cols = cols - rows;
    // Al is a block of size rows*Al_cols 
    // starting at (0, rows)  
    MatrixXd Al = A.block(0, rows, rows, Al_cols);


    MatrixXd Bt = get_Bt(At_inv, Al);
    B = get_B_from_Bt(Bt);

    MatrixXd Dl = get_Dl_from_Bt(Bt);
    D = get_D_from_Dl(Dl);

    #ifdef DEBUG 
    std::cout << "A\n" << A << std::endl;
    std::cout << "At_inv\n" << At_inv << std::endl;
    std::cout << "Al\n" << Al << std::endl;
    std::cout << "Bt\n" << Al << std::endl;
    std::cout << "B\n" << B << std::endl;
    std::cout << "Dl\n" << Dl << std::endl;
    std::cout << "D\n" << D << std::endl;
    #endif
}

// Given At^-1 and Al such that A = [ At | Al ],
// returns Bt = transpose(-At^-1 * Al)
MatrixXd incidence_matrix::get_Bt(const MatrixXd &At_inv, const MatrixXd &Al) const {
    return (-At_inv*Al).transpose();
}

// Given Bt return Dl
MatrixXd incidence_matrix::get_Dl_from_Bt(const MatrixXd &Bt) const {
    return (-Bt).transpose();
}

// Given Bt return B
MatrixXd incidence_matrix::get_B_from_Bt(const MatrixXd &Bt) const {
    int rows = Bt.rows();
    int cols = Bt.cols();

    MatrixXd B(rows, cols + rows);
    // Comma initialization
    B << Bt, MatrixXd::Identity(rows, rows);

    return B;
}


// Given Dl return D
MatrixXd incidence_matrix::get_D_from_Dl(const MatrixXd &Dl) const {
    int rows = Dl.rows();
    int cols = Dl.cols();

    MatrixXd D(rows, rows+cols);
    // Comma initialization
    D << MatrixXd::Identity(rows, rows), Dl;

    return D;
}




void incidence_matrix::swap_port_columns_first(branches branches, int from, int to) {
    ISC::internal::branch branch;
    unsigned id;

    std::vector<int> to_swap;

    for(int i = from; i < to; i++) {
        id = _labels.get_label(i).get_branch_id();
        branch = branches.get_branch(id);
        if (branch.is_port()) {
            to_swap.push_back(i);
        }
    } 

    for(auto el: to_swap) {
        swap_cols(el, from);
        from++;
    }

}

void incidence_matrix::swap_port_columns_last(branches branches, int from, int to) {
    ISC::internal::branch branch;
    unsigned id;

    std::vector<int> to_swap;

    for(int i = from; i < to; i++) {
        id = _labels.get_label(i).get_branch_id();
        branch = branches.get_branch(id);
        if (!branch.is_port()) {
            to_swap.push_back(i);
        }
    } 

    for(auto el: to_swap) {
        swap_cols(el, from);
        from++;
    }
}
