#ifndef SWITCHED_MODEL_H
#define SWITCHED_MODEL_H

#include <vector>
#include <bitset>
#include <Eigen/Dense>
#include "sc_ct/isc/equation_generator/topological_matrix.h"
#include "sc_ct/isc/equation_generator/branches.h"
#include "sc_ct/core/ct_state.h"

using namespace Eigen;
using namespace ISC::internal;

#define MAX_SWITCHES 64

// To allow for comparisons between bitsets
struct bitset_comparator {

    template<std::size_t N>
    bool operator() (const std::bitset<N> &x, const std::bitset<N> &y) const
    {
        for (int i = N-1; i >= 0; i--) {
            if (x[i] ^ y[i]) return y[i];
        }
        return false;
    }
};

// TO OPTIMIZE EIGEN
#define NDEBUG 

#define STORE_MATRICES_BY_TOPOLOGY 1

class SwitchedModel :  public topological_matrix {
    public: 
        SwitchedModel(const MatrixXd &m, const labels &_labels, 
            const branches &branches,
            unsigned outputs_start_row, unsigned outputs_start_col, unsigned switches_start_row, unsigned switches_start_col);

        void extract_ss_matrices();

        sct_core::ct_state get_derivatives(const sct_core::ct_state &x, const sct_core::ct_state &u);

        sct_core::ct_state get_outputs(const sct_core::ct_state &x, const sct_core::ct_state &u);

 
        void exchange_columns(unsigned branch_id);

        std::vector<unsigned> get_zero_row_positions(const MatrixXd &);
        VectorXd calculate_dependent_state_vars(const VectorXd &x, const VectorXd &u);
        void calculate_switch_control_variables(const VectorXd & dxdt, const VectorXd & x_old,  const VectorXd &x_new, const VectorXd &u);

        void determine_topology(const branches &);

        std::vector<ISC::id_type> get_internal_switches_to_toggle(const branches &branches, const sct_core::ct_state &dxdt, const sct_core::ct_state &x, const sct_core::ct_state &u);
        void toggle_switches(const std::vector<ISC::id_type> &);
        std::bitset<MAX_SWITCHES> get_topology_id();

        std::vector<ISC::id_type> get_ordered_output_branches() const;

    private:
        // Matrix that contains the columns 
        // that correspond to the independent
        // state variables and the rows 
        // corresponding to the zero rows 
        // of M_ss
        MatrixXd Adi;
        // Similar to Ai but for the dependent 
        // variables
        MatrixXd Add_inv;
        // Bd contains all rows of B_ss 
        // that correspond to the zero 
        // rows in M_ss
        MatrixXd Bd;

        // SS output-related matrices
        MatrixXd C1_ss;
        MatrixXd C_ss;
        MatrixXd D_ss;
        // SS state-related matrices
        MatrixXd M_ss;
        MatrixXd A_ss;
        MatrixXd B_ss;
        // Switch related matrices 
        MatrixXd Sdxdt;
        MatrixXd Sx;
        MatrixXd Su;
        MatrixXd S_imp;

        #ifdef STORE_MATRICES_BY_TOPOLOGY
            std::map<std::bitset<MAX_SWITCHES>, MatrixXd, bitset_comparator> m_map;
           
        #endif
        std::map<std::bitset<MAX_SWITCHES>, std::vector<unsigned>, bitset_comparator> M_ss_pivots_map;
        std::vector<unsigned> M_ss_pivots;
        std::map<std::bitset<MAX_SWITCHES>, std::vector<unsigned>, bitset_comparator> M_ss_zero_rows_map;
        std::vector<unsigned> M_ss_zero_rows;

        // Coordinates of the Q matrix in
        // m
        unsigned outputs_start_row;
        unsigned outputs_start_col;
        unsigned number_of_outputs;
        
        // Start row of switch control variables
        unsigned switches_start_row; 
        unsigned switches_start_col;
        unsigned number_of_switches;

        unsigned derivatives_start_col;
        unsigned ss_start_col;
        unsigned number_of_state_variables;
        unsigned number_of_inputs;
        unsigned impulsive_var_col_shift;

        // Impulsive and nonimpulsive 
        // switch control variables
        VectorXd impulsive, nonimpulsive;   
        std::vector<unsigned> dependent_state_vars, independent_state_vars;
        std::vector<unsigned> input_cols; 
        std::vector<unsigned> switch_cols;         
        std::vector<unsigned> state_var_cols;
        std::vector<unsigned> derivative_var_cols;
        std::vector<unsigned> diode_cols;


        void calc_dependent_state_vars();

        void calc_diode_cols(const branches &branches);

        void precalculate_dependent_state_matrices();
        // Dependent state matrices
        // MatrixXd Adi;
        // MatrixXd Add;
        // MatrixXd Bd;

};

#endif
