#ifndef BRANCH_H
#define BRANCH_H

#include <vector>
#include <iostream>
#include <string>

#include "sc_ct/isc/common/common.h"
#include "sc_ct/isc/equation_generator/branch_law_term.h"
#include "sc_ct/isc/equation_generator/label.h"

using namespace ISC::internal;


class ISC::internal::branch {

    public: 
        // To assign a different id to each branch
        static ISC::id_type count;

        branch();

        void set_element(ISC::element el);

        void set_on_state(bool s);

        bool is_on() const;

        ISC::element get_element() const;

        state_space_type get_current_ss_type();

        bool is_diode() const;

        state_space_type get_voltage_ss_type();

        void set_tree(bool v);

        void set_port(bool p);

        bool is_tree() const;

        bool is_port() const;

        unsigned get_id() const;

        std::vector<branch_law_term> get_law() const;

        // Add a term to the branch law expressed as either:
        // i = kv1*v1 + kv2*v2 + ... + ki1*i1 + ki2*i2 + ... kin*in  
        // or
        // v = kv1*v1 + kv2*v2 + ... + ki1*i1 + ki2*i2 + ... kin*in  
        // as signaled by law_expressed_as_voltage()
        void add_law_term(unsigned branch_id, 
            bool is_current, double coef);

        bool is_law_expressed_as_voltage();

        void set_law_expressed_as_voltage(bool val);

        // Removes all terms from the law
        void remove_law();
        void print_law() const;
        ISC::internal::branch_type get_type() const;


        friend std::ostream& operator<<(std::ostream&os, const branch&b);

        bool set_parameter_value(double val);

        double get_parameter_value() const;
    
    private:
        bool tree;
        bool port;
        ISC::id_type id;
        ISC::internal::branch_type type;
        std::vector<branch_law_term> law;
        bool law_expressed_as_voltage;
        ISC::element element;
        bool on_state;

        // Value of R, C, L, voltage, current, etc.
        double parameter_value;
};
#endif
