#ifndef SS_MATRIX_H
#define SS_MATRIX_H

#include <vector>
#include <Eigen/Dense>
#include "sc_ct/isc/equation_generator/topological_matrix.h"
#include "sc_ct/isc/equation_generator/cutset_matrix.h"
#include "sc_ct/isc/equation_generator/branches.h"

using namespace Eigen;
using namespace ISC::internal;

// Class that creates the state
// equations of a switched network
// given the FbFp topological matrix
class ISC::internal::ss_matrix : public topological_matrix {

    public: 
        ss_matrix (const MatrixXd &, const labels &, const branches &);

        unsigned get_Q_start_row ();
        unsigned get_Q_start_col ();
        unsigned get_S_start_row ();
        unsigned get_S_start_col ();
        
    private:
        MatrixXd T;
        MatrixXd U;
        MatrixXd S;
        MatrixXd Q;
        MatrixXd M;

        // Coordinates of the Q matrix in
        // m
        unsigned Q_start_row;
        unsigned Q_start_col;
        
        // Start row of switch control variables
        unsigned S_start_row; 
        unsigned S_start_col;


        // Set to true if there are underdetermined
        // variables (rectangular U, S, Q or M)
        bool is_underdetermined;


        // Utility functions
        void reorder_matrix(const branches &);

        void partition();
        
        unsigned find_first_zero_row(unsigned);

        //
        bool is_singular(const MatrixXd &) const;
        bool is_square(const MatrixXd &) const;
        bool has_zero_row(const MatrixXd &) const;
        std::vector<unsigned> identify_underdetermined_output_vars();
        bool excess_M_rows_identically_satisfied();

        void calculate_ss();
        void transform_Q_to_identity();
        void verify_network_consistency();
        unsigned get_first_consecutive_pivots_count(const MatrixXd &, unsigned, unsigned) const;

};

#endif
