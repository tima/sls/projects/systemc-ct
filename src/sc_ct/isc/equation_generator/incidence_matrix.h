#ifndef INCIDENCE_MATRIX_H
#define INCIDENCE_MATRIX_H

#include <vector>
#include <Eigen/Dense>
#include "sc_ct/isc/equation_generator/topological_matrix.h"
#include "sc_ct/isc/equation_generator/branches.h"

using namespace Eigen;
using namespace ISC::internal;

class ISC::internal::incidence_matrix : public topological_matrix {
    public: 
        incidence_matrix(const MatrixXd &,
            const labels &); 
        
        int reorder_tree_cotree(branches &);
        void reorder_port_nonport(branches &);

        // Note that we use Gauss-Jordan elimination
        // with back substitution to calculate the 
        // cutset matrix  instead of matrix inversion 
        // because in any case we have to find the tree and cotree
        // of the incidence matrix which is done by Gauss-Jordan.
        MatrixXd get_cutset_matrix(branches &);

    private:

        // Utility
        void swap_port_columns_first(branches, int, int);
        void swap_port_columns_last(branches, int, int);

        void get_loop_and_cutset_matrices(const MatrixXd &, MatrixXd &, MatrixXd &) const;
        MatrixXd get_Bt(const MatrixXd &, const MatrixXd &) const;
        MatrixXd get_Dl_from_Bt(const MatrixXd &) const;
        MatrixXd get_D_from_Dl(const MatrixXd &) const;
        MatrixXd get_B_from_Bt(const MatrixXd &) const;

};

#endif
