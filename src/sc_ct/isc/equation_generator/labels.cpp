#include "sc_ct/isc/equation_generator/labels.h"

labels::labels(const std::vector<label> &_labels) 
    : _labels(_labels)
{
    
}

void labels::set_label(const unsigned &col, label _label) {
    _labels.at(col) = _label;
}

label labels::get_label(const unsigned &col) const {
    return _labels.at(col);
}


const std::vector<label> &labels::get_labels() const {
    return _labels;
}

void labels::set_labels(const std::vector<label> &_labels) {
    this->_labels = _labels;
 }

void labels::swap_labels(unsigned col_a, unsigned col_b) {
    label aux = _labels[col_a];
    _labels[col_a] = _labels[col_b];
    _labels[col_b] = aux;
}

bool labels::is_valid_col(unsigned col) const {
    return col < _labels.size();
}

unsigned labels::get_count_by_ss_type(state_space_type t) {
    unsigned count = 0;
    for (auto _label : _labels) {
        if (_label.get_ss_type() == t) {
            count++;
        }
    }

    return count;
}

std::vector<unsigned> labels::get_columns_by_type(state_space_type t) const {
    std::vector<unsigned> positions;

    int i = 0;
    for (auto _label: _labels) {
        if (_label.get_ss_type() == t) {
            positions.push_back(i);
        }
        i++;
    } 
    return positions;
}

unsigned labels::size() const {
    return _labels.size();
}

void labels::erase(unsigned col) {
    _labels.erase(_labels.begin() + col);
}

std::vector<label> labels::get_output_labels() const {
    std::vector<label> result;

    for(const auto &_label: _labels){
        if (_label.get_ss_type() == output_var) {
            result.push_back(_label);
        }
    }

    return result;
}
