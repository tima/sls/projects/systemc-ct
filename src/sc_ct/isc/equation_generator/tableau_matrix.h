#ifndef TABLEAU_MATRIX_H
#define TABLEAU_MATRIX_H

#include <vector>
#include <Eigen/Dense>
#include "sc_ct/isc/equation_generator/topological_matrix.h"
#include "sc_ct/isc/equation_generator/cutset_matrix.h"

using namespace Eigen;
using namespace ISC::internal;

class ISC::internal::tableau_matrix {
    public: 
        tableau_matrix(); 

        // Create the [Fb Fp] matrix
        // Eq. 6-67 Chua et Lin, 1975
        const MatrixXd &get_reduced_tableau(const branches &, const cutset_matrix &); 
        
    private:
        MatrixXd F;
        MatrixXd m;

        int z_s; 
        int y_s; 
        int b_s;
        int a_s; 

        //
        void create_reduced_tableau(const branches &, const cutset_matrix &);
        MatrixXd get_Fiy_hat(const cutset_matrix &);
        MatrixXd get_Fvz_hat(const cutset_matrix &);
        MatrixXd get_Fib_hat(const cutset_matrix &); 
        MatrixXd get_Fva_hat(const cutset_matrix &);
        MatrixXd get_Fia_hat(const cutset_matrix &);
        MatrixXd get_Fvb_hat(const cutset_matrix &);
        void create_nonport_characteristics(const branches &branches);
        void check_count_by_type();
        MatrixXd get_Fiz(); 
        MatrixXd get_Fvy(); 
        MatrixXd get_Fiy(); 
        MatrixXd get_Fvz(); 
        MatrixXd get_Fia(); 
        MatrixXd get_Fvb(); 
        MatrixXd get_Fib(); 
        MatrixXd get_Fva();
};

#endif
