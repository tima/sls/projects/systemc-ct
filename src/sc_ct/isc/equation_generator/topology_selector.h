#ifndef TOPOLOGY_SELECTOR_H
#define TOPOLOGY_SELECTOR_H

#include <set>
#include <vector>

#include "sc_ct/core/ct_state.h"
#include "sc_ct/isc/common/common.h"
#include "sc_ct/isc/equation_generator/branches.h"
#include "sc_ct/isc/equation_generator/cutset_matrix.h"
#include "sc_ct/isc/equation_generator/incidence_matrix.h"
#include "sc_ct/isc/equation_generator/tableau_matrix.h"
#include "sc_ct/isc/equation_generator/ss_matrix.h"
#include "sc_ct/isc/equation_generator/switched_model.h"


using namespace ISC::internal;


class ISC::internal::topology_selector {

    public: 
        // Constructor from incidence matrix, branches and labels
        topology_selector ();

        void init(const MatrixXd &, const branches &, const labels &);

        // Return the derivative values according to the current
        // circuit topology, the current state x, and the set of inputs
        // u 
        sct_core::ct_state get_derivatives(const sct_core::ct_state &x, const sct_core::ct_state &u);


        // Set the switch state,
        bool set_switch_states(std::vector<ISC::id_type> branch_ids, double t);

        // Toggle the switches given their branch ids
        // It receives the time to store the last switching time
        // of the circuit
        void toggle_switches(std::vector<ISC::id_type> branch_ids, double t);

        // Return a vector of internally controlled switches, such as 
        // diodes, given the current state, inputs and time
        std::vector<ISC::id_type> get_internal_switches_to_toggle( 
            const sct_core::ct_state &x, const sct_core::ct_state &u, double t);

        sct_core::ct_state get_outputs(const sct_core::ct_state &x, const sct_core::ct_state &u);

        std::vector<ISC::id_type> get_ordered_output_branches() const;

        ~topology_selector();

    private:
        bool switch_state;
        unsigned switch_branch_id;
        unsigned diode_branch_id;
        double last_switching_time;
        // Set of topologies that the system has had during the 
        // current execution time
        std::set<std::bitset<MAX_SWITCHES>, bitset_comparator> traversed_topologies;

        incidence_matrix *A;
        cutset_matrix *D;
        ss_matrix *SS;
        SwitchedModel *circuit_model;
        branches _branches;


        void check_repeated_topology(double t);
};

#endif
