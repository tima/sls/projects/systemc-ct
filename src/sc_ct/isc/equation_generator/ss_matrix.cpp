#include "sc_ct/isc/equation_generator/ss_matrix.h"


ss_matrix::ss_matrix (const MatrixXd &m, 
    const labels &_labels, const branches &branches) :
    topological_matrix(m, _labels),
    is_underdetermined(false)
{
    // std::cout << "** Reordering matrix" << std::endl;
    reorder_matrix(branches); 
    // std::cout << "** Partitioning" << std::endl;
    partition();
    // std::cout << "** Verifying " << std::endl;
    verify_network_consistency();
    // std::cout << "** Transforming Q" << std::endl;
    transform_Q_to_identity();
}


// Reorder the topological matrix 
// according to [w ~u s y x' x 0 u] 
// See Massarini and Reggiani (2001)
void ss_matrix::reorder_matrix(const branches &branches) {
    // Assume that the types are returned in the
    // order specified by Massarini and Reggiani (2001)
    int new_col = 0;
    int start_of_derivatives = -1;
    int start_of_state = -1;

    for (auto type: label::get_ss_types()) {
        int col = 0;
        for (auto label: _labels.get_labels()) {
            if (label.get_ss_type() == type) {
                
                if (start_of_derivatives == -1 && type == derivative_var ) {
                    start_of_derivatives = new_col;
                }

                if (start_of_state == -1 && type == state_var ) {
                    start_of_state = new_col;
                }

                // Swap colums in matrix
                swap_cols(col, new_col);
                new_col++;
            }
            col++;
        }
    }

    // Reorder state variables according to derivatives order
    int end_of_state = start_of_state + (start_of_state - start_of_derivatives - 1);
    new_col = start_of_state;
    label label1, label2;
    ISC::internal::branch branch;

    for (int i = start_of_derivatives; i < start_of_state; i++) {
        label1 = _labels.get_label(i);
        
        // Multiply the corresponding derivative column by 
        // the capacitor/inductor parameter value according to the 
        // element law ic = C*dvc/dt, vl = L*dic/dt
        branch = branches.get_branch(label1.get_branch_id());
        m.col(i) = m.col(i) * branch.get_parameter_value();

         // Reorder state variables according to derivatives order
        for (int j = start_of_state; j <= end_of_state; j++) {
            label2 = _labels.get_label(j);
            if (label1.get_branch_id() == label2.get_branch_id()) {
                // Swap colums in matrix
                swap_cols(new_col, j);
                new_col++;
            }
        }

    }
     
}

void ss_matrix::partition() {
    topological_matrix::gauss_jordan_elimination(m, true);

    unsigned rows_shift = 0;
    unsigned cols_shift = 0;
    unsigned cols = 0;
    unsigned rows = 0;

    MatrixXd *matrices[] = {&T, &U, &S, &Q};
    int i = 0;

    for (auto el : label::get_ss_types()) {

        if (el == derivative_var) {
            break;
        }

        if (el == output_var) {
            Q_start_row = rows_shift;
            Q_start_col = cols_shift;
        }

        if (el == switch_var) {
            S_start_row = rows_shift;
            S_start_col = cols_shift;
        }

        cols = _labels.get_count_by_ss_type(el);
        rows = get_first_consecutive_pivots_count(m, rows_shift, cols_shift);
        rows = rows > cols ? cols : rows;

        *(matrices[i++]) = m.block(rows_shift, cols_shift, rows, cols);
        rows_shift += rows;
        cols_shift += cols;
    }

    M = m.block(rows_shift, cols_shift, m.rows() - rows_shift, m.cols() - cols_shift);

    // std::cout << "T is \n" << T << std::endl;
    // std::cout << "U is \n" << U << std::endl;
    // std::cout << "S is \n" << S << std::endl;
    // std::cout << "Q is \n" << Q << std::endl;
}

unsigned ss_matrix::get_Q_start_row () {
    return Q_start_row;
}

unsigned ss_matrix::get_Q_start_col () {
    return Q_start_col;
}

unsigned ss_matrix::get_S_start_row () {
    return S_start_row;
}

unsigned ss_matrix::get_S_start_col () {
    return S_start_col;
}

unsigned ss_matrix::get_first_consecutive_pivots_count(const MatrixXd &mat, unsigned start_row, unsigned start_col) const {
    unsigned count = 1;
    unsigned row = start_row + count;
    unsigned col = start_col + count;
    
    for (count = 1; row < mat.rows() && col < mat.cols(); count++) {
        if ( mat(row, col) == 0 ){
            break;
        }
        row = start_row + count;
        col = start_col + count;
    }
    
    return count - 1;
}

unsigned ss_matrix::find_first_zero_row(unsigned col) {
    unsigned i = 0;
    for (i = 0; i < m.rows(); i++) {
        if (m(i,col) == 0) {
            return i;
        }
    }
    return i;
}

void ss_matrix::verify_network_consistency() {
    if (is_singular(T)) {
        throw std::domain_error("ss_matrix::verify_network_consistency: Matrix T is singular; the variables of the circuit elements inside the n-port are not determined.");
    }

    // Network generally inconsistent, try to remove underdetermined variables
    if ( !(is_square(U) && is_square(S) && is_square(Q) && is_square(M)) ) {

        is_underdetermined = true;


        if (!excess_M_rows_identically_satisfied()) {
            throw std::domain_error("ss_matrix::verify_network_consistency: the network is inconsistent. Excess rows in M not identically satisfied");
        }

        // Remove underdetermined output variables
        if (!is_square(Q)){
            std::vector<unsigned> cols_to_remove = identify_underdetermined_output_vars();

            for (auto col: cols_to_remove) {
                remove_col(col);
            }
        }   
    }
    else {
        if (has_zero_row(M)) {
            throw std::domain_error("ss_matrix::verify_network_consistency: M is square and has zero rows");
        }
    }
}

void ss_matrix::calculate_ss() {
    transform_Q_to_identity();
    // extract_ss_matrices();
}

void ss_matrix::transform_Q_to_identity() {
    unsigned start_row = Q_start_row;
    unsigned end_row = Q_start_row + _labels.get_count_by_ss_type(output_var);
    unsigned start_col = Q_start_col;
    unsigned end_col =  Q_start_col+_labels.get_count_by_ss_type(output_var);
    topological_matrix::gauss_jordan_elimination(m, true, start_row, end_row, start_col, end_col);
}

bool ss_matrix::excess_M_rows_identically_satisfied() {
    // DImensions of the rowspace (rank) must be no more than
    // the size of x
    FullPivLU<MatrixXd> lu_decomp(M);
    auto rank = lu_decomp.rank();

    if (rank > _labels.get_count_by_ss_type(state_var)) {
        return false;
    }
    
    // What sould we do in this case ? 
    if (rank < _labels.get_count_by_ss_type(state_var)) {
        throw std::domain_error("ss_matrix::excess_M_rows_identically_satisfied: rank(M) < number of state varialbes (some variables are underdetermined) ");
    }

    return true;
}

bool ss_matrix::is_singular(const MatrixXd &mat) const {

    Eigen::FullPivLU<MatrixXd> lu(mat);
    
    return !lu.isInvertible();
}

bool ss_matrix::is_square(const MatrixXd &mat) const {
    return mat.rows() == mat.cols();
}

bool ss_matrix::has_zero_row(const MatrixXd &mat) const {
    bool all_zeros = true;
    for (unsigned row = 0; row < mat.rows(); row++) {
        all_zeros = true;
        for(unsigned col = 0; col < mat.cols(); col++) {
            if (mat(row, col) != 0) {
                all_zeros = false;
                break;
            }
        }
        if (all_zeros) {
            return true;
        }
    }
    
    return false;
}

std::vector<unsigned> ss_matrix::identify_underdetermined_output_vars() {
    std::vector<unsigned> cols;

    if (is_square(Q)) {
        return cols;
    }

    unsigned start_row = Q_start_row;
    unsigned end_row = Q_start_row + Q.rows();
    unsigned start_col = Q_start_col;
    unsigned end_col = Q_start_col + Q.cols();

    std::vector<unsigned> pivots = topological_matrix::gauss_jordan_elimination(m, true, start_row, end_row,
        start_col, end_col);

    bool determined = false;
    for (unsigned i = start_col; i < end_col; i++) {
        
        for (auto el: pivots) {
            if (el == i) {
                determined = true;
                break;
            }
        }

        if (!determined) {
            cols.push_back(i);
        }
    }

    return cols;
}
