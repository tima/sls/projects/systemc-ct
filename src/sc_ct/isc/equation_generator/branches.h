#ifndef BRANCHES_H
#define BRANCHES_H

#include <vector>
#include <map>
#include "branch.h"
#include "label.h"
#include "labels.h"

#include "sc_ct/isc/common/common.h"

// Cardinality of {tree, cotree}*{port, non-port}
#define BRANCH_VARS_CATEGORIES 4


class ISC::internal::branches {
    public: 
        branches();
        branches(const std::vector<branch> &);

        void add_branch(branch &);
        branch &get_branch(unsigned);
        const branch &get_branch(unsigned) const; 
        unsigned get_count_by_type(ISC::internal::branch_type) const;
        unsigned get_nonport_count() const;
        unsigned get_count() const;
        unsigned get_current_position_in_tableau(unsigned) const;
        unsigned get_voltage_position_in_tableau(unsigned) const;
        void count_types();
        void update_position_maps(labels);

        const std::map<unsigned, branch> &get_branches() const;

        void print_branch_positions();
        std::vector<label> get_reduced_tableau_labels() const;

    private:
        unsigned count_by_type[BRANCH_VARS_CATEGORIES];
        unsigned nonport_count;
        unsigned current_count[BRANCH_VARS_CATEGORIES];

        // Total sizes of each variable type
        // a : port branches in the tree
        // b : port branches in the cotree
        // z : nonport branches in the tree
        // y : nonport branches in the cotree
        unsigned z_s;
        unsigned y_s;
        unsigned b_s;
        unsigned a_s;

        std::map<unsigned,unsigned> currents_map;
        std::map<unsigned,unsigned> voltages_map;

        // Map of branches, branch id is the key
        std::map<unsigned, branch> map_id_to_branch;

        // Utility
        void init_vector(unsigned *, int, unsigned) const;
        int get_i_position_in_tableau(const branch &);
        int get_v_position_in_tableau(const branch &, int);  
        void init_id_map(const std::vector<branch> &);


};

#endif
