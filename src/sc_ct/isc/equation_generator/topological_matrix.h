#ifndef TOPOLOGICAL_MATRIX_H
#define TOPOLOGICAL_MATRIX_H

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <Eigen/Dense>

#include "sc_ct/isc/equation_generator/labels.h"

// TO OPTIMIZE EIGEN
#define NDEBUG 

using namespace Eigen;

#define GAUSS_JORDAN_ZERO_TOL 0.0000000000001

class ISC::internal::topological_matrix {
    public:
        topological_matrix(const MatrixXd &,
                const labels &); 

        // void set_label(unsigned col, label label);
        void set_labels(labels _labels); 
        // label get_label(unsigned col) const;
        void swap_cols(unsigned, unsigned);
        std::vector<unsigned> to_row_echelon_form();
        const labels &get_labels() const;

        void print() const;

        static std::vector<unsigned> gauss_jordan_elimination(MatrixXd &, bool = false, int = -1, int = -1, int = -1, int = -1);

        static void swap_rows(unsigned, unsigned, MatrixXd &);

        const MatrixXd &get_matrix() const;

    protected: 
        // Topological matrix values
        MatrixXd m;
        // // Column _labels
        labels _labels;



        // Utility functions
        static bool is_valid_col(unsigned, MatrixXd);
        static bool is_valid_row(unsigned, MatrixXd);
        void remove_col(unsigned);
        void _swap_cols(unsigned, unsigned, MatrixXd &, bool = false);

};

#endif
