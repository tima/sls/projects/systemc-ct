#include "sc_ct/isc/equation_generator/branch.h"

ISC::id_type ISC::internal::branch::count = 1;

ISC::internal::branch::branch() : id(count++) {
    tree = false;
    port = false;
    element = resistance_el;

    law_expressed_as_voltage = true;
    on_state = true;
    parameter_value = 1;
}

void ISC::internal::branch::set_element(ISC::element el) {
    element = el;
}

void ISC::internal::branch::set_on_state(bool s) {
    on_state = s;
}

bool ISC::internal::branch::is_on() const {
    return on_state;
}

ISC::element ISC::internal::branch::get_element() const {
    return element;
}

state_space_type ISC::internal::branch::get_current_ss_type() {
    state_space_type t;
    switch (element) {
        case capacitor_el:
            t = derivative_var;
            break;
        case inductor_el:
            t = state_var;
            break;
        case resistance_el:
            t = nonport_var;
            break;
        case ind_v_source_el:
            t = response_source_var;
            break;
        case ind_c_source_el:
            t = source_var;
            break;
        case cv_source_el:
        case cc_source_el:
            t = nonport_var;
            break;
        case switch_el:
            // "When an ideal switch changes from ON to OFF,
            // the corresponding port ceases to be a voltage port 
            // and becomes a current port (Massarini & Reggiani, 2001)"
            t = on_state ? switch_var : zero_port_var;
            break;
        case diode_el:
            t = on_state ? switch_var : zero_port_var;
            break;
        case voltmeter_el:
            t = zero_port_var;
            break;
        case ammeter_el:
            t = output_var;
            break;
        default:
            throw std::domain_error("Invalid element type");
    }
    return t;
}

bool ISC::internal::branch::is_diode() const {
    return element == diode_el;
}

state_space_type ISC::internal::branch::get_voltage_ss_type() {
    state_space_type t;
    switch (element) {
        case capacitor_el:
            t = state_var;
            break;
        case inductor_el:
            t = derivative_var;
            break;
        case resistance_el:
            t = nonport_var;
            break;
        case ind_v_source_el:
            t = source_var;
            break;
        case ind_c_source_el:
            t = response_source_var;
            break;
        case cv_source_el:
        case cc_source_el:
            t = nonport_var;
            break;
        case switch_el:
            // "When an ideal switch changes from ON to OFF,
            // the corresponding port ceases to be a voltage port 
            // and becomes a current port (Massarini & Reggiani, 2001)"
            t = on_state ? zero_port_var : switch_var;
            break;
        case diode_el:
            t = on_state ? zero_port_var : switch_var;
            break;
        case voltmeter_el:
            t = output_var;
            break;
        case ammeter_el:
            t = zero_port_var;
            break;
        default:
            throw std::domain_error("Invalid element type");
    }
    return t;
}

void ISC::internal::branch::set_tree(bool v) {
    tree = v;
}

void ISC::internal::branch::set_port(bool p) {
    port = p;
}

bool ISC::internal::branch::is_tree() const {
    return tree; 
}

bool ISC::internal::branch::is_port() const {
    return port;
}

unsigned ISC::internal::branch::get_id() const {
    return id;
}

std::vector<branch_law_term> ISC::internal::branch::get_law() const {
    return law;
}

// Add a term to the branch law expressed as either:
// i = kv1*v1 + kv2*v2 + ... + ki1*i1 + ki2*i2 + ... kin*in  
// or
// v = kv1*v1 + kv2*v2 + ... + ki1*i1 + ki2*i2 + ... kin*in  
// as signaled by law_expressed_as_voltage()
void ISC::internal::branch::add_law_term(unsigned branch_id, 
    bool is_current, double coef) {
    branch_law_term term(branch_id, is_current, coef);
    law.push_back(term);
}

bool ISC::internal::branch::is_law_expressed_as_voltage() {
    return law_expressed_as_voltage;
}

void ISC::internal::branch::set_law_expressed_as_voltage(bool val) {
    law_expressed_as_voltage = val;
}

// Removes all terms from the law
void ISC::internal::branch::remove_law() {
    law.clear();
}

void ISC::internal::branch::print_law() const {
    std::cout << (law_expressed_as_voltage ? "v_" : "i_") << get_id() << " = "; 
    for (auto el: get_law()) {
        std::cout << el.get_coefficient()
            << "*" << (el.is_current() ? "i_":"v_")
            << el.get_branch_id() << " ";
    }
    std::cout << std::endl;
}

ISC::internal::branch_type ISC::internal::branch::get_type() const {
    if (is_tree()) {
        // Port elements in the tree
        if (is_port()) {
            return a_port_in_tree;
        }
        // Non-port elements in the tree
        else {
            return z_nonport_in_tree;
        }
    }
    // Co-tree elements
    else { 
        // Port elements in the co-tree
        if (is_port()) {
            return b_port_in_cotree;
        }
        // Non-port elements in the co-tree
        else {
            return y_nonport_in_cotree;
        }
    }
}


std::ostream& ISC::internal::operator<<(std::ostream&os, const ISC::internal::branch&b) {
    std::string type_names[] = {
        "a_port_in_tree", 
        "b_port_in_cotree",
        "z_nonport_in_tree",
        "y_nonport_in_cotree"
    };

    os << "branch " << b.get_id() << " of topological type " 
        << type_names[b.get_type()] << " is on: " << b.is_on();
    return os;
}

bool ISC::internal::branch::set_parameter_value(double val) {
    ISC::element element = get_element();
    if (element == resistance_el || 
        element == capacitor_el ||
        element == inductor_el) {
            if (val <= 0) {
                return false;
            }
    }
    parameter_value = val;
    return true;
}

double ISC::internal::branch::get_parameter_value() const {
    return parameter_value;
}
