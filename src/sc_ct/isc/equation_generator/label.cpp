#include "sc_ct/isc/equation_generator/label.h"

label::label(unsigned id, col_type _col_type, state_space_type ss_type) {
    set_branch_id(id);
    set_col_type(_col_type);
    set_ss_type(ss_type);
}

void label::set_branch_id(unsigned id) {
    branch_id = id;
}

unsigned label::get_branch_id() const {
    return branch_id;
}

void label::set_col_type(col_type t) { 
    _col_type = t;
}

col_type label::get_col_type() const {
    return _col_type;
}

void label::set_ss_type(state_space_type type) {
    ss_type = type;
}

state_space_type label::get_ss_type() const {
    return ss_type;
}

std::ostream& operator<<(std::ostream &os, const label &l) {
    std::string state_space_type_names[] = {
        "nonport_var",
        "response_source_var",  
        "switch_var", 
        "output_var",
        "derivative_var", 
        "state_var",
        "zero_port_var", 
        "source_var"
    };

    os << (l.get_col_type() == current_t ? "i_" : "v_") << l.get_branch_id()
        << " " << state_space_type_names[l.get_ss_type()];

    return os;
}

std::vector<state_space_type> label::get_ss_types() {
    std::vector<state_space_type> types_list;
    // Valid as far as the enum values are not manually defined
    // and that source_var is the last value in the enumeration
    for (int i = 0; i <= source_var; i++) {
        types_list.push_back(static_cast<state_space_type>(i));
    }
    return types_list;
} 

bool label::is_current() const {
    return _col_type == current_t; 
}

bool label::is_voltage() const {
    return _col_type == voltage_t;;
}
