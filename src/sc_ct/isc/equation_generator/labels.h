#ifndef LABELS_H
#define LABELS_H

#include <vector>
#include "sc_ct/isc/equation_generator/label.h"

class ISC::internal::labels {
    public: 
        labels(const std::vector<label> &);

        void set_labels(const std::vector<label> &);
        const std::vector<label> &get_labels() const;

        label get_label(const unsigned &) const; 
        void set_label(const unsigned &, label);

        unsigned get_count_by_ss_type(state_space_type);
        void swap_labels(unsigned, unsigned);

        std::vector<unsigned> get_columns_by_type(state_space_type t) const;

        unsigned size() const;
        void erase(unsigned);

        std::vector<label> get_output_labels() const;

    private:
        // Column labels
        std::vector<label> _labels;

        bool is_valid_col(unsigned) const;

};

#endif
