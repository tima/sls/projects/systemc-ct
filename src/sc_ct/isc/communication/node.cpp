#include "sc_ct/isc/communication/node.h"


size_t ISC::node::count = 0;

ISC::node::node(sc_core::sc_module_name name) : 
    sc_core::sc_prim_channel(name) 
{
    id = ++count;
}

void ISC::node::update() {
    return;
}

const char* ISC::node::kind() const {
    return  ISC::node_kind.c_str();
}

ISC::id_type ISC::node::get_id() const {
    return id;
}
