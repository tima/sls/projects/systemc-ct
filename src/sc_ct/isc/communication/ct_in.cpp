#include "sc_ct/isc/communication/ct_in.h"

ISC::ct_in::ct_in(sc_core::sc_module_name name) 
    : sc_core::sc_port<ISC::ct_in_if>(name) {

}

void ISC::ct_in::before_end_of_elaboration() {
    ISC::module *parent_prim;
    sc_core::sc_object *parent_obj;

    parent_obj = this->get_parent_object();
    parent_prim = dynamic_cast<ISC::module *>(parent_obj);

    if (parent_prim != NULL) {
        ISC::ct_in_if *in_if = this->operator->();
        dynamic_cast<ISC::signal *>(in_if)->add_reader_id(parent_prim->get_id());
    }
}
