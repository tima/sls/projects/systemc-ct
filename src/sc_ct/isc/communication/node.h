#ifndef ISC_NODE_H
#define ISC_NODE_H

#include <systemc>
#include "sc_ct/isc/common/common.h"

#include "sc_ct/isc/communication/terminal_if.h"

class ISC::node: public sc_core::sc_prim_channel, public ISC::terminal_if {
    
    public: 
        node(sc_core::sc_module_name = sc_core::sc_gen_unique_name(node_kind.c_str()) );
        void update();

        virtual const char* kind() const;

        ISC::id_type get_id() const;

    private:
        ISC::id_type id;

        // Class static variable to count the number of 
        // sf nodes that have been instantiated
        static size_t count;

};

#endif
