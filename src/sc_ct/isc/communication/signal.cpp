#include "sc_ct/isc/communication/signal.h"

ISC::signal::signal(sc_core::sc_module_name name)
    : sc_core::sc_prim_channel(name) {
    value = 0.0; 
    has_writer = false;
    writer_id = 0;
}

void ISC::signal::write(double val) {
    value = val;
}

double ISC::signal::read() {
    return value;
}

void ISC::signal::add_reader_id(ISC::id_type id) {
    reader_ids.push_back(id);
}

void ISC::signal::set_writer_id(ISC::id_type id) {
    has_writer = true;
    writer_id = id;
}
