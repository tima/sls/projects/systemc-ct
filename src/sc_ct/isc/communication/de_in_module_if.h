#ifndef DE_IN_MODULE_IF_H
#define DE_IN_MODULE_IF_H

#include <systemc>
#include <list>
#include <map>

#include "sc_ct/isc/common/common.h"

 // List of DE input ports to which the primitive is sensitive
typedef sc_core::sc_port<sc_core::sc_signal_in_if<bool>, 1> sc_in_bool; 
typedef sc_core::sc_port<sc_core::sc_signal_in_if<double>, 1> sc_in_double; 


// Interface for modules containing DE inputs that can change 
// structure of the signal flow graph
class ISC::internal::de_in_module_if : virtual public  ISC::module {
    public:
        de_in_module_if(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("de_in_module_if")) :
            ISC::module(name), de_bool_ports(), de_double_ports()
        {
        }

        virtual const std::list<sc_in_bool *> &get_de_boolean_ports() {
            return de_bool_ports;
        }

        virtual const std::list<sc_in_double *> &get_de_double_ports() {
            return de_double_ports;
        }

        // This should be protected but allowed for sf_cluster (friend class)
        virtual bool set_de_input(sc_in_bool *port, bool val) {
            if (boolean_input_states[port] == val) {
                
                return false;
            }

            boolean_input_states[port] = val;
            return true;
        }

        virtual bool set_de_input(sc_in_double *port, double val) {
            if (double_input_states[port] == val) {
                return false;
            }
            
            double_input_states[port] = val;
            return true;
        }

    protected:
        virtual void register_de_port(sc_in_bool &port){
            de_bool_ports.push_back(&port);
        }

        virtual void register_de_port(sc_in_double &port){
            de_double_ports.push_back(&port);
        }
        friend class ISC::de_in<bool>;
        friend class ISC::de_in<double>;

        std::list<sc_in_bool *> de_bool_ports;
        std::list<sc_in_double *> de_double_ports;
        std::map<sc_in_bool *, bool> boolean_input_states;
        std::map<sc_in_double *, double> double_input_states;

};

#endif
