#ifndef ISC_CT_OUT_IF_H
#define ISC_CT_OUT_IF_H

#include <systemc>
#include "sc_ct/isc/common/common.h"

class ISC::ct_out_if: public virtual sc_core::sc_interface {
    public:
        virtual double read() = 0;
    protected:
        void set_writer_id(ISC::id_type id);
};

#endif
