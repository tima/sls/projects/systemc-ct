#ifndef ISC_SIGNAL_H
#define ISC_SIGNAL_H

#include <list>
#include <systemc>
#include "sc_ct/isc/common/common.h"
#include "sc_ct/isc/communication/ct_out_if.h"
#include "sc_ct/isc/communication/ct_in_if.h"

class ISC::signal: public virtual sc_core::sc_prim_channel, public virtual ISC::ct_out_if, public virtual ISC::ct_in_if {
    public: 
        signal(sc_core::sc_module_name name = sc_core::sc_gen_unique_name(ISC::signal_kind.c_str()) );

        virtual void write(double);

        virtual double read();

        void update() {
            return;
        }

        virtual const char* kind() const {
            return  ISC::signal_kind.c_str();
        }

        const std::list<ISC::id_type> &get_reader_ids() const {
            return reader_ids;
        }

        ISC::id_type get_writer_id(){
            return writer_id;
        }

    protected:
        void add_reader_id(ISC::id_type);

        void set_writer_id(ISC::id_type);

        friend class ISC::ct_out;
        friend class ISC::ct_in;



    private:
        double value;
        ISC::id_type writer_id;
        bool has_writer;
        std::list<ISC::id_type> reader_ids;  

};


#endif
