#include "sc_ct/isc/communication/ct_out.h"

ISC::ct_out::ct_out(sc_core::sc_module_name name) 
    : sc_core::sc_port<ISC::ct_out_if>(name) {

}

void ISC::ct_out::before_end_of_elaboration() {
    ISC::module *parent_prim;
    sc_core::sc_object *parent_obj;

    parent_obj = this->get_parent_object();
    parent_prim = dynamic_cast<ISC::module *>(parent_obj);

    if (parent_prim != NULL) {
        ISC::ct_out_if *out_if = this->operator->();
        dynamic_cast<ISC::signal *>(out_if)->set_writer_id(parent_prim->get_id());
    }
}
