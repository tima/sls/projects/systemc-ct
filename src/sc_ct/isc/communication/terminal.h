#ifndef ISC_TERMINAL_H
#define ISC_TERMINAL_H

#include <systemc>
#include "sc_ct/isc/common/common.h"
#include "sc_ct/isc/communication/terminal_if.h"

#include "sc_ct/isc/components/module.h"

// Electrical terminal, for the electrical elements:
// resistances, inductors, capactitors, etc.
class ISC::terminal: virtual public sc_core::sc_port<ISC::terminal_if> {
    public:
        terminal(sc_core::sc_module_name = sc_core::sc_gen_unique_name(terminal_kind.c_str()), ISC::polarity = positive);
        
        virtual const char* kind() const;

        virtual void end_of_elaboration();

    private:
        ISC::polarity polarity;

};

#endif
