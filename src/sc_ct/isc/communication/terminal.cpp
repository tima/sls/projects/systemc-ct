#include "sc_ct/isc/communication/terminal.h"


ISC::terminal::terminal(sc_core::sc_module_name name, ISC::polarity polarity) 
    : sc_core::sc_port<ISC::terminal_if>(name), polarity(polarity) {
}

const char* ISC::terminal::kind() const {
    return ISC::terminal_kind.c_str();
}

void ISC::terminal::end_of_elaboration() {
    ISC::module *parent_prim;
    ISC::node *node;
    ISC::terminal_if *terminal_if;

    sc_core::sc_object *parent_obj;

    parent_obj = this->get_parent_object();
    parent_prim = dynamic_cast<ISC::module *>(parent_obj);

    terminal_if = this->operator->();
    node = dynamic_cast<ISC::node *>(terminal_if);

    if (node == NULL) {
        throw std::domain_error("Could not get node from interface");
    }

    // Register node in parent module
    // so that given a module one can determine 
    // to which nodes it belongs
    if (parent_prim != NULL) {
        parent_prim->connect_node(*node, polarity);
    }
}
