#ifndef DE_IN_H
#define DE_IN_H

#include <systemc>
#include "sc_ct/isc/common/common.h"

#include "sc_ct/isc/communication/de_in_module_if.h"

template<class T>
class ISC::de_in : public sc_core::sc_in<T> {
    public:

        de_in(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("ISC::de_in")) : sc_core::sc_in<T>(name) {

        }
        
        virtual const char* kind() const {
            return ISC::de_in_kind.c_str();
        }

        virtual void bind(sc_core::sc_port<sc_core::sc_signal_in_if<T>, 1>&port){
            sc_core::sc_in<T>::bind(port);

            register_de_port(port);
        }

        void operator() (sc_core::sc_port<sc_core::sc_signal_in_if<T>, 1>&port){
            bind(port);
        }


    private:
        // Registers name of DE port in parent primitive
        void register_de_port(sc_core::sc_port<sc_core::sc_signal_in_if<T>, 1>&port){
            ISC::internal::de_in_module_if *parent_prim;
            sc_core::sc_object *parent_obj;

            parent_obj = this->get_parent_object();
            parent_prim = dynamic_cast< ISC::internal::de_in_module_if *>(parent_obj);

            if (parent_prim != NULL) {
                parent_prim->register_de_port(port);
            }
        }
};

#endif
