#ifndef ISC_CT_IN_H
#define ISC_CT_IN_H

#include <systemc>
#include "sc_ct/isc/common/common.h"
#include "sc_ct/isc/communication/ct_in_if.h"
#include "sc_ct/isc/communication/signal.h"
#include "sc_ct/isc/components/module.h"

class ISC::ct_in: public sc_core::sc_port<ISC::ct_in_if> {
    public:
        ct_in(sc_core::sc_module_name name = 
            sc_core::sc_gen_unique_name(ISC::ct_in_kind.c_str()));
        
        virtual const char* kind() const {
            return ISC::ct_in_kind.c_str();
        }

    protected:
        virtual void before_end_of_elaboration();

};

#endif
