#ifndef ISC_TERMINAL_IF_H
#define ISC_TERMINAL_IF_H

#include <systemc>
#include "sc_ct/isc/common/common.h"

// Interface for the electrical terminals, see terminal.h
class ISC::terminal_if: public virtual sc_core::sc_interface {
    public:

    protected:

};

#endif
