#include "sc_ct/core/sync_conf_utility.h"

bool get_sync_conf(int argc, char* argv[],    
    bool &use_adaptive, bool &avoid_rollback, double &timestep) {
    
    if (argc != 4) {
        return false;
    }
    else {
        use_adaptive = std::string(argv[1]) == std::string("y");
        avoid_rollback = std::string(argv[2]) == std::string("y");
        timestep = std::stof(argv[3]); 
    }

    return true;
}

void print_sync_stats(bool use_adaptive, bool avoid_rollback,
    double timestep,
    double simulated_time, double wallclock_time) 
{ 
    std::cout << "SYNC. STRATEGY: " << (use_adaptive ? "ADAPTIVE" : "FIXED STEP") << std::endl;

    if(!use_adaptive) {
        std::cout << "TIMESTEP: " << timestep << " seconds" << std::endl;
    }

    std::cout << "AVOID ROLLBACK (USE NEXT EVENT'S TIME): " << (avoid_rollback ? "YES" : "NO") << std::endl;

    std::cout << "SIMULATED TIME: " << simulated_time << " seconds" << std::endl;

    std::cout << "WALL-CLOCK SIMULATION TIME: " << wallclock_time << " seconds" << std::endl << std::endl;
}
