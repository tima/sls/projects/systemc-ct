#ifndef SYNC_CONF_UTILITY_H
#define SYNC_CONF_UTILITY_H

#include<string>
#include<iostream>

// Takes in the typical argc and argv main parameters 
// and sets the rest of parameters to use
// an adaptive step, to avoid rollbacks, and the 
// timestep, as specified by the user. 
// Returns true if (argc, argv) has a proper formatting,
// (the sync. parameters have been correctly set),  
// and false in any other case (the sync. parameters are not set).  
bool get_sync_conf(int argc, char* argv[],    
    bool &use_adaptive, bool &avoid_rollback, double &timestep);

void print_sync_stats(bool use_adaptive, bool avoid_rollback,
    double timestep,
    double simulated_time, double wallclock_time);
#endif