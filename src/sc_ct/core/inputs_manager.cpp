#include "sc_ct/core/inputs_manager.h"

inputs_manager::inputs_manager(){
    mode = current_value;
}

void inputs_manager::use_checkpoints(bool use_it) {
    if(use_it) {
        this->set_mode(checkpoint);
    }
    else{
        this->set_mode(current_value);
    }
}

bool inputs_manager::operator[](const sc_core::sc_in<bool> &port) {
    if (mode==current_value) {
        return this->get_current_value_bool(port.name());
    }
    
    if(mode==checkpoint) {
        return this->get_checkpoint_value_bool(port.name());
    }

    throw std::invalid_argument("port");
}

double inputs_manager::operator[](const sc_core::sc_in<double> &port) {
    if (mode==current_value) {
        return this->get_current_value_double(port.name());
    }
    
    if(mode==checkpoint) {
        return this->get_checkpoint_value_double(port.name());
    }

    throw std::invalid_argument("port");
}


bool inputs_manager::operator[](const sc_core::sc_port<sc_core::sc_signal_in_if<bool> > &port){
    const sc_core::sc_in<bool> *port_ptr = static_cast<const sc_core::sc_in<bool> *>(&port);
    return this->operator[](*port_ptr);
}

double inputs_manager::operator[](const sc_core::sc_port<sc_core::sc_signal_in_if<double> > &port){
    const sc_core::sc_in<double> *port_ptr = static_cast<const sc_core::sc_in<double> *>(&port);
    return this->operator[](*port_ptr);
}



void inputs_manager::set_mode(modes_of_operation new_mode) {
    mode = new_mode;
}

void inputs_manager::register_input_ports(std::vector<sc_core::sc_object* > child_objects) {
        // Iterate over child objects to store input ports
        for (sc_core::sc_object * object: child_objects) { 
            if (is_input_port(object) && !store_port(object)) {
                std::cout << "*** Failed to store input port in synchronization layer";
                sc_core::sc_stop();
            }
        }
}

void inputs_manager::create_checkpoint() {
    // std::cout << "inputs_manager::create_checkpoint called" << std::endl;
    for (auto &el : boolean_inputs) {
        // el is of type 
        // std::pair<std::string, std::pair< sc_core::sc_in<bool>*, bool>>
        // The first .second returns an object of type 
        // std::pair< sc_core::sc_in<bool>*, bool>
        // The second one returns an object of type bool  
        el.second.second = el.second.first->read();
    }

    for (auto &el : double_inputs) {
        // See comment for boolean case above
        el.second.second = el.second.first->read();
    }
}

bool inputs_manager::have_inputs_changed() const {
    return have_boolean_inputs_changed() || have_double_inputs_changed();
}

bool inputs_manager::have_boolean_inputs_changed() const {
    for (auto el : boolean_inputs) {
        if (el.second.second != el.second.first->read()) {              
            return true;
        }
    }
    return false;
}

bool inputs_manager::have_double_inputs_changed() const {
    for (auto el : double_inputs) {
        if (el.second.second != el.second.first->read()) {
            return true;
        }
    }
    return false;
}

const sc_core::sc_event_or_list & inputs_manager::get_sensitivity_list() {
    static bool already_calculated = false;
    if (!already_calculated) {
        for (auto el : boolean_inputs) {
            sensitivity_list |= el.second.first->default_event();
        }
        for (auto el : double_inputs) {
            sensitivity_list |= el.second.first->default_event();
        }
    }
    return sensitivity_list;
}




//////////////////////////////////////////////////////////////////
// UTILITY FUNCTIONS 
//////////////////////////////////////////////////////////////////

bool inputs_manager::is_input_port(sc_core::sc_object *object) {
    return std::string(object->kind()) == "sc_in";
}

bool inputs_manager::store_port(sc_core::sc_object * port) {
    return store_boolean_port(port) || store_double_port(port);
}


bool inputs_manager::store_boolean_port(sc_core::sc_object * port) {
    sc_core::sc_in<bool> *port_ptr;
    if((port_ptr = dynamic_cast<sc_core::sc_in<bool>* >(port))) {
        auto port_pointer_and_backup = std::make_pair(
                port_ptr,
                port_ptr->read() 
        );

        auto element = std::make_pair(
            // Key
            port_ptr->name(), 
            // Value: pointer to the port and checkpoint value
            port_pointer_and_backup 
        );
        
        boolean_inputs.insert(element);
        return true;
    }
    return false;
}

bool inputs_manager::store_double_port(sc_core::sc_object * port) {
    sc_core::sc_in<double> *port_ptr;
    if((port_ptr = dynamic_cast<sc_core::sc_in<double>* >(port))) {
        auto port_pointer_and_backup = std::make_pair(
                port_ptr,
                port_ptr->read() 
        );

        auto element = std::make_pair(
            // Key
            port_ptr->name(), 
            // Value: pointer to the port and checkpoint value
            port_pointer_and_backup 
        );
        
        double_inputs.insert(element);
        return true;
    }
    return false;
}

bool inputs_manager::get_current_value_bool(std::string port_name) {
    return boolean_inputs[port_name].first->read();
}


double inputs_manager::get_current_value_double(std::string port_name) {
    return double_inputs[port_name].first->read();

}

bool inputs_manager::get_checkpoint_value_bool(std::string port_name) {
    return boolean_inputs[port_name].second;
}

double inputs_manager::get_checkpoint_value_double(std::string port_name) {
    return double_inputs[port_name].second;
} 

