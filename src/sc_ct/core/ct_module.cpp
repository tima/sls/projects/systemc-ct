#include "sc_ct/core/ct_module.h"

sct_core::ct_module::ct_module(bool use_adaptive_sync_step, bool use_time_to_next_event) : 
    use_adaptive_sync_step(use_adaptive_sync_step),
    use_time_to_next_event(use_time_to_next_event)    
{
    
}

void sct_core::ct_module::end_of_elaboration(){
    // Invoke method to set attributes as defined by the user
    set_sync_parameters();

    // Register input ports in inputs manager
    sct_kernel::ode_system::inputs.register_input_ports(sc_core::sc_module::get_child_objects());

    // Create synchronization layer object
    this->sync_layer = new sct_kernel::synchronization_layer(this, max_step, 
        use_adaptive_sync_step, use_time_to_next_event);

    sync_layer->store_input_event_sensitivity_list(
        sct_kernel::ode_system::inputs.get_sensitivity_list()
    );


    // Set initial conditions (as defined by the user)
    // and create a checkpoint on them
    set_initial_conditions();
    sct_kernel::ode_system::backup_state();
}

// To be implemented by the user, 
// as in TDF modules of SystemC AMS
// e.g. call set_max_timestep
// The default behavior is to set 
// a default maximum timestep
void sct_core::ct_module::set_sync_parameters() {
    // Set default maximum synchronization timestep
    set_max_timestep(DEFAULT_DELTA_T);
}

// Set the maximum allowed synchronization timestep
void sct_core::ct_module::set_max_timestep(double max){
    if(max > MIN_DELTA_T){
        max_step = max;
    }
    else{
        max_step = MIN_DELTA_T;
        std::cout << "Couldn't set DELTA T below " << max_step << " s. Set to " << max_step << std::endl; 
    }
}

void sct_core::ct_module::set_initial_conditions() {
    for (auto&& x_val : sct_kernel::ode_system::x) {
        x_val = 0;
    }
}

// 
void sct_core::ct_module::end_of_simulation() {
    // Clear screen in UNIX systems
    std::cout << "\x1B[2J\x1B[H"; 
    std::cout << "********************************************** " << std::endl;
    std::cout << "* STATISTICS                                 *" << std::endl;
    std::cout << "********************************************** " << std::endl;
    std::cout << "NUMBER OF BACKTRACKS : " << sync_layer->get_backtrack_count() << std::endl;
    std::cout << "TIME SPENT IN HANDLE_REACTIVATION : " << sync_layer->get_handle_reactivation_sim_time() << std::endl;
    std::cout << "TIME SPENT IN CALCULATE_SOLUTIONS : " << sync_layer->get_calculate_solutions_sim_time() << std::endl;
    
    std::cout << "*** T AUX : " << sync_layer->get_t_aux() << std::endl;


    create_trace_file(name());
}



void sct_core::ct_module::use_adaptive_sync(bool val){
    use_adaptive_sync_step = val;
}

void sct_core::ct_module::avoid_rollbacks(bool val){
    use_time_to_next_event = val;
}
