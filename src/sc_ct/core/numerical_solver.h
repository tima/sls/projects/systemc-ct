#ifndef NUMERICAL_SOLVER_H
#define NUMERICAL_SOLVER_H

#include <utility>
#include <algorithm>
#include <array>
#include <time.h>
#include <iomanip>

///////////////////////////////////////////////////////////////////////
// SOLVER LIBRARY
//
// Boost macro definition to allow deprecated headers
#define BOOST_ALLOW_DEPRECATED_HEADERS   1
//
#include <boost/numeric/odeint/integrate/integrate_adaptive.hpp>
#include <boost/numeric/odeint/stepper/runge_kutta_dopri5.hpp>
#include <boost/numeric/odeint/stepper/generation.hpp>
#include <boost/numeric/odeint/iterator/adaptive_time_iterator.hpp>
#include <boost/numeric/odeint/iterator/adaptive_iterator.hpp>
#include <boost/numeric/odeint/integrate/max_step_checker.hpp>
#include <boost/numeric/odeint/util/detail/less_with_sign.hpp>
#include <boost/numeric/odeint/stepper/controlled_step_result.hpp>
namespace odeint = boost::numeric::odeint;
///////////////////////////////////////////////////////////////////////


// Integration algorithm is adaptive, 
// these values refer just to the first step
#define MAX_STEP 0.01 // seconds
#define MIN_STEP 0.0001 // seconds
 

namespace sct_kernel {

    class numerical_solver {
        public: 
            numerical_solver(){};

            // Integration method
            template<class DiffEquations, class State, class EventDetector, class Tracer>
            double integrate( DiffEquations diff_equations, State &state, 
                double t_start, double t_end, EventDetector event_detector, Tracer tracer)
            {
                double int_step = (t_end-t_start) / 10.0;
                int_step = int_step > MAX_STEP ? MAX_STEP : int_step; 
                int_step = int_step < MIN_STEP ? MIN_STEP : int_step; 
                static double dt = int_step;


                typedef odeint::runge_kutta_dopri5< State, double, State, double, odeint::vector_space_algebra> error_stepper;

                typedef odeint::default_error_checker<
                    typename error_stepper::value_type ,
                    typename error_stepper::algebra_type ,  
                    typename error_stepper::operations_type>  error_checker;

                typedef odeint::controlled_runge_kutta< error_stepper, error_checker > stepper_type;

                // a_x and a-dxdt have their default values of 1.
                // Error compared with  eps_abs + eps_rel * ( a_x
                //* |x| + a_dxdt * dt * |dxdt|)
                double abs_err = 1.0e-6, rel_err = 1.0e-6,  a_x = 1.0 , a_dxdt = 1.0;
                
                auto stepper = stepper_type(error_checker( abs_err , rel_err, a_x, a_dxdt) );

                // to throw a runtime_error if step size adjustment fails
                odeint::failed_step_checker fail_checker;  

                while (odeint::detail::less_with_sign(t_start , t_end , dt)){
                    tracer(t_start, state);
                    if (odeint::detail::less_with_sign(t_end, t_start + dt, dt)) {
                        dt = t_end - t_start;
                    }

                    odeint::controlled_step_result res;
                    do {
                        res = stepper.try_step(diff_equations, state , t_start , dt);

                        // check number of failed steps
                        fail_checker(); 
                    }
                    while (res == odeint::fail);
                    // if we reach here, the step was successful -> reset fail checker
                    fail_checker.reset();  
                    

                    // Avoid integrating until the end of the interval
                    // if one state event is found
                    if (event_detector(state , t_start)) {
                        tracer(t_start, state);
                        break;
                    }
                }

                // std::cout << "Ended " << " with "<< dt << std::endl;

                if (dt < MIN_STEP) {
                    dt = MIN_STEP;
                }


                return t_start;
            }  

            // Root location method
            template<class DiffEquations, class State, class StateEventDetector, class Tracer>
            bool locate_state_event(
                DiffEquations diff_equations, const State &x0, State &x_at_event, const State &x_end,
                const double t_start, double &t_end,
                StateEventDetector is_event, 
                Tracer tracer) 
            {
                const double precision = 1E-7;
                
                // integrates an ODE until some threshold is crossed
                // returns time and state at the point of the threshold crossing
                // if no threshold crossing is found, some time > t_end is returned
   
                auto stepper = odeint::make_dense_output( 1.0e-3, 1.0e-3, odeint::runge_kutta_dopri5< State, double, State, double, odeint::vector_space_algebra>());


                validate_interval(stepper, diff_equations, is_event, tracer, x0, t_start, t_end);
                
                // the dense out stepper now covers the interval where the condition changes
                // improve the solution by bisection
                double t0 = t_start;
                double t1 = t_end;

                double t_m = 0.5 * (t0 + t1);
                State x_m(x0.size());
                State x1 = x_end;

                // TODO: Make sure to return exactly the point where the state event occurs
                // t0 - tm is equal to t1 - tm because tm = 0.5(t0+t1)
                while (std::abs(t0 - t_m) > precision / 2) { 
                    stepper.calc_state(t_m, x_m); // obtain the corresponding state

                    if (is_event(x_m, t_m)){
                        t1 = t_m;  // condition changer lies before midpoint
                        x1 = x_m;
                    }
                    else{
                        tracer(t_m, x_m);
                        t0 = t_m;  // condition changer lies after midpoint
                    }

                    t_m = 0.5 * (t0 + t1);  // get the mid point time
                }

                t_m = t1;

                if (t_m == t_end){
                    x_m = x_end;
                }
                else{
                    // we find the interval of size eps
                    stepper.calc_state(t_m, x_m);
                }


                t_end = t_m;
                x_at_event = x_m;

                tracer(t_end, x_at_event);

                return true;
            }

        private:

           

            template<class Stepper, class DiffEquations, class State, class StateEventDetector, class Tracer> 
            bool validate_interval( Stepper &stepper,
                DiffEquations diff_equations,
                StateEventDetector is_event, Tracer tracer, State x0, 
                const double t_start, double t_end )
            {
                auto ode_range = odeint::make_adaptive_time_range(std::ref(stepper), 
                    diff_equations,
                    x0,
                    t_start,
                    t_end,
                    t_end-t_start/10
                );

                std::find_if(ode_range.first,
                ode_range.second,
                [&](const std::pair<State const&, double> &it){ 
                    State x = it.first; 
                    double t = it.second;
                    return is_event(x, t);
                }); 

                return true;
            }

    };

}

#endif
