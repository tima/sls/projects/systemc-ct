trace_1 = load("results.dat");

figure(1);
hold on;
grid on;

plot(trace_1(:,1), trace_1(:,4) , '-r', 'LineWidth', 2);
plot(trace_1(:,1), trace_1(:,5) , '-g', 'LineWidth', 2);
plot(trace_1(:,1), trace_1(:,6) , '-b', 'LineWidth', 2);

legend("x", "y", "z");

input("Press enter");