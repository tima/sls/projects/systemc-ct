#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <systemc>
#include "common.h"


class Controller : public sc_core::sc_module { 
    public:
        // System DE state
        sc_core::sc_out<double> state_out;
        
        // Inform the DE part of threshold being reached
        sc_core::sc_in<double> threshold_reached_in;

        Controller(sc_core::sc_module_name name){ 
            SC_HAS_PROCESS(Controller);
            
            SC_METHOD(update_state);
            sensitive << threshold_reached_in;

            state = inside;
            state_out.initialize(state);
        }

        void update_state(){
            // The logic of setting the state has been 
            // simplified by a proper choice of the 
            // value coming from the integrator module 
            // to signal a threshold crossing
            state = threshold_reached_in.read();
            state_out.write(state);
        }

    private:
        int state;

};
#endif