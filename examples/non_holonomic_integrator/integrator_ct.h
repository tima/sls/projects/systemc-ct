#ifndef INTEGRATOR_CT_H
#define INTEGRATOR_CT_H

#include <systemc>
#include "ct_module.h"
#include <cmath>

#include "common.h"

// OBSERVATION : SIMULATING THIS SYSTEM POSES 
// PROBLEMS TO THE INTEGRATION ALGORITHM.
// WE ARE NOT YET THERE, NOT VERY IMPORTANT 
// FOR OUR AIMS.
// EXAMPLE TAKEN FROM 
// https://ths.rwth-aachen.de/research/projects/hypro/non-holonomic-integrator/
// WHICH IS BASED ON  
// [1] J. Hespanha, A. Morse. Stabilization of nonholonomic integrators via logic-based switching. In Automatica, volume 35(3), pages 385–393, Elsevier, 1999.

// Order of the system of diff. equations
#define ORDER 3 
// Max. timestep to be taken for synchronization
#define MAX_STEP_INTEGRATOR 0.1 // seconds


class IntegratorCt : public sct_core::ct_module { 
    public:
        // System DE state
        sc_core::sc_in<double> state_in;
        
        // Inform the DE part of threshold being reached
        sc_core::sc_out<double> threshold_reached_out;

        sc_core::sc_out<double> x_out;
        sc_core::sc_out<double> y_out;
        sc_core::sc_out<double> z_out;

        IntegratorCt(sc_core::sc_module_name name){ 
        }

        void set_sync_parameters(){  
            set_max_timestep(MAX_STEP_INTEGRATOR);
        }

        void set_initial_conditions(){
        }

        void get_derivatives(bool use_input_checkpoints,
            const sct_core::ct_state &x , sct_core::ct_state &dxdt , double t){
            
            double u, v, _x, _y, _z;
            _x = x[0];
            _y = x[1];
            _z = x[2];

            // std::cout << "-- get_derivatives t " << t << std::endl;

            if (state_in.read() == inside) {
                u = 1.0;
                v = 1.0; 
            }
            else {
                double sum_squares_x_y = pow(_x, 2) + pow(_x, 2);  
                u = -_x + 2 * _y * _z / sum_squares_x_y;
                v = -_x + 2 * _x * _z / sum_squares_x_y; 
            }

            // std::cout << "-- get_derivatives t " << t << std::endl;

            dxdt[0] = u; 
            dxdt[1] = -v;
            dxdt[2] = (_x * v) - (_y * u);

            std::cout << "Wirting derivatives " << dxdt << " at " << t << std::endl;
        }

        // Threshold crossing condition.
        std::unordered_map<int, bool>  is_event(const sct_core::ct_state &x, double t = 0){
            std::unordered_map<int, bool> events;    
            events[inside_event_id] =  (state_in.read() == outside) && (pow(x[0],2) + pow(x[1],2) <= abs(x[2]));

            events[outside_event_id] =  (state_in.read() == inside) && (pow(x[0],2) + pow(x[1],2) > abs(x[2]));

            return events;
        }

        void generate_outputs(bool state_event_located, 
            std::unordered_map<int, bool> events
        ) {
            // First delta cycle, we save a back-up of the continuous state
            x_out.write(x[0]);
            y_out.write(x[1]);
            z_out.write(x[2]);

            // If it is an stop event
            if (events[inside_event_id]) {
                threshold_reached_out.write(inside);
            }

            // If it is an stop event
            if (events[outside_event_id]) {
                threshold_reached_out.write(outside);
            }
        }

    private:
        enum event_ids {inside_event_id, outside_event_id};

};

#endif