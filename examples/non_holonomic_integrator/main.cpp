#include<systemc>
#include<systemc-ct.h>
#include<iostream> 
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */


#include "controller.h"
#include "integrator_ct.h"


int sc_main(int argc, char* argv[]){
    clock_t t = clock();
    sc_core::sc_signal<double> state_sig, threshold_reached_sig, x_sig, y_sig, z_sig;
   

    Controller controller_de("controller_de");
    IntegratorCt integrator_ct("integrator_ct");

    controller_de.state_out.bind(state_sig);
    controller_de.threshold_reached_in.bind(threshold_reached_sig);

    integrator_ct.state_in.bind(state_sig);
    integrator_ct.threshold_reached_out.bind(threshold_reached_sig);
    integrator_ct.x_out.bind(x_sig);
    integrator_ct.y_out.bind(y_sig);
    integrator_ct.z_out.bind(z_sig);

    // Start simulation for a given time
    sc_core::sc_start(8, sc_core::SC_SEC);

    // Finish simulation
    sc_core::sc_stop();


    t = clock() - t;
    std::cout << "It took me" << t << "clicks (" << ((float)t)/CLOCKS_PER_SEC << "seconds).\n";

    return 0;
}