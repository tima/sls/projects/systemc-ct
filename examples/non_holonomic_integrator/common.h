#ifndef COMMON_H
#define COMMON_H

// Discrete state of integrator system.
// inside means x^2+y^2 <= |z|
// outside means x^2+y^2 > |z|
enum state {inside, outside};

#endif 