#include<iostream> 
#include<cstdlib>
#include<string>


// Function prototypes
void compile(std::string);
void execute(std::string, bool, bool, double);
void show_results(std::string);
std::string cd_command(std::string);
int select_example();
void clear_screen();
void main_loop();
bool request_use_adaptive();
bool request_avoid_rollback();
double request_timestep();


// Constants
#define VEHICLE_ID 1
#define BO_BALL_ID 2
#define CIRCUIT_ID 3




int main(int argc, char* argv[]) {

    main_loop();

    return 0;
}

void get_sync_conf(bool &use_adaptive, bool &avoid_rollback, double &timestep, int argc, char* argv[]) {
    if(argc != 4) {
        use_adaptive = true;
        avoid_rollback = false;
        timestep = 0;
    }
    else {
        use_adaptive = std::string(argv[1]) == std::string("y");
        avoid_rollback = std::string(argv[2]) == std::string("y");
        timestep = std::stof(argv[3]); 
    }

    if (use_adaptive) {
        std::cout << "USE ADAPTIVE" << std::endl;
    }
    if (avoid_rollback) {
        std::cout << "AVOID ROLLBACK" << std::endl;
    }
    std::cout << "TIMESTEP = " << timestep << std::endl;
}


void main_loop() {

    std::string rc_circuit_path = "../bang_bang/ct_de";
    std::string bouncing_ball_path = "../bouncing_ball/ct_de";
    std::string vehicle_path = "../vehicle_cruise_control/ct_de";
    std::string path;

    bool use_adaptive = true;
    bool avoid_rollback = false;
    double timestep = 0;

    int example = select_example();
    while (example != 0) {
        switch (example) {
            case VEHICLE_ID:
                path = vehicle_path;
                break;
            case BO_BALL_ID:
                path = bouncing_ball_path;
                break;
            case CIRCUIT_ID:
                path = rc_circuit_path;
                break;
            default:
                exit(1);
                break;
        }

        // Request sync. parameters
        use_adaptive = request_use_adaptive();
        if (!use_adaptive) {
            timestep = request_timestep();
        }
        avoid_rollback = request_avoid_rollback();  

        // Compile, execute and show results
        compile(path);
        execute(path, use_adaptive, avoid_rollback, timestep);
        show_results(path);

        // Request new selection
        example = select_example();
    }

    clear_screen();
}

int select_example() {
    int case_study = -1;

    while(case_study < 0 || case_study > 3){
        clear_screen();
        std::cout <<
        "********************************************************\n";
        std::cout << "* SELECT ONE CASE STUDY                                *\n";
        std::cout << "* (" + std::to_string(VEHICLE_ID) + ") Vehicle's Automatic Transmission Control         *\n";
        std::cout << "* (" + std::to_string(BO_BALL_ID) + ") Bouncing Ball                                    *\n";
        std::cout << "* (" + std::to_string(CIRCUIT_ID) + ") Switched RC Circuit                              *\n";
        std::cout << "* (0) TO EXIT                                          *\n";
        std::cout << "********************************************************\n";
        std::cout << ">> ";


        std::cin >> case_study;
    }

    return case_study;
}

bool request_use_adaptive() {
    std::string ans = "a";

    while (ans != std::string("y") && ans != std::string("Y") &&
           ans != std::string("n") && ans != std::string("N")) 
    {
        clear_screen();
        std::cout <<
        "********************************************************\n";
        std::cout << "* USE ADAPTIVE SYNCHRONIZATION ? (Y/n)                 *\n";
        std::cout << "********************************************************\n";
        std::cout << ">> ";
        std::cin >> ans;
    }

    return ans == std::string("y") || ans == std::string("Y");
}

bool request_avoid_rollback() {
    std::string ans = "a";

    while (ans != std::string("y") && ans != std::string("Y") &&
           ans != std::string("n") && ans != std::string("N"))
    {
        clear_screen();
        std::cout <<
        "********************************************************\n";
        std::cout << "* AVOID ROLLING BACK ? (Y/n)                           *\n";
        std::cout << "********************************************************\n";
        std::cout << ">> ";

        std::cin >> ans;    
    }
    
    return ans == std::string("y") || ans == std::string("Y");
}

double request_timestep() {
    double ans = 0;

    while (ans <= 0 || ans > 100) {
        clear_screen();
        std::cout <<
        "********************************************************\n";
        std::cout << "* FOR NON-ADAPTIVE SYNCHRONIZATION ENTER A TIMESTEP    *\n";
        std::cout << "* (0 < TIMESTEP < 100)                                 *\n";
        std::cout << "********************************************************\n";
        std::cout << ">> ";

        std::cin >> ans;
    }

    return ans;
}

void compile(std::string path) {
    std::string make_command = "make -j5 ";
    std::string command = cd_command(path) + " && " + make_command;
    
    system(command.c_str());
    clear_screen();
}

void execute(std::string path, bool use_adaptive, bool avoid_rollback, double timestep) {

    std::string command = cd_command(path) + " && ./main ";

    command += use_adaptive ? "y " : "n ";
    command += avoid_rollback ? "y " : "n ";
    command += std::to_string(timestep); 

    system(command.c_str());
}

void show_results(std::string path) {
    std::string command = cd_command(path) + "&& octave results.m";

    system(command.c_str());
}

std::string cd_command(std::string path){
    return "cd " + path;
}

void clear_screen() {
    /* ONLY FOR UNIX */ 
    std::cout << "\x1B[2J\x1B[H";
}
