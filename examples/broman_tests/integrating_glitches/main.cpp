#include<systemc>
#include<systemc-ct.h>
#include<iostream> 
#include<chrono>


#include "integrator_ct.h"
#include "generator_de.h"


int sc_main(int argc, char* argv[]){
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

    sc_core::sc_signal<double> der_a_sig,der_b_sig, int_a_sig,int_b_sig;

    GeneratorDe generator_de("generator_de");
    IntegratorCt integrator_ct("integrator_ct");

    generator_de.sig_a_out.bind(der_a_sig);
    generator_de.sig_b_out.bind(der_b_sig);

    integrator_ct.sig_a_in.bind(der_a_sig);
    integrator_ct.sig_b_in.bind(der_b_sig);
    integrator_ct.sig_a_out.bind(int_a_sig);
    integrator_ct.sig_b_out.bind(int_b_sig);
    

    // Start simulation for a given time
    sc_core::sc_start(7, sc_core::SC_SEC);

    // Finish simulation
    sc_core::sc_stop();


    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    auto sim_time = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    std::cout << "*** THE SIMULATION TOOK " <<  sim_time << " MICROSECONDS " << std::endl;

    return 0;
}