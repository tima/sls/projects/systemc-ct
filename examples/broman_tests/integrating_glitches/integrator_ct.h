#ifndef INTEGRATOR_CT_H
#define INTEGRATOR_CT_H

#include <systemc>
#include "ct_module.h"
#include <cmath>


// Slope when input signal is false/true
#define FALSE_SLOPE 0
#define TRUE_SLOPE 1

// Max. timestep to be taken for synchronization
#define MAX_STEP_INTEGRATOR 1 // seconds

// Order of the system
#define ORDER 2


class IntegratorCt : public sct_core::ct_module { 
    public:
        // Input DE signals
        sc_core::sc_in<double> sig_a_in;
        sc_core::sc_in<double> sig_b_in; 

        // Result fo the integration
        sc_core::sc_out<double> sig_a_out;
        sc_core::sc_out<double> sig_b_out;

        IntegratorCt(sc_core::sc_module_name name)
        {
        }

        void set_sync_parameters(){  
            set_max_timestep(MAX_STEP_INTEGRATOR);
        }

        void get_derivatives(bool use_input_checkpoints,
            const sct_core::ct_state &x , sct_core::ct_state &dxdt , double t
        ){
            // Set the mode of the inputs manager to either current_values or checkpoint
            ode_system::inputs.use_checkpoints(use_input_checkpoints);
            // Get the value of inputs 
            double sig_a = ode_system::inputs[sig_a_in];
            double sig_b = ode_system::inputs[sig_b_in];

            // Integrate the input signal
            dxdt[0] = sig_a;
            dxdt[1] = sig_b; 
        }


        void generate_outputs(bool state_event_located,
            std::unordered_map<int, bool> events) {
            sig_a_out.write(x[0]);
            sig_b_out.write(x[1]);
        }
};

#endif