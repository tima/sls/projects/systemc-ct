trace_1 = load("results.dat");

figure(1);
hold on;
grid on;
stairs(trace_1(:,1), trace_1(:,2) , '-g', 'LineWidth', 2);
% stairs(trace_1(:,1), trace_1(:,3) , '-b', 'LineWidth', 2);
% plot(trace_1(:,1), trace_1(:,4) , '-r', 'LineWidth', 2);
% plot(trace_1(:,1), trace_1(:,5) , '-ok', 'LineWidth', 2);

legend("Derivative A", "Derivative B", "Integral A", "Integral B");

input("Press enter");