#ifndef INTEGRATOR_CT_H
#define INTEGRATOR_CT_H

#include <systemc>
#include "ct_module.h"
#include <cmath>


// Slope when input signal is false/true
#define THRESHOLD_VALUE 1

#define CONSTANT_DERIVATIVE 1


// Max. timestep to be taken for synchronization
#define MAX_STEP_INTEGRATOR 1 // seconds

// Order of the system
#define ORDER 1


class IntegratorCt : public sct_core::ct_module { 
    public:
        // Input DE signals
        sc_core::sc_in<bool> reset_in; 

        // Result fo the integration
        sc_core::sc_out<double> sig_out;
        sc_core::sc_out<bool> sig_threshold_out;

        IntegratorCt(sc_core::sc_module_name name)
        {
            last_reset = false;
        }

        void set_sync_parameters(){  
            set_max_timestep(MAX_STEP_INTEGRATOR);
        }

        void get_derivatives(bool use_input_checkpoints,
            const sct_core::ct_state &x , sct_core::ct_state &dxdt , double t
        ){
            // Integrate the input signal
            dxdt[0] = CONSTANT_DERIVATIVE;
        }

        bool execute_updates() {
            if(reset_in.read() != last_reset ){
                x[0] = 0;
                last_reset = reset_in.read();
                return true;
            } 
            return false;
        }

        std::unordered_map<int, bool> is_event(const sct_core::ct_state &x, double t = 0) {
            std::unordered_map<int, bool> events;
            events[threshold_crossing] = x[0] >= THRESHOLD_VALUE;
            
            return events;
        }

        void generate_outputs(bool state_event_located,
            std::unordered_map<int, bool> events
        ){
            sig_out.write(x[0]);

            // The tracing system in SystemC does not support mulitple values 
            // at the same real time (with different deltas)
            if (events[threshold_crossing]) {
                // Transitions signal threshold crossing
                sig_threshold_out.write(!sig_threshold_out.read());
            }
        }

    private:
        bool last_reset;
        enum event_ids {threshold_crossing};
};

#endif