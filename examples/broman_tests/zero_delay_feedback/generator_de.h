#ifndef GENERATOR_DE_H
#define GENERATOR_DE_H

#include <systemc>


#define AMPLITUDE 1
#define GLITCH_TIME 1.5


class GeneratorDe : public sc_core::sc_module { 
    public:
        // Constant signal 
        sc_core::sc_out<double> sig_a_out;
        // Signal with a glitch
        sc_core::sc_out<double> sig_b_out;

        GeneratorDe(sc_core::sc_module_name name)
        { 
            SC_HAS_PROCESS(GeneratorDe);

            SC_THREAD(generate_sig);

            sig_a_out.initialize(AMPLITUDE);
            sig_b_out.initialize(AMPLITUDE);
        }

        // Signal generation thread
        void generate_sig(){
            wait(GLITCH_TIME, sc_core::SC_SEC);                
            sig_b_out.write(2*AMPLITUDE);
            wait(sc_core::SC_ZERO_TIME);
            sig_b_out.write(AMPLITUDE);
        }

};

#endif