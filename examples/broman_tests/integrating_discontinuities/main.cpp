#include<systemc>
#include<systemc-ct.h>
#include<iostream> 
#include<chrono>


#include "integrator_ct.h"
#include "generator_de.h"


int sc_main(int argc, char* argv[]){
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

    sc_core::sc_signal<double> der_sig, int_sig;

    GeneratorDe generator_de("generator_de");
    IntegratorCt integrator_ct("integrator_ct");

    generator_de.sig_out.bind(der_sig);
   

    integrator_ct.sig_in.bind(der_sig);
    integrator_ct.sig_out.bind(int_sig);
    

    // Start simulation for a given time
    sc_core::sc_start(7, sc_core::SC_SEC);

    // Finish simulation
    sc_core::sc_stop();


    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    auto sim_time = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    std::cout << "*** THE SIMULATION TOOK " <<  sim_time << " MICROSECONDS " << std::endl;

    return 0;
}