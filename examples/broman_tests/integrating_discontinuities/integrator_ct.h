#ifndef INTEGRATOR_CT_H
#define INTEGRATOR_CT_H

#include <systemc>
#include "ct_module.h"
#include <cmath>


// Slope when input signal is false/true
#define FALSE_SLOPE 0
#define TRUE_SLOPE 1

// Max. timestep to be taken for synchronization
#define MAXI_STEP 1 // seconds


class IntegratorCt : public sct_core::ct_module { 
    public:
        // Input DE signal that defines the slope 
        // of the continuous signal to integrate
        sc_core::sc_in<double> sig_in; 

        // Result fo the integration
        sc_core::sc_out<double> sig_out;

        IntegratorCt(sc_core::sc_module_name name)
        { 
            // should_stop_out.initialize(false);
        }

        void set_sync_parameters(){  
            set_max_timestep(MAXI_STEP);
        }

        void set_initial_conditions(){
            x.resize(1);
            x[0] = 0;
        }

        void get_derivatives(bool use_input_checkpoints,
            const sct_core::ct_state &x , sct_core::ct_state &dxdt , double t
        ){
             // Set the mode of the inputs manager to either current_values or checkpoint
            ode_system::inputs.use_checkpoints(use_input_checkpoints);
            // Get the value from the stop_in port
            double inp = ode_system::inputs[sig_in];
            
            // Integrate the input signal
            dxdt[0] = inp; 
        }


        void generate_outputs(bool state_event_located,
            std::unordered_map<int, bool> events
        ) {
            std::cout << "Gen out" << std::endl; 
            sig_out.write(x[0]);
         }
};

#endif