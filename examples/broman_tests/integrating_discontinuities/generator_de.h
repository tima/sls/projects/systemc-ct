#ifndef GENERATOR_DE_H
#define GENERATOR_DE_H

#include <systemc>


#define PERIOD 1.5
#define ON_AMPLITUDE 1
#define OFF_AMPLITUDE 0

class GeneratorDe : public sc_core::sc_module { 
    public:
        // Amplitude of the constant signal to integrate
        sc_core::sc_out<double> sig_out;

        GeneratorDe(sc_core::sc_module_name name)
        { 
            SC_HAS_PROCESS(GeneratorDe);

            SC_THREAD(generate_sig);

            on_state = false; 
        }

        // Signal generation thread
        void generate_sig(){
            while(true){
                sig_out.write(on_state ? ON_AMPLITUDE : OFF_AMPLITUDE);
                wait(PERIOD, sc_core::SC_SEC);
                on_state = !on_state;
            }
        }

    private:
        bool on_state;
};

#endif