#include "switch_driver_de.h"

// Constructor
SwitchDriverDE::SwitchDriverDE(sc_core::sc_module_name name){
    // Declare switching method
    SC_METHOD(up_ctrl_thread);
    dont_initialize();
    // Make it sensitive to the up_in and down_in default events (signal change)
    sensitive << up_in << down_in;
    
    // Initialize switch_out value: switch is closed at t = 0
    switch_out.initialize(true);

    // Initialize aux variable
    last_up_event = false;
}


void SwitchDriverDE::up_ctrl_thread(){
    // up event
    if (up_in.read() != last_up_event) {
        switch_out.write(false);
        last_up_event = up_in.read();
    }
    // down event
    else {
        switch_out.write(true);
    }
}
