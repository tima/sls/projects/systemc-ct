#include <systemc>
#include <systemc-ct.h>
#include <iostream> 
#include <time.h> 

#include "switch_driver_de.h"
#include "switched_rc_circuit.h"


// Pass as first parameter 'y' to use the adaptive sync. strategy,
// 'y' as second parameter to avoid rollbacks (use time of the next
// event),
// and a real number as the third parameter to set the timestep
// Ex: ./main y y 0.1 
int sc_main(int argc, char* argv[]){

    // To measure execution time
    clock_t t = clock();
    
    // Default syncronization values
    bool use_adaptive = true;
    bool avoid_rollback = false;
    double timestep = 0;

    // Get synchronization configuration from argv
    get_sync_conf(argc, argv, use_adaptive, avoid_rollback, timestep);

    // Declare signals
    //// Circuit's output voltage
    sc_core::sc_signal<double> v_sg;
    //// Switch signal, up threshold crosses signal, and 
    //// down threshold crossed signal
    sc_core::sc_signal<bool> switch_sg, up_sg, down_sg;
    
    // Declare modules 
    SwitchDriverDE switch_driver("driver");
    SwitchedRCCircuit circuit("circuit", use_adaptive, avoid_rollback, timestep);

    // Port binding 
    switch_driver.switch_out.bind(switch_sg);
    switch_driver.up_in.bind(up_sg);
    switch_driver.down_in.bind(down_sg);

    circuit.v_out.bind(v_sg);
    circuit.switch_in.bind(switch_sg);
    circuit.up_out.bind(up_sg);
    circuit.down_out.bind(down_sg);

    // Start simulation for a given time
    sc_core::sc_time simulated_time(2, sc_core::SC_SEC);
    sc_core::sc_start(simulated_time);

    // Finish simulation
    sc_core::sc_stop();
 
    // Print simulation time statistics and synchronization configuration 
    // information
    t = clock() - t;
    print_sync_stats(use_adaptive, avoid_rollback, timestep, simulated_time.to_seconds(), ((double)t)/CLOCKS_PER_SEC);

    return 0;
}