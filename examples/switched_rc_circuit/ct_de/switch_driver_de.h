#ifndef SWITCH_DRIVER_DE_H
#define SWITCH_DRIVER_DE_H

#include<systemc>

// Module that generates
// the switch control signal
SC_MODULE(SwitchDriverDE){
    // Inputs
    sc_core::sc_in<bool> up_in;
    sc_core::sc_in<bool> down_in;

    // Outputs
    sc_core::sc_out<bool> switch_out;
    
    // Constructor
    SC_CTOR(SwitchDriverDE);

    // Methods
    void up_ctrl_thread();
    
    private:
        bool last_up_event; 
};

#endif