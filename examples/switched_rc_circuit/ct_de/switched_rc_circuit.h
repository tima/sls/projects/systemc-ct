#ifndef SWITCHED_RC_CIRCUIT_H
#define SWITCHED_RC_CIRCUIT_H

#include <systemc>
#include "ct_module.h"

// Length of integration interval
#define DELTA_T_CIRCUIT 2 // seconds
#define SAMPLING_PERIOD 0.05 //seconds 
 
class SwitchedRCCircuit : public sct_core::ct_module{
    public:
        //////////////////////////////////////////////////////////////
        // PORTS 
        //////////////////////////////////////////////////////////////

        // Outputs 
        sc_core::sc_out<bool> up_out;
        sc_core::sc_out<bool> down_out;
        sc_core::sc_out<double> v_out;

        // Inputs
        sc_core::sc_in<bool> switch_in; 


        //////////////////////////////////////////////////////////////
        // Constructor
        //////////////////////////////////////////////////////////////
        SwitchedRCCircuit(sc_core::sc_module_name name,
            bool use_adaptive = true, bool avoid_rollback = true, 
            double sync_step = DELTA_T_CIRCUIT
            ) :
            // Call port constructors
            up_out("up_out"),
            down_out("down_out"),
            v_out("v_out"),
            switch_in("switch_in"),
            // Initialize synchronization parameters
            avoid_rollback(avoid_rollback),
            use_adaptive(use_adaptive), 
            sync_step(sync_step),
            next_sampling_time(0, sc_core::SC_SEC)
        { 
        }

        //////////////////////////////////////////////////////////////
        // Synchronization configuration. 
        // This function is automatically called once at the 
        // end of elaboration
        //////////////////////////////////////////////////////////////
        void set_sync_parameters(){  
            sync_step = sync_step <= 0 ? DELTA_T_CIRCUIT : sync_step;
            set_max_timestep(sync_step);
            use_adaptive_sync(use_adaptive);
            avoid_rollbacks(avoid_rollback);
        }

        //////////////////////////////////////////////////////////////
        // CT State (x attribute) setting 
        // x is a vector of size equal to the order of the 
        // differential equation (ODE) system. 
        //////////////////////////////////////////////////////////////
        void set_initial_conditions(){
            // ODE system size = 1: only one independent energy storing 
            // element in the system, the capacitor
            x.resize(1);
            // Set the initial capacitor voltage to 0
            x[0] = 0.0;
        }


        //////////////////////////////////////////////////////////////
        // SYNCHRONIZATION INTERFACE
        //////////////////////////////////////////////////////////////
        
        // Given a boolean indicating whether or not to use the checkpointed 
        // value of inputs (use_input_checkpoints), the state (x), 
        // and the time (t), calculate and set the derivatives of the state 
        // (dxdt).  
        // Mandatory
        void get_derivatives( 
            bool use_input_checkpoints,
            const sct_core::ct_state &x,
            sct_core::ct_state &dxdt,
            double t)
        {
            // Set the mode of the inputs manager to use either the  current values or the most recent checkpoint
            inputs.use_checkpoints(use_input_checkpoints);
            
            // Get the value from the switch_in port
            bool switch_closed = inputs[switch_in];

            // Depending on the switch state, calculte and set the 
            // differnetial equations 
            if (switch_closed) {
                // Capacitor is charging
                dxdt[0] = Vi/(C*R1) - ((R1+R2)/(C*R1*R2))*x[0];
            }
            else {
                // Capacitor is discharging
                dxdt[0] = -1/(R2*C)*x[0];
            }
        }


        // Given a boolean indicating whether the output events have been 
        // precisely located and an unordered map containing these events, 
        // Write the events in the output ports 
        // Mandatory
        void generate_outputs(
            bool state_event_located, 
            std::unordered_map<int, bool> events)
        {
            if (state_event_located) {
                
                // To inform the event, we inverse the 
                // signals boolean values

                if (events[up_event]) {
                    up_out.write(!up_out.read());
                }

                if (events[down_event]) {
                    down_out.write(!down_out.read());
                }

            }
            // Write the voltage output port, in case the 
            // value is needed for another module
            v_out.write(x[0]);
        }


        // Given the state (x) and time (t), return an unordered map object 
        // containing elements whose key is an integer identifying the event 
        // and as value a boolean that indicates whether or not 
        // the event has been produced  
        // Not mandatory
        std::unordered_map<int, bool> is_event(
            const sct_core::ct_state &x,
            double t = 0) 
        {
            // Object to return 
            std::unordered_map<int, bool> events;            
            
            // Evaluate predicate indicating that the up threshold is crossed: 
            // switch closed (capacitor charging) and up threshold crossed
            events[up_event] = (switch_in.read() && x[0] >= max_threshold);

            // Evaluate predicate indicating that the down threshold is crossed: 
            // switch open (capacitor discharging) and down threshold crossed
            events[down_event] = (!switch_in.read() && x[0] <= min_threshold);
            
            return events;
        }

        // To force a outputs at fixed intervals or at known event times
        // return a non-zero value greater than the current simulation time.
        // Notice that it degrades performance w.r.t. non-sampled version where // outputs are produced only at state events. 
        // Not mandatory. 
        sc_core::sc_time get_next_predictable_event_time() {
            while (next_sampling_time <= sc_core::sc_time_stamp()) {
                next_sampling_time = next_sampling_time +
                    sc_core::sc_time(SAMPLING_PERIOD, sc_core::SC_SEC);
            }
            return next_sampling_time;
        }

    private:
        // System parameters
        double R1 = 0.5;
        double R2 = 0.5;
        double C = 1;
        double Vi = 1.0;
        double max_threshold = 0.40;
        double min_threshold = 0.20;

        // Event identifiers 
        enum event_ids {up_event, down_event};

        // Sync. parameters
        bool avoid_rollback, use_adaptive; 
        double sync_step;
        sc_core::sc_time next_sampling_time;
};

#endif