/* ---------------------------------------------------------------------------
 * SystemC CT: SystemC Continuous Time Library
 *
 * Copyright (C) 2018-2021
 * Univ. Grenoble Alpes, CNRS, Grenoble INP*, TIMA, 38000 Grenoble, France.
 * *Institute of Engineering Univ. Grenoble Alpes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *--------------------------------------------------------------------------*/

/* ---------------------------------------------------------------------------
 * \file        main.cpp
 * \brief       TODO
 * \author      Breytner Fernandez
 * \modified    Liliana Andrade
 *--------------------------------------------------------------------------*/

#include <systemc>
#include <systemc-ct.h>
#include <iostream>
#include <time.h>

#include "ball_ct.h"
#include "ball_de.h"


// Pass as first parameter 'y' to use the adaptive sync. strategy,
// 'y' as second parameter to avoid rollbacks (use time of the next
// event),
// and a real number as the third parameter to set the timestep
// Ex: ./main y y 0.1
int sc_main(int argc, char* argv[]){

    // To measure execution time
    clock_t t = clock();

    // Default syncronization values
    bool use_adaptive = true;
    bool avoid_rollback = false;
    double timestep = 0;

    // Get synchronization configuration from argv
    get_sync_conf(argc, argv, use_adaptive, avoid_rollback, timestep);

    // Declare signals
    //// Ball height and speed
    sc_core::sc_signal<double> h_out_sig, v_out_sig;
    //// Collisition, stop and low energy signals
    sc_core::sc_signal<bool> collision_sig, stop_sig, low_energy_sig;

    // Declare modules
    BallDe ball_de("ball_de");
    BallCt ball_ct("ball_ct", use_adaptive, avoid_rollback, timestep);

    // Port binding
    ball_de.stop_out.bind(stop_sig);
    ball_de.low_energy_in.bind(low_energy_sig);
    ball_ct.h_out.bind(h_out_sig);
    ball_ct.v_out.bind(v_out_sig);
    ball_ct.collision_out.bind(collision_sig);
    ball_ct.bounce_in.bind(collision_sig);
    ball_ct.stop_in.bind(stop_sig);
    ball_ct.low_energy_out.bind(low_energy_sig);

    // Start simulation for a given time
    sc_core::sc_time simulated_time(15, sc_core::SC_SEC);
    sc_core::sc_start(simulated_time);

    // Finish simulation
    sc_core::sc_stop();

    // Print simulation time statistics and synchronization configuration
    // information
    t = clock() - t;
    print_sync_stats(use_adaptive, avoid_rollback, timestep,
                     simulated_time.to_seconds(), ((double)t)/CLOCKS_PER_SEC);

    return 0;
}

// ---------------------------------------------------------------------------
// vim: ft=cpp : expandtab : tabstop=4 : softtabstop=4 : shiftwidth=4
