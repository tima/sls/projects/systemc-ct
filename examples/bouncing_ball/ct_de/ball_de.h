/* ---------------------------------------------------------------------------
 * SystemC CT: SystemC Continuous Time Library
 *
 * Copyright (C) 2018-2021
 * Univ. Grenoble Alpes, CNRS, Grenoble INP*, TIMA, 38000 Grenoble, France.
 * *Institute of Engineering Univ. Grenoble Alpes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *--------------------------------------------------------------------------*/

/* ---------------------------------------------------------------------------
 * \file        ball_de.h
 * \brief       TODO
 * \author      Breytner Fernandez
 * \modified    Liliana Andrade
 *--------------------------------------------------------------------------*/

#ifndef BALL_DE_H
#define BALL_DE_H

#include <systemc>
#include "ball_common.h"

class BallDe : public sc_core::sc_module {
    public:
        // A transition from false to true means that the ball should stop
        // bouncing
        sc_core::sc_out<bool> stop_out;
        // A transition from false to true means that the integration
        // procedure failed to find the time of the bouncing
        sc_core::sc_in<bool> low_energy_in;

        int bounce_count;

        BallDe(sc_core::sc_module_name name)
        {
            SC_HAS_PROCESS(BallDe);

            SC_METHOD(stop_bouncing);
            sensitive << low_energy_in;
            dont_initialize();
            bounce_count = 0;

            state = running;
        }

        // Output a signal to stop the ball bouncing when the numerical solver
        // is not capable of finding the state event
        void stop_bouncing(){
            state = stopped;
            stop_out.write(true);
        }

    private :
        BallState state;
};

#endif

// ---------------------------------------------------------------------------
// vim: ft=cpp : expandtab : tabstop=4 : softtabstop=4 : shiftwidth=4
