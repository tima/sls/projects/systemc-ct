/* ---------------------------------------------------------------------------
 * SystemC CT: SystemC Continuous Time Library
 *
 * Copyright (C) 2018-2021
 * Univ. Grenoble Alpes, CNRS, Grenoble INP*, TIMA, 38000 Grenoble, France.
 * *Institute of Engineering Univ. Grenoble Alpes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *--------------------------------------------------------------------------*/

/* ---------------------------------------------------------------------------
 * \file        ball_ct.h
 * \brief       TODO
 * \author      Breytner Fernandez
 * \modified    Liliana Andrade
 *--------------------------------------------------------------------------*/

#ifndef BALL_CT_H
#define BALL_CT_H

#include <systemc>
#include <systemc-ct.h>
#include <cmath>

#include "ball_common.h"

// Order of the system of diff. equations
#define ORDER 2
// Length of integration interval
#define DELTA_T_BALL_MODEL 0.1 // seconds

class BallCt : public sct_core::ct_module {
    public:

        //////////////////////////////////////////////////////////////
        // PORTS
        //////////////////////////////////////////////////////////////

        // Inputs
        //// Each transition signals a collision and, consequently,
        //// that the ball should bounce
        sc_core::sc_in<bool> bounce_in;
        //// A transition from false to true means that the ball goes
        //// from Bouncing to Stopped
        sc_core::sc_in<bool> stop_in;


        // Outputs
        //// Ball's height
        sc_core::sc_out<double> h_out;
        //// Ball's velocity
        sc_core::sc_out<double> v_out;
        //// Each transition signals a collision event
        sc_core::sc_out<bool> collision_out;
        //// The first transition from false to true signals
        //// the low energy event
        sc_core::sc_out<bool> low_energy_out;


        //////////////////////////////////////////////////////////////
        // Constructor
        //////////////////////////////////////////////////////////////
        BallCt(sc_core::sc_module_name name,
               bool use_adaptive = true, bool avoid_rollback = true,
               double sync_step = DELTA_T_BALL_MODEL)
            :
                // Call port constructors
                bounce_in("bounce_in"),
                stop_in("stop_in"),
                h_out("h_out"),
                v_out("v_out"),
                collision_out("collision_out"),
                low_energy_out("low_energy_out"),
                // Initialize synchronization parameters
                use_adaptive(use_adaptive),
                avoid_rollback(avoid_rollback),
                sync_step(sync_step)
    {
    }

        //////////////////////////////////////////////////////////////
        // Synchronization configuration.
        // This function is automatically called once at the
        // end of elaboration
        //////////////////////////////////////////////////////////////
        void set_sync_parameters(){
            sync_step = sync_step <= 0 ? DELTA_T_BALL_MODEL : sync_step;
            set_max_timestep(sync_step);
            use_adaptive_sync(use_adaptive);
            avoid_rollbacks(avoid_rollback);
        }


        //////////////////////////////////////////////////////////////
        // CT State (x attribute) setting
        // x is a vector of size equal to the order of the
        // differential equation (ODE) system.
        //////////////////////////////////////////////////////////////
        void set_initial_conditions(){
            x.resize(2);
            x[0] = 10;
            x[1] = 0;
        }

        //////////////////////////////////////////////////////////////
        // SYNCHRONIZATION INTERFACE
        //////////////////////////////////////////////////////////////

        // Given a boolean indicating whether or not to use the checkpointed
        // value of inputs (use_input_checkpoints), the state (x),
        // and the time (t), calculate and set the derivatives of the state
        // (dxdt).
        // Mandatory
        void get_derivatives(bool use_input_checkpoints,
                             const sct_core::ct_state &x,
                             sct_core::ct_state &dxdt,
                             double t)
        {
            // Set the mode of the inputs manager to either current_values or
            // checkpoint
            ode_system::inputs.use_checkpoints(use_input_checkpoints);
            // Get the value from the stop_in port
            bool stopped = ode_system::inputs[stop_in];

            // Bouncing.
            if (!stopped) {
                dxdt[0] = x[1];
                dxdt[1] = -g;
            }
            // Stopped, all derivatives to zero.
            else {
                dxdt[0] = 0;
                dxdt[1] = 0;
            }
        }

        // Given a boolean indicating whether the output events have been
        // precisely located and an unordered map containing these events,
        // Write the events in the output ports
        // Mandatory
        void generate_outputs(bool state_event_located,
                              std::unordered_map<int, bool> events)
        {
            h_out.write(x[0]);
            v_out.write(x[1]);

            // If it is a low energy event
            if (events[low_energy_event]) {
                low_energy_out.write(true);
            }

            // If it is a collision event and it has been located
            if (state_event_located && events[collision_event]) {
                collision_out.write(!collision_out.read());
            }
        }


        // Given the state (x) and time (t), return an unordered map object
        // containing elements whose key is an integer identifying the event
        // and as value a boolean that indicates whether or not
        // the event has been produced
        // Not mandatory
        std::unordered_map<int, bool> is_event(const sct_core::ct_state &x,
                                               double t = 0){
            std::unordered_map<int, bool> events;

            bool stopped = ode_system::inputs[stop_in];

            // Bouncing.
            if (!stopped) {
                // Condition for the bounce event
                events[collision_event] =  x[0] <= min_x_threshold && x[1] < 0;
                events[low_energy_event] = x[0] <= min_x_threshold &&
                    fabs(x[1]) < min_v_threshold;
            }

            return events;
        }

        // Execute discontinuous updates in the state depending
        // on the current inputs, state, and simulation time.
        // Return true if at least one update.
        // Not mandatory.
        bool execute_updates() {
            // Condition for the state update
            // Notice that last_bounce_in_value should not constitute
            // a DE state inside the CT module. The problem here is with
            // the channel : we are not allowed to consume the event
            // of bouncing, so we need to base our login on the transition
            // of the boolean variable. This has to be solved with the
            // definition and implementation of appropriate channels.
            if(bounce_in.read() != last_bounce_in_value){
                last_bounce_in_value = bounce_in.read();
                // Discontinuity in state
                x[1] = -elasticity_factor*x[1];
                // Return true to indicate the discontinuous change
                // to the synch. algorithm
                return true;
            }
            // No update
            return false;
        }

        // Map time (t) and state (x) to a state to be traced.
        // This function is called by the internal tracer of the SystemC-CT
        //simulator.
        // Not mandatory. Trace the whole state by default.
        sct_core::ct_state map_state_to_trace(double t,
                                              const sct_core::ct_state &x) {
            return x;
            // We could return, for example, a reduced state
            // containing just one component:
            // sct_core::ct_state ret(1); // Declare a state vector of size 1
            // ret[0] = x[0]; // Copy the ball's height
            // return ret; // Return the one-dimensional vector
        }

    private:
        // System parameters
        double g = 9.81;
        double min_x_threshold = 0.001;
        double min_v_threshold = 0.001;
        double elasticity_factor = 0.8;

        // Foolish var. Needed because we still do not count with
        // appropriate channels to transmit events and their values.
        bool last_bounce_in_value;

        typedef enum {collision_event, low_energy_event} event_ids;

        // Sync. parameters
        bool use_adaptive, avoid_rollback;
        double sync_step;
};

#endif

// ---------------------------------------------------------------------------
// vim: ft=cpp : expandtab : tabstop=4 : softtabstop=4 : shiftwidth=4
