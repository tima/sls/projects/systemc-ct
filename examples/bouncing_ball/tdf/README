------------------------------------------------------------------------------

    README : Example Description and Execution Notes (TDF Bouncing Ball Model)
    SystemC CT: SystemC Continuous Time Library

    Breytner FERNANDEZ <Breytner.Fernandez@univ-grenoble-alpes.fr>
    Liliana ANDRADE <Liliana.Andrade@univ-grenoble-alpes.fr>
    Frédéric Pétrot <Frederic.Petrot@univ-grenoble-alpes.fr>

------------------------------------------------------------------------------

Copyright (C) 2018-2021
Univ. Grenoble Alpes, CNRS, Grenoble INP*, TIMA, 38000 Grenoble, France.
*Institute of Engineering Univ. Grenoble Alpes

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

------------------------------------------------------------------------------


------------------------------------------------------------------------------
File Description (Bouncing Ball Model - TDF interface)
------------------------------------------------------------------------------

Model-related Files
------------------------------------------------------------------------------
main.cpp                SystemC AMS main model. It includes one instantiated
                        TDF module ('BallTdf')

ball_tdf.h              TDF module implementing specific CT functions called
                        by the CT/DE synchronization algorithm

plot_results.m          Script to be used by designer for plotting simulation
                        results. It loads the 'bouncing_tdf.dat' file
                        generated after the model execution and the
                        'simulink_bouncing.mat' file located in the 'matlab'
                        directory


Scripts and Configuration Files
------------------------------------------------------------------------------
Please do not modify the following files:

Makefile                Makefile to be used by designer in the library install
                        directory
 
test.am                 Configuration file used by autotools for the Makefile
                        generation in the development directory


------------------------------------------------------------------------------
Model Compilation and Execution (designer mode)
------------------------------------------------------------------------------
Please execute the following commands in the library install directory:

    Access to the example specific directory (where this file is located)
        > cd examples/bouncing_ball/tdf

    Compile and execute the model
        > make
        > ./main

    View simulation results
        > octave plot_results.m

    Clean the example specific directory (optional)
        > make clean

# ----------------------------------------------------------------------------
# vim: expandtab : tabstop=4 : softtabstop=4 : shiftwidth=4 : textwidth=79
