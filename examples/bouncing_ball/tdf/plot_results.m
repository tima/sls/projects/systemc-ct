trace_1 = load("bouncing_tdf.dat");
load '../matlab/simulink_bouncing.mat'
t_matlab = ans(1,:);
x_matlab = ans(2,:);
v_matlab = ans(3,:);



figure(1);
hold on;
grid on;
plot(trace_1(:,1), trace_1(:,2) , '-g', 'LineWidth', 2);
plot(trace_1(:,1), trace_1(:,3) , '-r', 'LineWidth', 2);
plot(t_matlab, x_matlab , '--r', 'LineWidth', 2);
set(gca,'XTick', [0:1.5:15]);
xlim([ 0.000 15]);
legend("Height", "Velocity", "MATLAB");


drawnow();

input("Press enter");