/* ---------------------------------------------------------------------------
 * SystemC CT: SystemC Continuous Time Library
 *
 * Copyright (C) 2018-2021
 * Univ. Grenoble Alpes, CNRS, Grenoble INP*, TIMA, 38000 Grenoble, France.
 * *Institute of Engineering Univ. Grenoble Alpes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *--------------------------------------------------------------------------*/

/* ---------------------------------------------------------------------------
 * \file        ball_tdf.h
 * \brief       TODO
 * \author      Breytner Fernandez
 * \modified    Liliana Andrade
 *--------------------------------------------------------------------------*/

#ifndef BALL_TDF_H
#define BALL_TDF_H

#include <systemc>
#include <systemc-ams>
#include <systemc-ct>

#include <cmath>


class BallTdf :  public sca_tdf::sca_module {
    public:
        //////////////////////////////////////////////////////////////
        // To save traces with the usuar SystemC AMS mechanism.
        // We need to provide a mechanism that does not depend on
        // signals to record the CT state, because the CT module
        // communicates with the external world through events
        // that do not necessarily contain all the information we might
        // want to record about the internals of the CT module
        //////////////////////////////////////////////////////////////
        sca_tdf::sca_de::sca_out<double> h_out;
        sca_tdf::sca_de::sca_out<double> v_out;


        //////////////////////////////////////////////////////////////
        // Public methods
        //////////////////////////////////////////////////////////////

        BallTdf(sc_core::sc_module_name name)
            //  : bounce_in("bounce_in"), stop_in("stop_in")
        {
            solver = new sct_kernel::numerical_solver();

            diff_equations_func = [&](const sct_core::ct_state &x ,
                                      sct_core::ct_state &dxdt , double t){
                get_derivatives(x, dxdt, t);
            };

            // Lambda function for threshold detection conditions
            state_event_observer =  [&](const sct_core::ct_state &x,
                                        const double t){
                return false;
            };

            x.resize(2);
            x[0] = 10;
            x[1] = 0;

            stopped = false;
        }

        void processing() {
            double t_start = sc_core::sc_time_stamp().to_seconds();
            double t_end = t_start + get_timestep().to_seconds();

            solver->integrate(
                              diff_equations_func,
                              x,
                              t_start,
                              t_end,
                              state_event_observer,
                              [&](double t, sct_core::ct_state &state) {
                              return; } // Empty tracer
                             );

            // Generate outputs
            h_out.write(x[0]);
            v_out.write(x[1]);

            // Backup state
            x_backup = x;

            // Stop event
            if (x[0] <= min_x_threshold &&
                std::fabs(x[1]) < min_v_threshold) {
                stopped = true;
            }

            // Collision event
            if (x[0] <= min_x_threshold && x[1] < 0) {
                x[1] = -elasticity_factor*x[1];
            }


        }

        void set_attributes() {
            set_timestep(0.01, sc_core::SC_SEC);
        }

        // Method that models the ODE system
        void get_derivatives( const sct_core::ct_state &x,
                              sct_core::ct_state &dxdt,
                              double t) {
            // Bouncing.
            if (!stopped) {
                dxdt[0] = x[1];
                dxdt[1] = -g;
            }
            // Stopped, all derivatives to zero.
            else {
                dxdt[0] = 0;
                dxdt[1] = 0;
            }
        }


    private:
        // System parameters
        double g = 9.81;
        double min_x_threshold = 0.001;
        double min_v_threshold = 0.001;
        double elasticity_factor = 0.8;
        sct_kernel::numerical_solver *solver;
        sct_core::ct_state x, x_backup;
        bool stopped;

        std::function<void(const sct_core::ct_state &x,
                           sct_core::ct_state &dxdt,
                           double t)> diff_equations_func;
        std::function<bool(const sct_core::ct_state &,
                           const double)> state_event_observer;

};

#endif

// ---------------------------------------------------------------------------
// vim: ft=cpp : expandtab : tabstop=4 : softtabstop=4 : shiftwidth=4
