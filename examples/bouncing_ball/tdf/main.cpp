/* ---------------------------------------------------------------------------
 * SystemC CT: SystemC Continuous Time Library
 *
 * Copyright (C) 2018-2021
 * Univ. Grenoble Alpes, CNRS, Grenoble INP*, TIMA, 38000 Grenoble, France.
 * *Institute of Engineering Univ. Grenoble Alpes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *--------------------------------------------------------------------------*/

/* ---------------------------------------------------------------------------
 * \file        main.cpp
 * \brief       TODO
 * \author      Breytner Fernandez
 * \modified    Liliana Andrade
 *--------------------------------------------------------------------------*/

#include <systemc>
#include <systemc-ams>
#include <iostream>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */


#include "ball_tdf.h"


int sc_main(int argc, char* argv[]){
    clock_t t = clock();

    sc_core::sc_signal<double> h_out_sig, v_out_sig;

    BallTdf ball_tdf("ball_tdf");


    ball_tdf.h_out.bind(h_out_sig);
    ball_tdf.v_out.bind(v_out_sig);


    sca_util::sca_trace_file *atf;
    atf = sca_util::sca_create_tabular_trace_file("bouncing_tdf.dat");
    sca_trace(atf, h_out_sig, "h");
    sca_trace(atf, v_out_sig, "v");


    // Start simulation for a given time
    sc_core::sc_start(15, sc_core::SC_SEC);

    // Finish simulation
    sc_core::sc_stop();

    sca_util::sca_close_tabular_trace_file(atf);


    t = clock() - t;
    printf ("It took me %d clicks (%f seconds).\n",(int) t,
            ((float)t)/CLOCKS_PER_SEC);

    return 0;
}

// ---------------------------------------------------------------------------
// vim: ft=cpp : expandtab : tabstop=4 : softtabstop=4 : shiftwidth=4
