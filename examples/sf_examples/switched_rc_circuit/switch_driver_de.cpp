/* ---------------------------------------------------------------------------
 * SystemC CT: SystemC Continuous Time Library
 *
 * Copyright (C) 2018-2021
 * Univ. Grenoble Alpes, CNRS, Grenoble INP*, TIMA, 38000 Grenoble, France.
 * *Institute of Engineering Univ. Grenoble Alpes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *--------------------------------------------------------------------------*/

/* ---------------------------------------------------------------------------
 * \file        switch_driver_de.cpp
 * \brief       TODO
 * \author      Breytner Fernandez
 * \modified    Liliana Andrade
 *--------------------------------------------------------------------------*/

#include "switch_driver_de.h"

SwitchDriverDE::SwitchDriverDE(sc_core::sc_module_name name){
    SC_METHOD(up_ctrl_thread);
    sensitive << up_in << down_in;
    dont_initialize();

    switch_out.initialize(true);
}

void SwitchDriverDE::control_switch(bool sw_state){
    switch_out.write(sw_state);
}

void SwitchDriverDE::start_of_simulation() {
    last_up_event = up_in.read();
}

void SwitchDriverDE::up_ctrl_thread(){
    // up event
    if (up_in.read() != last_up_event) {
        control_switch(false);
        last_up_event = up_in.read();
    }
    // down event
    else {
        control_switch(true);
    }
}

// ---------------------------------------------------------------------------
// vim: ft=cpp : expandtab : tabstop=4 : softtabstop=4 : shiftwidth=4
