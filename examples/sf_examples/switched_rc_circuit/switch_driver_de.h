/* ---------------------------------------------------------------------------
 * SystemC CT: SystemC Continuous Time Library
 *
 * Copyright (C) 2018-2021
 * Univ. Grenoble Alpes, CNRS, Grenoble INP*, TIMA, 38000 Grenoble, France.
 * *Institute of Engineering Univ. Grenoble Alpes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *--------------------------------------------------------------------------*/

/* ---------------------------------------------------------------------------
 * \file        switch_driver_de.h
 * \brief       TODO
 * \author      Breytner Fernandez
 * \modified    Liliana Andrade
 *--------------------------------------------------------------------------*/

#ifndef SWITCH_DRIVER_DE_H
#define SWITCH_DRIVER_DE_H

#include<systemc>

// Module that generates the switch control signal
SC_MODULE(SwitchDriverDE){
    // Inputs
    sc_core::sc_in<bool> up_in;
    sc_core::sc_in<bool> down_in;

    // Outputs
    sc_core::sc_out<bool> switch_out;

    SC_CTOR(SwitchDriverDE);

    void control_switch(bool sw_state);
    void up_ctrl_thread();
    void down_ctrl_thread();
    void start_of_simulation();

    private:
    // Needed until we define a proper communication channel
    bool last_up_event;
};

#endif

// ---------------------------------------------------------------------------
// vim: ft=cpp : expandtab : tabstop=4 : softtabstop=4 : shiftwidth=4
