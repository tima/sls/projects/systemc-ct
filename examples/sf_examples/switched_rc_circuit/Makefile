## ---------------------------------------------------------------------------
## SystemC CT: SystemC Continuous Time Library
##
## Copyright (C) 2018-2021
## Univ. Grenoble Alpes, CNRS, Grenoble INP*, TIMA, 38000 Grenoble, France.
## *Institute of Engineering Univ. Grenoble Alpes
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
## ---------------------------------------------------------------------------
##
## ---------------------------------------------------------------------------
## \file        Makefile
## \brief       Makefile used by designer for compiling the example in the
##              current directory (after library install)
## \author      Liliana Andrade
## \modified    Liliana Andrade
## ---------------------------------------------------------------------------

### ---------------------------------------------------------------------------
## include generic makefile for examples

include ../../Makefile.examples

## ---------------------------------------------------------------------------
## define misc tools
RM				:= rm -f

## ---------------------------------------------------------------------------
## add when needed additional search paths, definitions and flags
CPPFLAGS		+= -I.
CXXFLAGS		+= -Werror

## ---------------------------------------------------------------------------
## add when needed additional libraries
LDFLAGS			+=
LIBS			+=

## ---------------------------------------------------------------------------
## set the name of the executable and source files
TARGET_D0		:= main
SOURCES_D0		:= main.cpp switch_driver_de.cpp
OBJS_D0			:= $(SOURCES_D0:.cpp=.o)

## ---------------------------------------------------------------------------
## build rules
OBJS			:= $(OBJS_D0) $(OBJS_WD)
TARGET			:= $(TARGET_D0) $(TARGET_WD)

all: $(TARGET)

$(TARGET_D0): $(OBJS_D0)
$(TARGET_WD): $(OBJS_WD)

$(TARGET):
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	$(RM) $(OBJS) $(TARGET) *.dat sink_1

# ----------------------------------------------------------------------------
# vim: tabstop=4 : softtabstop=4 : shiftwidth=4 : textwidth=79
