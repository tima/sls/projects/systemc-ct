/* ---------------------------------------------------------------------------
 * SystemC CT: SystemC Continuous Time Library
 *
 * Copyright (C) 2018-2021
 * Univ. Grenoble Alpes, CNRS, Grenoble INP*, TIMA, 38000 Grenoble, France.
 * *Institute of Engineering Univ. Grenoble Alpes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *--------------------------------------------------------------------------*/

/* ---------------------------------------------------------------------------
 * \file        sf_rc_circuit.h
 * \brief       TODO
 * \author      Breytner Fernandez
 * \modified    Liliana Andrade
 *--------------------------------------------------------------------------*/

#ifndef SF_RC_CIRCUIT_H
#define SF_RC_CIRCUIT_H

#include <systemc>
#include <systemc-ct.h>


class sf_rc_circuit : public sf::sf_cluster {
    public:
        sc_core::sc_out<bool> threshold_up_out, threshold_down_out;
        sc_core::sc_out<double> sampler_out;
        sc_core::sc_in<bool> mux_ctrl_in;

        sf_rc_circuit(sc_core::sc_module_name name)
            :
                threshold_up_out("threshold_up_out"),
                threshold_down_out("threshold_down_out"),
                sampler_out("sampler_out"), source_1("source_1", 1),
                adder_1("adder_1"), gain_1("gain_1", -2),
                gain_2("gain_2", 2),  gain_3("gain_3", -4),
                integrator_1("integrator_1", 1), sink_1("sink_1"),
                mux_1("mux_1"), gain_1_sg("gain_1_sg"),
                gain_2_sg("gain_2_sg"), gain_3_sg("gain_3_sg"),
                adder_1_sg("adder_1_sg"), source_1_sg("source_1_sg"),
                mux_1_sg("mux_1_sg"), integrator_1_sg("integrator_1_sg"),
                detector_1("detector_1", 0.4),
                detector_2("detector_2", 0.2, false),
                sampler_1("sampler_1", sc_core::sc_time(0.05, sc_core::SC_SEC))
    {
        gain_1.x(integrator_1_sg);
        gain_1.y(gain_1_sg);

        source_1.y(source_1_sg);

        gain_2.x(source_1_sg);
        gain_2.y(gain_2_sg);

        adder_1.x1(gain_2_sg);
        adder_1.x2(gain_3_sg);
        adder_1.y(adder_1_sg);

        mux_1.x1(gain_1_sg);
        mux_1.x2(adder_1_sg);
        mux_1.ctrl_in(mux_ctrl_in);
        mux_1.y(mux_1_sg);

        integrator_1.x(mux_1_sg);
        integrator_1.y(integrator_1_sg);

        gain_3.x(integrator_1_sg);
        gain_3.y(gain_3_sg);

        sink_1.x(integrator_1_sg);

        detector_1.x(integrator_1_sg);
        detector_1.event_out(threshold_up_out);

        detector_2.x(integrator_1_sg);
        detector_2.event_out(threshold_down_out);

        sampler_1.x(integrator_1_sg);
        sampler_1.val_out(sampler_out);
    }


    private:
        // Eq. primitives
        sf::sf_source source_1;
        sf::sf_adder adder_1;
        sf::sf_gain gain_1, gain_2, gain_3;
        sf::sf_integrator integrator_1;
        sf::sf_sink_file sink_1;
        sf::sf_mux mux_1;

        // Signals
        sf::sf_signal gain_1_sg, gain_2_sg, gain_3_sg, adder_1_sg, source_1_sg,
            mux_1_sg, integrator_1_sg;

        // CT/DE converters
        sf::sf_threshold_detector detector_1;
        sf::sf_threshold_detector detector_2;
        sf::sf_sampler sampler_1;
};

#endif

// ---------------------------------------------------------------------------
// vim: ft=cpp : expandtab : tabstop=4 : softtabstop=4 : shiftwidth=4
