/* ---------------------------------------------------------------------------
 * SystemC CT: SystemC Continuous Time Library
 *
 * Copyright (C) 2018-2021
 * Univ. Grenoble Alpes, CNRS, Grenoble INP*, TIMA, 38000 Grenoble, France.
 * *Institute of Engineering Univ. Grenoble Alpes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *--------------------------------------------------------------------------*/

/* ---------------------------------------------------------------------------
 * \file        main.cpp
 * \brief       TODO
 * \author      Breytner Fernandez
 * \modified    Liliana Andrade
 *--------------------------------------------------------------------------*/

#include <systemc>
#include <systemc-ct.h>
#include <iostream>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

#include "switch_driver_de.h"
#include "sf_rc_circuit.h"

int sc_main(int argc, char* argv[]){
    clock_t t = clock();

    sc_core::sc_signal<bool> switch_sg, up_sg, down_sg;
    sc_core::sc_signal<double> sampler_sg;

    SwitchDriverDE switch_driver("driver");
    sf_rc_circuit circuit("circuit");

    switch_driver.switch_out.bind(switch_sg);
    switch_driver.up_in.bind(up_sg);
    switch_driver.down_in.bind(down_sg);

    circuit.threshold_up_out(up_sg);
    circuit.threshold_down_out(down_sg);
    circuit.mux_ctrl_in(switch_sg);
    circuit.sampler_out(sampler_sg);


    // Start simulation for a given time
    sc_core::sc_start(2, sc_core::SC_SEC);

    // Finish simulation
    sc_core::sc_stop();


    t = clock() - t;
    printf ("It took me %d clicks (%f seconds).\n", (int) t,
            ((float)t)/CLOCKS_PER_SEC);

    return 0;
}

// ---------------------------------------------------------------------------
// vim: ft=cpp : expandtab : tabstop=2 : softtabstop=4 : shiftwidth=4
