trace_2 = load("circuit.dat");

figure(1);
hold on;
grid on;
plot(trace_2(:,1), trace_2(:,2) , '-b', 'LineWidth', 2);

legend("v_c SF");

input("Press enter");