
% Load data
trace_ctde = load("circuit.dat");
plecs_v2 = load("../plecs/v2.mat").data;
% plecs_v1 = load("../plecs/v1.mat").data;
% plecs_l1 = load("../plecs/l1.mat").data;
% plecs_l2 = load("../plecs/l2.mat").data;
spice_v2 =  load("../spice/vout.dat");

figure(1);
hold on;
grid on;    
plot(plecs_v2(1,:), plecs_v2(2,:), '-b', 'LineWidth', 1);
plot(trace_ctde(:,1), trace_ctde(:,4), '--k', 'LineWidth', 1);
plot(spice_v2(:,1), spice_v2(:,2), '--r', 'LineWidth', 1);
legend("Vc2 PLECS", "Vc2 ISC", "Vc2 NGSPICE");

% figure(2);
% hold on;
% grid on;  
% plot(plecs_v1(1,:), plecs_v1(2,:), '-b', 'LineWidth', 1);
% plot(trace_ctde(:,1), trace_ctde(:,2), '--k', 'LineWidth', 1);
% legend("Vc1 PLECS", "Vc1 ISC");

% figure(3);
% hold on;
% grid on;    
% plot(plecs_l1(1,:), plecs_l1(2,:), '-b', 'LineWidth', 1);
% plot(trace_ctde(:,1), trace_ctde(:,4), '--k', 'LineWidth', 1);
% legend("I1 PLECS", "I1 ISC");

% figure(4);
% hold on;
% grid on;   
% plot(plecs_l2(1,:), plecs_l2(2,:), '-b', 'LineWidth', 1);
% plot(trace_ctde(:,1), trace_ctde(:,5), '--k', 'LineWidth', 1);
% legend("I2 PLECS", "I2 ISC");


drawnow();

input("Press enter to exit");