#include "cuk_converter.h"


sepi_converter::sepi_converter(sc_core::sc_module_name name, 
            bool use_adaptive, bool avoid_rollback, 
            double sync_step)
    :  
    ISC::cluster(name, use_adaptive, avoid_rollback, sync_step),
    switch_ctrl_in("switch_ctrl_in"),
    source_el("source_el", -V),
    switch_el("switch_el"), ind_1_el("ind_1_el", L1), ind_2_el("ind_2_el", L2), diode_el("diode_el", false), cap_1_el("cap_1_el", C1), cap_2_el("cap_2_el", C2), res_load_el("res_load_el", RLOAD), ground_node("ground_node"),
    node_1("node_1"), node_2("node_2"), node_3("node_3"),
    node_4("node_4")
{
    source_el.terminal_a(ground_node);
    source_el.terminal_b(node_1);

    ind_1_el.terminal_a(node_1);
    ind_1_el.terminal_b(node_2);

    switch_el.terminal_a(node_2);
    switch_el.terminal_b(ground_node);
    switch_el.ctrl_in(switch_ctrl_in);

    cap_1_el.terminal_a(node_2);
    cap_1_el.terminal_b(node_3);

    ind_2_el.terminal_a(node_3);
    ind_2_el.terminal_b(node_4);

    diode_el.terminal_a(node_3);
    diode_el.terminal_b(ground_node);

    cap_2_el.terminal_a(node_4);
    cap_2_el.terminal_b(ground_node);

    res_load_el.terminal_a(node_4);
    res_load_el.terminal_b(ground_node);
}


void sepi_converter::set_initial_conditions() {
    x.resize(4);
    x[0] = 0.0;
    x[1] = 0.0;
    x[2] = 0.0;
    x[3] = 0.0;
}

