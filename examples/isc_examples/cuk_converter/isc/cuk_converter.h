#ifndef SEPI_CONVERTER_H
#define SEPI_CONVERTER_H

#include <systemc>

#include <systemc-ct.h>

// Length of integration interval
#define SEPI_DELTA_T 0.0000001 // seconds

// Parameter values
#define V 12
#define L1 500e-6
#define L2 500e-6
#define C1 5e-6
#define C2 400e-6
#define RLOAD 0.5


class sepi_converter : public ISC::cluster {
    public:
        //////////////////////////////////////////////////////////////
        // Input events 
        //////////////////////////////////////////////////////////////
        sc_core::sc_in<bool> switch_ctrl_in; 


       sepi_converter(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sepi_converter"), 
            bool use_adaptive = true, bool avoid_rollback = true, 
            double sync_step = DELTA_T);

        void set_initial_conditions();


    private: 
        // Elements
        ISC::v_source source_el;
        ISC::switch_t switch_el;
        ISC::inductor ind_1_el, ind_2_el;
        ISC::diode diode_el;
        ISC::capacitor cap_1_el, cap_2_el;
        ISC::resistance res_load_el;
        ISC::node ground_node, node_1, node_2, node_3, node_4;
};

#endif