#ifndef FLYBACK_CONVERTER_H
#define FLYBACK_CONVERTER_H

#include <systemc>

#include <systemc-ct.h>

// Length of integration interval
#define DELTA_T 2 // seconds


class flyback_converter : public ISC::cluster {
    public:
        //////////////////////////////////////////////////////////////
        // Input events 
        //////////////////////////////////////////////////////////////
        sc_core::sc_in<bool> switch_ctrl_in; 


       flyback_converter(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("flyback_converter"), 
            bool use_adaptive = true, bool avoid_rollback = true, 
            double sync_step = DELTA_T);

        void set_initial_conditions();


    private: 
        // Elements
        ISC::v_source source_el;
        ISC::switch_t switch_el;
        ISC::inductor ind_el;
        ISC::diode diode_el;
        ISC::capacitor cap_el;
        ISC::resistance res_el;
        ISC::node ground_node, node_1, node_2, node_3;
};

#endif