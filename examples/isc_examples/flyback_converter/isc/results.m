% Load data
trace_ctde = load("circuit.dat");
% load("../matlab/simulink_switched.mat");
% t_matlab = ans(1,:);
% sw_matlab = ans(2,:);
% vc_matlab = ans(3,:);


figure(1);
hold on;
grid on;    
plot(trace_ctde(:,1), trace_ctde(:,2), '-ob', 'LineWidth', 2);
plot(trace_ctde(:,1), trace_ctde(:,3), '-or', 'LineWidth', 2);


legend("Il", "Vc");

drawnow();

input("Press enter to exit");