#ifndef SWITCH_DRIVER_DE_H
#define SWITCH_DRIVER_DE_H

#include <systemc>

// Module that generates
// the switch control signal
SC_MODULE(SwitchDriverDE){
    public: 
        // Outputs
        sc_core::sc_out<bool> switch_out;
        
        SC_CTOR(SwitchDriverDE);

        void switch_ctrl_thread();

    private:

};

#endif