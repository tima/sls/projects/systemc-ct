#include "switch_driver_de.h"

SwitchDriverDE::SwitchDriverDE(sc_core::sc_module_name name){

    SC_THREAD(switch_ctrl_thread);
    
    switch_out.initialize(true);
}

void SwitchDriverDE::switch_ctrl_thread(){
   
    while (true) {
        // Open switch after one second
        sc_core::wait(1, sc_core::SC_SEC);
        switch_out.write(false);

        // Close switch after one more second
        sc_core::wait(1, sc_core::SC_SEC);
        switch_out.write(true);
    }
}
