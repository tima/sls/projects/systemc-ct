#ifndef CIRCUIT_SC_MODULE_H
#define CIRCUIT_SC_MODULE_H

#include <systemc>
#include "ct_module.h"

// Length of integration interval
#define DELTA_T 2 // seconds

#define SAMPLING_PERIOD 0.05 //seconds

class CircuitScModule : public sct_core::ct_module{
    public:
        //////////////////////////////////////////////////////////////
        // Input events 
        //////////////////////////////////////////////////////////////
        sc_core::sc_in<bool> switch_in; 


        //////////////////////////////////////////////////////////////
        // Public methods 
        //////////////////////////////////////////////////////////////
        CircuitScModule(sc_core::sc_module_name name,
            bool use_adaptive = true, bool avoid_rollback = true, 
            double sync_step = DELTA_T
            ) :
            switch_in("switch_in"), 
            use_adaptive(use_adaptive), 
            avoid_rollback(avoid_rollback),
            sync_step(sync_step),
            next_sampling_time(0, sc_core::SC_SEC)
        { 

        }

        void set_sync_parameters(){  
            sync_step = sync_step <= 0 ? DELTA_T : sync_step;
            
            set_max_timestep(sync_step);
            use_adaptive_sync(use_adaptive);
            avoid_rollbacks(avoid_rollback);
        }

        void set_initial_conditions(){
            x[0] = 0.0;
            x[1] = 0.0;
        }

        void get_derivatives(bool use_input_checkpoints,
            const sct_core::ct_state &x , sct_core::ct_state &dxdt , double t ){
             // Set the mode of the inputs manager to either current_values or checkpoint
            ode_system::inputs.use_checkpoints(use_input_checkpoints);
            // Get the value from the stop_in port
            bool switch_closed = ode_system::inputs[switch_in];

            if (switch_closed) {
                // Capacitor's voltage 
                dxdt[0] = (-1 / (R * C)) * x[0];
                // Inductor's current
                dxdt[1] = (1 / L) * E;
            }
            else {
                // Capacitor's voltage 
                dxdt[0] = (-1 / (R * C)) * x[0] + (-1 / C) * x[1];
                // Inductor's current
                dxdt[1] = (1 / L) * x[0];
            }
        }

       std::unordered_map<int, bool> is_event(const sct_core::ct_state &x, double t = 0) {
            std::unordered_map<int, bool> events;
            return events;
        }

        void generate_outputs(bool state_event_located, 
            std::unordered_map<int, bool> events
        ) {

        }

    private:
        // System parameters
        double R = 1;
        double L = 1;
        double C = 1;
        double E = 1; //Input voltage

        // Sync. parameters
        bool use_adaptive, avoid_rollback; 
        double sync_step;
        sc_core::sc_time next_sampling_time;
};

#endif