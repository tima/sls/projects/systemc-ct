#include<systemc>
#include<systemc-ct.h>
#include<iostream> 
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

#include "switch_driver_de.h"
#include "circuit_sc_module.h"



// Pass as first parameter y to use the adaptive sync. strategy,
// y as second parameter to avoid rollbacks (use time of the next
// event), and a real number as the third parameter to set the timestep
int sc_main(int argc, char* argv[]){
    clock_t t = clock();

    sc_core::sc_time simulated_time(3, sc_core::SC_SEC);
    bool use_adaptive = true;
    bool avoid_rollback = false;
    double timestep = 0;

    get_sync_conf(argc, argv, use_adaptive, avoid_rollback, timestep);


    sc_core::sc_signal<bool> switch_sg;

    SwitchDriverDE switch_driver("driver");
    CircuitScModule circuit("circuit", use_adaptive, avoid_rollback, timestep);

    switch_driver.switch_out.bind(switch_sg);
    circuit.switch_in.bind(switch_sg);

    // Start simulation for a given time
    sc_core::sc_start(simulated_time);

    // Finish simulation
    sc_core::sc_stop();
 
    t = clock() - t;

    print_sync_stats(use_adaptive, avoid_rollback, timestep, simulated_time.to_seconds(), ((double)t)/CLOCKS_PER_SEC);

    return 0;
}