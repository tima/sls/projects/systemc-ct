pkg load control

% Parameters
R = 1;
C = 0.05;
L = 0.3;
E = 1;

% Switching time
t1 = 1;
t2 = 2;
t3 = 3;

% Initial conditions x[0] is Vc and x[1] is Il
x0 = [0 0]';

% Flyback converter SS, switch ON and diode OFF
A1 = [-1/(R*C) 0; 0 0]
B1 = [0; E/L]
C1 = [1 0; 0 1]
D1 = [0;0]

ss1 = ss(A1,B1,C1,D1)

% Flyback converter SS, switch OFF and diode ON
A2 = [-1/(R*C) -1/C; 1/L 0]
B2 = [0; 0]
C2 = [1 0; 0 1]
D2 = [0;0]

ss2 = ss(A2,B2,C2,D2)

% Simulation from 0 to stwitching time t1
% Switch ON and diode OFF
t = linspace(0,t1);
u1 = ones(length(t),1);
[y1,ty1] = lsim(ss1,u1,t,x0);

% Simulation from t1 to switching time t2
% Switch OFF and diode ON
x0 = [y1(end, 1) y1(end, 2)]
t = linspace(t1,t2);
u2 = ones(length(t),1);
[y2,ty2] = lsim(ss2,u2,t,x0);

% Simulation from t2 to end time t3
% Switch ON and diode OFF
x0 = [y2(end, 1) y2(end, 2)]
t = linspace(t2,t3);
u3 = ones(length(t),1);
[y3,ty3] = lsim(ss1,u3,t,x0);

% Plot response
y = [y1; y2; y3];
t = [ty1; ty2; ty3];

plot(t, y);

input ("Press any key")