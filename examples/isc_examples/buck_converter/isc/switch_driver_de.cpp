#include "switch_driver_de.h"

SwitchDriverDE::SwitchDriverDE(sc_core::sc_module_name name){

    SC_THREAD(switch_ctrl_thread);
    
    switch_out.initialize(true);
}

void SwitchDriverDE::switch_ctrl_thread(){
   
    double time_to_wait = 1.0 / SWITCHING_FREQ;
    double time_on = time_to_wait * DUTY_CYCLE;
    double time_off = time_to_wait - time_on;

    while (true) {
        // // Open switch after one second
        sc_core::wait(time_off, sc_core::SC_SEC);
        switch_out.write(false);

        // Close switch after one more second
        sc_core::wait(time_on, sc_core::SC_SEC);
        switch_out.write(true);
    }
}
