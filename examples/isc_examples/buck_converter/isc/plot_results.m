% Load data
data = load("../matlab/data.mat").data;
t_matlab = data(:,1);
vc_matlab = data(:,2);
il_matlab = data(:,3);
trace_ctde = load("circuit.dat");
plecs_il = load("../plecs/il.mat").data;
plecs_vc = load("../plecs/vc.mat").data;
spice_vc = load("../spice/vout.dat");


figure(1);
hold on;
grid on;    
plot(plecs_vc(1,:), plecs_vc(2,:), '-b', 'LineWidth', 1);
plot(trace_ctde(:,1), trace_ctde(:,2), '--k', 'LineWidth', 1);
plot(t_matlab, vc_matlab, '--r', 'LineWidth', 1);
plot(spice_vc(:,1), spice_vc(:,2), '--g', 'LineWidth', 1);
legend("Vc PLECS", "Vc ISC", "Vc MATLAB", "Vc SPICE");


% figure(2);
% hold on;
% grid on;    
% plot(plecs_il(1,:), plecs_il(2,:), '-b', 'LineWidth', 1);
% plot(trace_ctde(:,1), trace_ctde(:,3), '--k', 'LineWidth', 1);
% plot(t_matlab, il_matlab, '--g', 'LineWidth', 2);
% legend("Il PLECS", "Il ISC", "Il MATLAB");
% drawnow();

input("Press enter to exit");