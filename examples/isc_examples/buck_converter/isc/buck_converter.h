#ifndef BUCK_CONVERTER_H
#define BUCK_CONVERTER_H

#include <systemc>

#include <systemc-ct.h>

// Tentative length of integration interval
#define BUCK_DELTA_T 0.00001 // seconds ~ (1/ freq)

class buck_converter : public ISC::cluster {
    public:
        //////////////////////////////////////////////////////////////
        // Input events 
        //////////////////////////////////////////////////////////////
        sc_core::sc_in<bool> switch_ctrl_in; 


       buck_converter(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("buck_converter"), 
            bool use_adaptive = true, bool avoid_rollback = true, 
            double sync_step = BUCK_DELTA_T);

        void set_initial_conditions();


    private: 
        // Elements
        ISC::v_source source_el;
        ISC::switch_t switch_el;
        ISC::inductor ind_el;
        ISC::diode diode_el;
        ISC::capacitor cap_el;
        ISC::resistance res_el;
        ISC::node ground_node, node_1, node_2, node_3;
};

#endif