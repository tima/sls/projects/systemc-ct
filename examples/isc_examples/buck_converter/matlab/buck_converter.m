pkg load control

% Parameters
R = 2.5; 
C = 4e-06
L = 125e-06
E = 10;

% Frequency in hertz
freq = 50000; 
% 
period = 1/freq;
duty_cycle = 0.5;
t_on = duty_cycle * period;
t_off = period - t_on;
t_sim = 0.0005;
t = 0;

% Initial conditions x[0] is Vc and x[1] is Il
x0 = [0 0]';

% Flyback converter SS, switch ON and diode OFF
A1 = [-1/(R*C) 1/C; -1/L 0]
B1 = [0; E/L]
C1 = [1 0; 0 1]
D1 = [0;0]

ss1 = ss(A1,B1,C1,D1)

% % Flyback converter SS, switch OFF and diode ON
A2 = [-1/(R*C) 1/C; -1/L 0]
B2 = [0; 0]
C2 = [1 0; 0 1]
D2 = [0;0]

ss2 = ss(A2,B2,C2,D2)

% Simulation from 0 to stwitching time t1
% Switch ON and diode OFF

y_vec = [0 0];
t_vec = [0];

while t < t_sim 
    x0 = [y_vec(end, 1) y_vec(end, 2)]
    _t = linspace(t_vec(end), t_vec(end) + t_on);
    u1 = ones(length(_t),1);
    [y1,ty1] = lsim(ss1,u1,_t,x0);

    y_vec = [y_vec; y1];
    t_vec = [t_vec; ty1];


    x0 = [y_vec(end, 1) y_vec(end, 2)]
    _t = linspace(t_vec(end),t_vec(end)+t_off);
    u1 = ones(length(_t),1);
    [y1,ty1] = lsim(ss2,u1,_t,x0);

    y_vec = [y_vec; y1];
    t_vec = [t_vec; ty1];

    t = t_vec(end);
end

data = [t_vec y_vec];
save data.mat data

plot(t_vec, y_vec);
legend("Vc", "Il");


input ("Press any key")