#ifndef SWITCH_DRIVER_DE_H
#define SWITCH_DRIVER_DE_H

#include <systemc>

//Switching frequency in hertz
#define SWITCHING_FREQ 50000

// Duty cycle
#define DUTY_CYCLE 0.5

// Module that generates
// the switch control signal
SC_MODULE(SwitchDriverDE){
    public: 
        // Outputs
        sc_core::sc_out<bool> switch_out;
        
        SC_CTOR(SwitchDriverDE);

        void switch_ctrl_thread();

    private:

};

#endif