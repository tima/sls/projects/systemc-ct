#ifndef boost_CONVERTER_H
#define boost_CONVERTER_H

#include <systemc>
#include <systemc-ct.h>

// Length of integration interval
#define BOOST_DELTA_T 0.00001 // seconds

// Parameter values
#define V 6
#define L 150e-6
#define C 33.33e-6
#define R 6



class boost_converter : public ISC::cluster {
    public:
        //////////////////////////////////////////////////////////////
        // Input events 
        //////////////////////////////////////////////////////////////
        sc_core::sc_in<bool> switch_ctrl_in; 


       boost_converter(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("boost_converter"), 
            bool use_adaptive = true, bool avoid_rollback = true, 
            double sync_step = BOOST_DELTA_T);

        void set_initial_conditions();


    private: 
        // Elements
        ISC::v_source source_el;
        ISC::switch_t switch_el;
        ISC::inductor ind_el;
        ISC::diode diode_el;
        ISC::capacitor cap_el;
        ISC::resistance res_el;
        ISC::node ground_node, node_1, node_2, node_3;
};

#endif