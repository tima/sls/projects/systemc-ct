#include "boost_converter.h"


boost_converter::boost_converter(sc_core::sc_module_name name, 
            bool use_adaptive, bool avoid_rollback, 
            double sync_step)
    :  
    ISC::cluster(name, use_adaptive, avoid_rollback, sync_step),
    switch_ctrl_in("switch_ctrl_in"),
    source_el("source_el", -V),
    switch_el("switch_el"), ind_el("ind_el", L), diode_el("diode_el", false), cap_el("cap_el", C), res_el("res_el", R), ground_node("ground_node"),
    node_1("node_1"), node_2("node_2"), node_3("node_3")
{
    source_el.terminal_a(ground_node);
    source_el.terminal_b(node_1);

    ind_el.terminal_a(node_1);
    ind_el.terminal_b(node_2);

    switch_el.terminal_a(node_2);
    switch_el.terminal_b(ground_node);
    switch_el.ctrl_in(switch_ctrl_in);

    diode_el.terminal_a(node_2);
    diode_el.terminal_b(node_3);

    cap_el.terminal_a(node_3);
    cap_el.terminal_b(ground_node);

    res_el.terminal_a(node_3);
    res_el.terminal_b(ground_node);
}


void boost_converter::set_initial_conditions() {
    x.resize(2);
    x[0] = 0.0;
    x[1] = 0.0;
}

