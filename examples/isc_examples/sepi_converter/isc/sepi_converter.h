#ifndef SEPI_CONVERTER_H
#define SEPI_CONVERTER_H

#include <systemc>

#include <systemc-ct.h>

// Length of integration interval
#define SEPI_DELTA_T 0.00001 // seconds

// Parameter values
#define V 120
#define L1 500e-6
#define L2 100e-6
#define RL1 100e-3
#define RL2 20e-3
#define C1 47e-6
#define C2 200e-6
#define RLOAD 50


class sepi_converter : public ISC::cluster {
    public:
        //////////////////////////////////////////////////////////////
        // Input events 
        //////////////////////////////////////////////////////////////
        sc_core::sc_in<bool> switch_ctrl_in; 

        sc_core::sc_out<bool> event_out; 

       sepi_converter(sc_core::sc_module_name name = sc_core::sc_gen_unique_name("sepi_converter"), 
            bool use_adaptive = true, bool avoid_rollback = true, 
            double sync_step = DELTA_T);

        void set_initial_conditions();


    private: 
        // Elements
        ISC::v_source source_el;
        ISC::switch_t switch_el;
        ISC::inductor ind_1_el, ind_2_el;
        ISC::diode diode_el;
        ISC::capacitor cap_1_el, cap_2_el;
        ISC::resistance res_1_el, res_2_el, res_load_el, res_load_2_el;
        ISC::voltmeter voltmeter_1, voltmeter_2;
        ISC::ammeter ammeter_1;
        ISC::tracer tracer_1, tracer_2, tracer_3;
        ISC::signal signal_1, signal_2, signal_3;
        ISC::cc_source cc_source_1;
        ISC::threshold_detector detector_1;
        ISC::node ground_node, node_1, node_2, node_3, node_4, node_5, node_6, node_7, node_8;
};

#endif