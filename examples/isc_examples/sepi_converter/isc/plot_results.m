
% Load data
trace_ctde = load("circuit.dat");
plecs_v2 = load("../plecs/v2.mat").data;
plecs_v1 = load("../plecs/v1.mat").data;
plecs_l1 = load("../plecs/l1.mat").data;
plecs_l2 = load("../plecs/l2.mat").data;
spice_v2 =  load("../spice/vout.dat");
spice_il1 =  load("../spice/il.dat");
matlab_v2 = load("../matlab/vout.mat").vout;
matlab_il1 = load("../matlab/il1.mat").il1;



figure(1);
subplot(2,1,1);
hold on;
grid on;    
plot(plecs_v2(1,:), plecs_v2(2,:), '-b', 'LineWidth', 2);
plot(trace_ctde(1:40:end,1), trace_ctde(1:40:end,3), '--r', 'LineWidth', 2);
plot(spice_v2(1:9000:end,1), spice_v2(1:9000:end,2), '*k', 'LineWidth', 2);
plot(matlab_v2(1,1:75:end), matlab_v2(2,1:75:end), 'og', 'LineWidth', 2);
xlim([0 0.016]);
ylim([0 225]);
set(gca,'XTick',0:0.004:0.016);
set(gca,'YTick',0:25:225);
legend( "PLECS", "ISC", "SPICE", "MATLAB");

subplot(2,1,2);
hold on;
grid on;  
plot(plecs_l1(1,:), plecs_l1(2,:), '-b', 'LineWidth', 2); 
plot(trace_ctde(1:50:end,1), trace_ctde(1:50:end,2), '--r', 'LineWidth', 2);
plot(spice_il1(1:7000:end,1), spice_il1(1:7000:end,2), '*k', 'LineWidth', 2);
plot(matlab_il1(1,1:60:end), matlab_il1(2,1:60:end), 'og', 'LineWidth', 2);
xlim([0 0.016]);
set(gca,'XTick',0:0.004:0.016);
legend("PLECS", "ISC", "SPICE", "MATLAB");



% figure(2);
% hold on;
% grid on;    
% plot(plecs_v1(1,:), plecs_v1(2,:), '-b', 'LineWidth', 1);
% plot(trace_ctde(:,1), trace_ctde(:,2), '-k', 'LineWidth', 1);
% legend("Vc1 PLECS", "Vc1 ISC");

% figure(3);
% hold on;
% grid on;    
% plot(plecs_l1(1,:), plecs_l1(2,:), '-b', 'LineWidth', 1);
% plot(trace_ctde(:,1), trace_ctde(:,4), '--k', 'LineWidth', 1);
% legend("I1 PLECS", "I1 ISC");

% figure(4);
% hold on;
% grid on;   
% plot(plecs_l2(1,:), plecs_l2(2,:), '-b', 'LineWidth', 1);
% plot(trace_ctde(:,1), trace_ctde(:,5), '--k', 'LineWidth', 1);
% legend("I2 PLECS", "I2 ISC");


drawnow();

input("Press enter to exit");