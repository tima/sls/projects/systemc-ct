#include "sepi_converter.h"


sepi_converter::sepi_converter(sc_core::sc_module_name name, 
            bool use_adaptive, bool avoid_rollback, 
            double sync_step)
    :  
    ISC::cluster(name, use_adaptive, avoid_rollback, sync_step),
    switch_ctrl_in("switch_ctrl_in"), event_out("event_out"),
    source_el("source_el", -V),
    switch_el("switch_el"), ind_1_el("ind_1_el", L1), ind_2_el("ind_2_el", L2), diode_el("diode_el", false), cap_1_el("cap_1_el", C1), cap_2_el("cap_2_el", C2), res_1_el("res_1_el", RL1), res_2_el("res_2_el", RL2), res_load_el("res_load_el", RLOAD), res_load_2_el("res_load_2_el", RLOAD),
    voltmeter_1("voltmeter_1"), voltmeter_2("voltmeter_2"),
    ammeter_1("ammeter_1"),
    tracer_1("tracer_1"), tracer_2("tracer_2"), tracer_3("tracer_3"),
    signal_1("signal_1"), signal_2("signal_2"), signal_3("signal_3"),
    cc_source_1("cv_source_1", -2.0/RLOAD),
    detector_1("detector_1", 250),
    ground_node("ground_node"),
    node_1("node_1"), node_2("node_2"), node_3("node_3"),
    node_4("node_4"), node_5("node_5"), node_6("node_6"),
    node_7("node_7"), node_8("node_8")
{
    source_el.terminal_a(ground_node);
    source_el.terminal_b(node_1);

    ind_1_el.terminal_a(node_1);
    ind_1_el.terminal_b(node_2);

    ammeter_1.terminal_a(node_2);
    ammeter_1.terminal_b(node_3);
    ammeter_1.signal_out(signal_1);
    
    tracer_1.signal_in(signal_1);

    res_1_el.terminal_a(node_3);
    res_1_el.terminal_b(node_4);

    switch_el.terminal_a(node_4);
    switch_el.terminal_b(ground_node);
    switch_el.ctrl_in(switch_ctrl_in);

    cap_1_el.terminal_a(node_4);
    cap_1_el.terminal_b(node_5);

    ind_2_el.terminal_a(node_5);
    ind_2_el.terminal_b(node_6);

    res_2_el.terminal_a(node_6);
    res_2_el.terminal_b(ground_node);

    diode_el.terminal_a(node_5);
    diode_el.terminal_b(node_7);

    cap_2_el.terminal_a(node_7);
    cap_2_el.terminal_b(ground_node);

    voltmeter_1.terminal_a(node_7);
    voltmeter_1.terminal_b(ground_node);
    voltmeter_1.signal_out(signal_2);
    
    tracer_2.signal_in(signal_2);

    res_load_el.terminal_a(node_7);
    res_load_el.terminal_b(ground_node);

    cc_source_1.terminal_a(node_8);
    cc_source_1.terminal_b(ground_node);
    cc_source_1.signal_in(signal_2);

    res_load_2_el.terminal_a(node_8);
    res_load_2_el.terminal_b(ground_node);

    voltmeter_2.terminal_a(node_8);
    voltmeter_2.terminal_b(ground_node);
    voltmeter_2.signal_out(signal_3);

    detector_1.signal_in(signal_3);
    detector_1.event_out(event_out);

    tracer_3.signal_in(signal_3);

}


void sepi_converter::set_initial_conditions() {
    x.resize(4);
    x[0] = 0.0;
    x[1] = 0.0;
    x[2] = 0.0;
    x[3] = 0.0;
}

