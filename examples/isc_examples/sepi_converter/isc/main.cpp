
#include<systemc>
#include<systemc-ct.h>
#include<iostream> 
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include "sepi_converter.h"
#include "switch_driver_de.h"





int sc_main(int argc, char* argv[]){
    // sc_core::sc_time simulated_time(1, sc_core::SC_SEC);

    // ISC::cluster cluster;

    // // Start simulation for a given time
    // sc_core::sc_start(simulated_time);

    // // Finish simulation
    // sc_core::sc_stop();

    // return 0;


    sc_core::sc_time simulated_time(0.04100, sc_core::SC_SEC);
 //   sc_core::sc_time simulated_time(1e-06, sc_core::SC_SEC);
    bool use_adaptive = true;
    bool avoid_rollback = false;
    double timestep = 0;

    get_sync_conf(argc, argv, use_adaptive, avoid_rollback, timestep);
    if (timestep == 0) {
        timestep = DELTA_T;
    }


    sc_core::sc_signal<bool> switch_sg, crossing_sg;

    SwitchDriverDE switch_driver("driver");
    sepi_converter circuit("circuit", use_adaptive, avoid_rollback, timestep);

    switch_driver.switch_out.bind(switch_sg);
    switch_driver.event_in.bind(crossing_sg);
    
    circuit.switch_ctrl_in.bind(switch_sg);
    circuit.event_out.bind(crossing_sg);


    clock_t t = clock();
    std::cout << "Start" << std::endl;

    // Start simulation for a given time
    sc_core::sc_start(simulated_time);

    // Finish simulation
    sc_core::sc_stop();
 
    t = clock() - t;

    print_sync_stats(use_adaptive, avoid_rollback, timestep, simulated_time.to_seconds(), ((double)t)/CLOCKS_PER_SEC);

    return 0;

}