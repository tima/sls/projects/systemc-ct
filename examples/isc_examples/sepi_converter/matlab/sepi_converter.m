pkg load control

% Parameters
V = 120
L1 = 500e-6
L2 = 100e-6
RL1 = 100e-3
RL2 = 20e-3
CAP1 = 47e-6
CAP2 = 200e-6
RLOAD = 50


% Frequency in hertz
freq = 100000; 
% 
period = 1/freq;
duty_cycle = 0.5;
t_on = duty_cycle * period;
t_off = period - t_on;
t_sim = 0.04;
t = 0;

% Initial conditions x[0] is Vc and x[1] is Il
x0 = [0 0 0 0]';

% Boost converter SS, switch ON and diode OFF
A1 = [
    0                   0                     0     1/CAP1;
    0           -1/(RLOAD*CAP2)               0           0;
    0                   0                   -RL1/L1     0;
    -1/L2              0                   0           -RL2/L2;
];

B1 = [0; 0; V/L1; 0];
C1 = eye(4);
D1 = [0;0;0;0];

ss1 = ss(A1,B1,C1,D1)

% % Boost converter SS, switch OFF and diode ON

A2 = [
    0                   0                   1/CAP1       0;
    0                   -1/(RLOAD*CAP2)       1/CAP2        -1/CAP2;
    -1/L1               -1/L1              -RL1/L1     0;
    0                1/L2                   0           -RL2/L2;
];
B2 = [0; 0; V/L1; 0];
C2 = eye(4);
D2 = [0;0;0;0];

ss2 = ss(A2,B2,C2,D2)

% Simulation from 0 to stwitching time t1
% Switch ON and diode OFF

y_vec = [0 0 0 0];
t_vec = [0];

while t < t_sim 
    x0 = [y_vec(end, 1) y_vec(end, 2)   y_vec(end, 3)   y_vec(end, 4)]
    _t = linspace(t_vec(end), t_vec(end) + t_on);
    u1 = ones(length(_t),1);
    [y1,ty1] = lsim(ss1,u1,_t,x0);

    y_vec = [y_vec; y1];
    t_vec = [t_vec; ty1];


    x0 = [y_vec(end, 1) y_vec(end, 2)   y_vec(end, 3)   y_vec(end, 4)]
    _t = linspace(t_vec(end),t_vec(end)+t_off);
    u1 = ones(length(_t),1);
    [y1,ty1] = lsim(ss2,u1,_t,x0);

    y_vec = [y_vec; y1];
    t_vec = [t_vec; ty1];

    t = t_vec(end);
end

data = [t_vec y_vec];
save data.mat data

plot(t_vec, y_vec);
legend("Vc", "Il");

drawnow();

input ("Press any key")